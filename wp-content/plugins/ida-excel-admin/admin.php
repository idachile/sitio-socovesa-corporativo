<?php
/** * @version 1.0 */
/*
Plugin Name: INDICES IPSA / SOCOVESA
Plugin URI: http://wordpress.org/plugins
Description: Tabla de  índices IPSA y SOCOVESA
Author: IDA
Version: 1
Author URI: IDA Chile @IDAChile
*/

class catsup_indice {
   function __construct() {
      // add_action('admin_init', array($this, 'check_download'));;
      add_action('admin_print_styles', array($this, 'include_style'));
      add_action('admin_menu', array($this, 'forms_page'));
   }

   function set_ajax() {
      if (isset($_GET['funcion']) && $_GET['funcion'] && method_exists($this, $_GET['funcion'])) :
         $this->{$_GET['funcion']}($_GET);
      else:
         die('Not Allowed >:(');
      endif;
   }

   function include_style() {
      if ( is_admin() ):
         wp_deregister_style('newsLetterStyles');
         wp_register_style('newsLetterStyles', plugins_url('crm.css', __FILE__));
         wp_enqueue_style('newsLetterStyles');
         // datepicker para exportacion de cortizacioes
         wp_register_style('crm_datepicker', plugins_url('datepicker.css', __FILE__));
         wp_enqueue_style('crm_datepicker');

         wp_register_script( 'crm_admin_script', plugins_url('crm.js', __FILE__), array( 'jquery-ui-datepicker' ), false, true );
         wp_enqueue_script('jquery-ui-datepicker');
         wp_enqueue_script( 'crm_admin_script' );
      endif;
   }

   function get_current_date(){
      $dia = date_i18n('j', strtotime(current_time('d/m/Y')));
      $mes = date_i18n('m', strtotime(current_time('d/m/Y')));
      $ano = date_i18n('Y', strtotime(current_time('d/m/Y')));
      $fecha = $dia . '-' . ucfirst($mes) . '-' . $ano;
      return $fecha;
   }

   function forms_page() {
      add_menu_page('Indice IPSA', 'IPSA', 'manage_options', 'indice_ipsa', array($this, 'listar_ipsa'), 'dashicons-chart-bar');
      // add_submenu_page('descarga_contacto','Descarga de Contactos', 'Descarga de Contactos', 'manage_options', 'descarga_contacto', array($this, 'descargas_formulario'));
      // add_submenu_page('descarga_contacto','Descarga de Newsletter', 'Descarga de Newsletter', 'manage_options', 'descarga_newsletter', array($this, 'descargas_formulario'));
      // add_submenu_page('descarga_contacto','Descarga de Referir', 'Descarga de Referir', 'manage_options', 'descarga_referir_amigo', array($this, 'descargas_formulario'));
      // add_submenu_page('descarga_contacto','Descarga de Canal inversionista', 'Descarga de Canal inversionista', 'manage_options', 'descarga_canal_inversion', array($this, 'descargas_formulario'));
      //
      // add_submenu_page('descarga_contacto','Descarga de Visitas agendadas', 'Descarga de Visitas agendadas', 'manage_options', 'descarga_agendar', array($this, 'descargas_formulario_type'));
      // add_submenu_page('descarga_contacto','Descarga de Más info', 'Descarga de Más info', 'manage_options', 'descarga_masinfo', array($this, 'descargas_formulario_type'));
      // add_submenu_page('descarga_contacto','Descarga de Novedades', 'Descarga de Novedades', 'manage_options', 'descarga_novedades', array($this, 'descargas_formulario_type'));
      //
      // add_submenu_page('descarga_contacto','Descarga de Cotizaciones', 'Descarga de Cotizaciones', 'manage_options', 'descarga_cotizacion', array($this, 'descargas_formulario_type'));
   }

   function listar_ipsa() {
     global $wpdb;
      $count = 1;

      $valores = $wpdb->get_results('SELECT id, fecha, ipsa, puntos, precio, socovesa FROM wp_valor_bursatil ORDER by id DESC');
      $out  =  '<div id="newsletter-page-wrapper">';
      $out .=     '<h1>Gestión de IPSA -SOCOVESA</h1>';
      $out .=     '<div id="emailsTable-holder" >';

      $out .=        '<form action="/wp-json/agregar/ipsa/" method="post" autocomplete="off">';
      $out .=           '<input type="hidden" name="guardar_ipsa" value="true" >';
      $out .=           'Fecha <input type="text" name="fecha" placeholder="Ingrese fecha" required >';
      $out .=           'IPSA <input type="text" name="ipsa" placeholder="Ingrese índice" required >';
      $out .=           'Puntos <input type="text" name="puntos" placeholder="Ingrese puntos" required >';
      $out .=           'Socovesa <input type="text" name="socovesa" placeholder="Ingrese índice" required >';
      $out .=           'Precio <input type="text" name="precio" placeholder="Ingrese precio" required >';
      $out .=           '<input type="submit" value="Agregar valores">';
      $out .=        '</form>';

      $out .=        '<table id="emailsTable">';
      $out .=           '<tr>';
      $out .=              '<th>#</th>';
      $out .=              '<th>Fecha</th>';
      $out .=              '<th>IPSA %</th>';
      $out .=              '<th>Puntos</th>';
      $out .=              '<th>Socovesa %</th>';
      $out .=              '<th>Precio</th>';
      $out .=              '<th></th>';
      $out .=           '</tr>';

         foreach ($valores as $valor):
            $out .=     '<tr>';
            $out .=        '<td>' . $count . '</td>';
            $out .=        '<td>' . $valor->fecha . '</td>';
            $out .=        '<td>' . $valor->ipsa . '</td>';
            $out .=        '<td>' . $valor->puntos . '</td>';
            $out .=        '<td>' . $valor->socovesa . '</td>';
            $out .=        '<td>' . $valor->precio . '</td>';
            $out .=        '<td><a class="delete" href="/wp-json/eliminar/ipsa/'.$valor->id.'/" title="Eliminar valor"> Eliminar  </a></td>';
            $out .=     '</tr>';
            $count++;
         endforeach;


      $out .=        '</table>';
      $out .=     '</div>';
      $out .=  '</div>';

      echo $out;
   }
}

function init_idaexcel_handler() {
   new catsup_indice();
}
add_action('init', 'init_idaexcel_handler');
?>
