(function($){
	$(function(){
		$('[data-crm-datepicker]').datepicker({
			dayNames : ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesMin : ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            dayNamesShort : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            dateFormat: 'dd/mm/yy'
		}).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
	});
}(jQuery));