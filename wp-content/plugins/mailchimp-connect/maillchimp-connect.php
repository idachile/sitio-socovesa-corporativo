<?php

/*
  Plugin Name: Conexion Mailchimp
  Plugin URI: http://ida.cl
  Description: Plugin de conexión para mailchimp
  Version: 1.1
  Author: Fernando Silva
  Author URI: https://github.com/fdograph
  License: Open Source
*/

/*
    Deberia haber una pagina de configuracion y graficos
        La configuracion debiese tener
            - apikey
            - activacion de archivo historico de newsletters
            - estadisticas de interes


    Deberia crear un post type de suscriptores asociados por taxonomia u otro metodo a cada lista
    Deberia existir un shortcode y un action para mostrar y editar un formulario de suscripcion conectado a
        mailchimp y wordpress

    Deberia crear un post type de newsletters asociados a cada campaña
    Deberia ser capaz de usar la estructura clasica de temas de wordpress para crear el html de cada campaña
    Cada campaña debe tener un area de configuracion que lo asocie a una campaña y lista en mailchimp, deberia contar con botones para enviar una prueba y la version de produccion
    Deberia tener la capacidad de ser previsualizado


*/
class Ninja_mailchimp {
    var $api_key = false;
    function __construct(){
         if( !class_exists('Mailchimp') ){ require_once 'api/Mailchimp.php'; }
         $this->api_key = get_field("api_key", "options");
         $this->api = new Mailchimp( $this->api_key, array( 'debug' => true ));
    }
}

/*add_action('admin_init', function(){
     $ninja_mailchimp = new Ninja_mailchimp();
});*/



if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'menu_title' 	=> 'IDA Mailchimp',
		'page_title' 	=> 'IDA Mailchimp',
		'parent_slug' 	=> 'tools.php',
	));
}
?>