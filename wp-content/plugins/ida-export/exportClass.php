<?php

class ExportClassContacto{

    private $filename = 'file.csv';
    private $filetype = 'csv';
    private $data = [];

    function __construct($filename,$filetype,$data)
    {
		$upload_dir   = wp_upload_dir();
		$new_folder = $upload_dir['basedir'] . '/downloads';

		if (! is_dir($new_folder))  wp_mkdir_p( $new_folder );

      $this->filename = $new_folder.'/'.$filename .'.'.$filetype ;
      $this->filetype = $filetype;
      $this->data = $data;
    }

    public function generar(){
      ob_clean();
      ob_start();

      try{

        $file = fopen($this->filename, "w");

        if($this->filetype == "csv"):
          foreach ($this->data as $fields) { fputcsv($file, $fields); }
        elseif($this->filetype == "tsv"):
          foreach ($this->data as $fields) { fwrite($file, implode("\t",$fields).PHP_EOL); }
        endif;

        fclose($file);

      }catch(Exception $error){
        die('Se produjo un error inesperado');
      }

      $this->{'header_'.$this->filetype}();

      readfile($this->filename);
      exit;
    }

    public function header_tsv(){
      header('Content-Description: File Transfer');
      header('Content-type: text/tab-separated-values');
      header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($this->filename));
    }

    public function header_csv(){
      header('Content-Description: File Transfer');
      header('Content-Type: application/csv');
      header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($this->filename));
    }

}