<?php

function contactos_module_list(){
	use_script('script-init');
	
	export_contactos();

	$args_post = array(
		'post_type' => 'contacto',
		'posts_per_page' => 20,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'tax_query' => array(
			array(
				'taxonomy' => 'tipo_contacto',
				'field' => 'slug',
				'terms' => 'general'
			)
		)
	);

	$contactos_general = new WP_Query($args_post);
?>

<div class="wrap">
	<h2>Contactos generales</h2>
	<p>Mostrando los últimos 20 contactos</p>
	<div class="tablenav top">
		<div class="alignleft actions bulkactions">
			<form method='get' action="admin.php?page=contactos_module" autocomplete="off">
				<input type="hidden" name='page' value="contactos_module" />
				<input type="hidden" name='noheader' value="1" />
				<input class="datepicker-wp" type="text" name="fecha_inicio" placeholder="Fecha de inicio" style="height:28px" required>
				<input class="datepicker-wp" type="text" name="fecha_fin" placeholder="Fecha de término" style="height:28px" required>
				<select name="format">
					<option value="csv" selected>Extensión csv</option>
					<option value="tsv">Extensión tsv</option>
				</select>
				<input type="submit" name="export" class="button button-primary" value="Descargar">
			</form>
		</div>
		<br class="clear">
	</div>

	<?php if($contactos_general->have_posts()): ?>
	<table class='wp-list-table widefat fixed striped posts' style="margin-top: 2rem;">
		<tr>
			<th class="manage-column ss-list-width" width="15%" style="font-weight: bold;">Fecha</th>
			<th class="manage-column ss-list-width" width="15%" style="font-weight: bold;">Nombre completo</th>
			<th class="manage-column ss-list-width" width="15%" style="font-weight: bold;">E-mail</th>
			<th class="manage-column ss-list-width" width="15%" style="font-weight: bold;">Tema a consultar</th>
			<th class="manage-column ss-list-width" width="10%" style="font-weight: bold;"></th>
		</tr>
		<?php while ($contactos_general->have_posts()): $contactos_general->the_post(); $pid = get_the_ID(); ?>
			<?php if(!empty($data = get_field('datos_de_contacto', $pid))): ?>
		<tr>
			<td class="manage-column ss-list-width">
				<?php echo get_the_date('d-m-Y H:i:s', $pid); ?>
			</td>
			<td class="manage-column ss-list-width">
				<?php if(!empty($data['nombre']))  echo $data['nombre'].' '.$data['apellido']; ?>
			</td>
			<td class="manage-column ss-list-width">
				<?php if(!empty($data['email']))  echo $data['email']; ?>
			</td>
			<td class="manage-column ss-list-width">
				<?php if(!empty($data['tema'])) echo $data['tema']; ?>
			</td>
			<td class="manage-column ss-list-width">
				<a href="<?php echo get_edit_post_link($pid); ?>" class="button main" title="Ver detalle de postulación">Ver contacto</a>
			</td>
		</tr>
			<?php endif; ?>
		<?php endwhile; wp_reset_query(); ?>
	</table>
	<?php endif; ?>
</div>
<?php } ?>