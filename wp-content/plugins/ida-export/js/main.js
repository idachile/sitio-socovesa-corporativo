$(function(){
    $( ".datepicker-wp" ).datepicker({ dateFormat: "yy-mm-dd"});

    $('.message_box .btn_response').on('click',function(){
        var $this = $(this);
        var box = $this.parents('.message_box');
        var action = $this.data('action');
        var check = $('#message_container .btn_response.active');
        if(check.length > 0){
            if(check != $this){
              delete_comment(check);
            }
        }
        if(action == "resp"){
          if($('#message_container').find('#comment').length == 0){
            $('input[name=message_id]').val(box.data('id'));
            box.after($('#message_comment').html());
          }
          $this.html('Cancelar').data('action','cancel').addClass('active');
        }else if(action == "cancel"){
          delete_comment($this);
        }
    });

    function delete_comment($this){
      $this.html('Responder').data('action','resp').removeClass('active');
      $('#message_container').find('#comment').remove();
    }
});
