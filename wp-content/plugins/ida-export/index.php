<?php
/*
Plugin Name: IDA Exportador
Description: Modulo para la exportación de recepción de formularios
Version: 1.2
Author: IDA Developers
Author URI: http://www.ida.cl/
*/


//Agregando la opcion en el menu del dashboard
function add_contact_menu() {
  	add_menu_page(
		'Descargas',
		'Descargas',
		'manage_options',
		'contactos_module',
		'contactos_module_list',
		'dashicons-download'
	);

	add_submenu_page(
		'contactos_module',
		'Descargar Contactos generales',
		'Contactos generales',
		'manage_options',
		'contactos_module',
		'contactos_module_list'
	);

	add_submenu_page(
		'contactos_module',
		'Descargar Contactos Inversionista',
		'Contactos Inversionista',
		'manage_options',
		'inversion_module',
		'inversion_module_list'
	);
}
add_action('admin_menu','add_contact_menu');

function script_init() {
	wp_register_script( 'jquery-3', plugins_url( '/js/jquery.js', __FILE__ ), array(), '3.2.1', true);
	wp_register_script( 'jquery-ui', "https://code.jquery.com/ui/1.12.1/jquery-ui.js", array("jquery-3"),'3.2.1', true);
	wp_register_script( 'script-init', plugins_url( '/js/main.js', __FILE__ ), array("jquery-3","jquery-ui"), '1.1', true);
}
add_action('admin_enqueue_scripts','script_init');


define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'exportClass.php');
require_once(ROOTDIR . 'helpers.php');
require_once(ROOTDIR . 'contactos_export.php');
require_once(ROOTDIR . 'inversion_export.php');
require_once(ROOTDIR . 'contactos_view.php');
require_once(ROOTDIR . 'inversion_view.php');