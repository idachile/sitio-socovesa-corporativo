<?php

function export_inversion() {
	$date = array();

	foreach($_GET as $key=>$val){
		$mindata[$key]=mb_strtolower($val);
		$bad = preg_match('/(print|print_r|echo|system|bak|bac|war|xz|old|jspf|jspx|cbz|jar|lzma|md5|jpg\.|gif\.|exec|exe|system32|code|script|sleep|sysdate|now|temp|eval|mysql|function|\)|\(|\<|\>|foo|bar)/i', $mindata[$key], $matches);
		if($bad===1){
			http_response_code(400);
			die("Bad Request");
			exit;
		}
	}

	if ( isset( $_GET['export'] )):
		$date = array();
		if( isset($_GET['fecha_inicio']) && isset($_GET['fecha_fin']) ):

			$fecha_inicio = explode('-', $_GET['fecha_inicio']);
			$date['start'] = date("Y-m-d", strtotime(intval($fecha_inicio[1]) . '/' . intval($fecha_inicio[2]) . '/' . intval($fecha_inicio[0])));

			$fecha_fin = explode('-', $_GET['fecha_fin']);
			// $date['end'] = date("Y-m-d", strtotime(intval($fecha_fin[1]) . '/' . intval($fecha_fin[0]) . '/' . intval($fecha_fin[2])));
			$date['end'] = date("Y-m-d", strtotime(intval($fecha_fin[1]) . '/' . intval($fecha_fin[2]) . '/' . intval($fecha_fin[0])));
		endif;

		$result = obtener_inversion($date);

		foreach($result as $key=>$val){
			$mindata[$key]=mb_strtolower($val);
			$bad = preg_match('/(print|print_r|echo|system|bak|bac|war|xz|old|jspf|jspx|cbz|jar|lzma|md5|jpg\.|gif\.|exec|exe|system32|code|script|sleep|sysdate|now|temp|eval|mysql|function|\)|\(|\<|\>|foo|bar)/i', $mindata[$key], $matches);
			if($bad===1){
				http_response_code(400);
				die("Bad Request");
				exit;
			}
		}

      $export = new ExportClassContacto('contactos_inversion_' . current_time('timestamp'), $_GET['format'], $result);
      $export->generar();
	endif;
}

function obtener_inversion($date){
	$array[] = array(
		'Fecha',
		'Nombre completo',
		'E-mail',
		'Teléfono',
		'Empresa',
		'Mensaje'
	);
	$count = 1;

	$contactos = new WP_Query(array(
		'post_type' => 'contacto',
		'posts_per_page' => 20,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'date_query' => array(
			array(
				'after'     => $date['start'],
				'before'    => $date['end'],
				'inclusive' => true,
			)
		),
		'tax_query' => array(
			array(
				'taxonomy' => 'tipo_contacto',
				'field' => 'slug',
				'terms' => 'inversionista'
			)
		)
	));

	if($contactos->have_posts()):
		while ( $contactos->have_posts() ): $contactos->the_post();
			$pid = get_the_ID();

			if($datos = get_field('datos_de_contacto', $pid)):
				$nombre_completo = $datos['nombre'].' '.$datos['apellido'];

				$array[$count][0] = get_the_date('d-m-Y H:i:s', $pid);
				$array[$count][1] = $nombre_completo;
				$array[$count][2] = $datos['email'];
				$array[$count][3] = $datos['telefono'];
				$array[$count][4] = $datos['empresa'];
				$array[$count][5] = $datos['mensaje'];
			endif;

			$count++;
		endwhile;
		wp_reset_query();
	endif;

	return $array;
}