<?php

function format_date_contact($date){
	return date("d-m-Y H:m:s",strtotime($date));
}

function use_script( $script_name ){
	if( !wp_script_is( $script_name ) ){
		 wp_enqueue_script( $script_name );
	}
}

function trim_string( $string, $charnum, $sufix = ' ...' ){
	$string = strip_tags( $string );
	if( strlen($string) > $charnum ){
		$string = substr($string, 0, ($charnum - strlen( $sufix )) ) . $sufix;
	}
	return mb_convert_encoding($string, "UTF-8");
}
?>
