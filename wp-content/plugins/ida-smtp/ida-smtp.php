<?php
/**
 * @version 1.0
 */
/*
Plugin Name: IDA SMTP Mail sender
Plugin URI: http://wordpress.org/plugins
Description: IDA SMTP Mail sender
Author: IDA
Version: 1.1
Author URI: http://ida.cl/
*/
add_action( 'phpmailer_init', 'idamailerSMTP' );
 function idamailerSMTP($phpmailer){
    if(get_field("smtp_mailing", "options")==true):
        $phpmailer->IsSMTP();
        $phpmailer->SMTPAuth   = get_field("smtp_auth", "options");  // true / false
        $phpmailer->Host       = get_field("smtp_host", "options");
        $phpmailer->Username   = get_field("smtp_username", "options");
        $phpmailer->Password   = get_field("smtp_password", "options");
        $phpmailer->SMTPSecure = get_field("smtp_secure", "options"); // ssl Enable if required - 'tls' is another possible value
        $phpmailer->Port       = get_field("smtp_port", "options");    // SMTP Port - 26 is for GMail
        // try {
        //     $phpmailer->status = $phpmailer->Send();
        // } catch ( phpmailerException $e ){
        //     $phpmailer->status = 0;
        // }
    endif;
}

// add_action( 'phpmailer_init', 'WCMphpmailerException' );
function WCMphpmailerException( $phpmailer ){
    if ( ! defined( 'WP_DEBUG' ) OR ! WP_DEBUG )
    {
        $phpmailer->SMTPDebug = 0;
        $phpmailer->debug = 0;
        return;
    }
    if ( ! current_user_can( 'manage_options' ) )
        return;

    // Enable SMTP
    $phpmailer->IsSMTP();
    $phpmailer->SMTPDebug = 2;
    $phpmailer->debug     = 1;

    // Use `var_dump( $data )` to inspect stuff at the latest point and see
    // if something got changed in core. You should consider dumping it during the
    // `wp_mail` filter as well, so you get the original state for comparison.
    apply_filters(
        'wp_mail',
        compact( 'to', 'subject', 'message', 'headers', 'attachments' )
    );

    current_user_can( 'manage_options' )
        AND print htmlspecialchars( var_export( $phpmailer, true ) );

    $error = null;
    try
    {
        $sent = $phpmailer->Send();
        ! $sent AND $error = new WP_Error( 'phpmailerError', $sent->ErrorInfo );
    }
    catch ( phpmailerException $e )
    {
        $error = new WP_Error( 'phpmailerException', $e->errorMessage() );
    }
    catch ( Exception $e )
    {
        $error = new WP_Error( 'defaultException', $e->getMessage() );
    }

    if ( is_wp_error( $error ) )
        return printf(
            "%s: %s",
            $error->get_error_code(),
            $error->get_error_message()
        );
}


//////////
/////PAGE
//////////

if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'menu_title' 	=> 'IDA SMTP',
		'page_title' 	=> 'IDA SMTP',
		'parent_slug' 	=> 'tools.php',
	));
}


////////////
//////ACF Call
////////////

if( function_exists('acf_add_local_field_group') ):
	acf_add_local_field_group(array(
		'key' => 'group_59383bb5bd1eb',
		'title' => 'Configuración : Envío SMTP',
		'fields' => array(
			array(
				'key' => 'field_5beec439bba51',
				'label' => 'Activar SMTP',
				'name' => 'smtp_mailing',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_59383b0de3d56',
				'label' => 'SMTP Auth',
				'name' => 'smtp_auth',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_59383b6ce3d5a',
				'label' => 'SMTP Secure',
				'name' => 'smtp_secure',
				'type' => 'button_group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					0 => 'none',
					'ssl' => 'SSL',
					'tls' => 'TSL',
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
			array(
				'key' => 'field_59383b4de3d58',
				'label' => 'SMTP Username',
				'name' => 'smtp_username',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_59383b5ae3d59',
				'label' => 'SMTP Password',
				'name' => 'smtp_password',
				'type' => 'password',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array(
				'key' => 'field_59383b2de3d57',
				'label' => 'SMTP Host',
				'name' => 'smtp_host',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_59383bafe3d5b',
				'label' => 'SMTP Port',
				'name' => 'smtp_port',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-ida-smtp',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
endif;
?>