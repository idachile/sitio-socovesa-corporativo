<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>
		<?php bloginfo('name'); ?> |
		<?php
		global $post;
		is_front_page() ? bloginfo('description') : wp_title('');
		?>
	</title>
	<?php if (is_page('repositorio')) : ?>
		<!-- Elemento necesario para el funcionamiento de Angular -->
		<base href="/">
		<!-- Elemento necesario para el funcionamiento de Angular -->
	<?php endif; ?>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>
	<!--[if IE]>
	<style>.no-ie { display:none !important; }</style>
	<![endif]-->
	<?php the_field('google_analytics', 'options'); ?>
	<?php the_field('google_ads', 'options'); ?>
	<?php the_field('facebook_pixel', 'options'); ?>
	<?php if (!empty(get_field('search_console', 'options'))) : ?>
		<meta name="google-site-verification" content="<?php the_field('search_console', 'options'); ?>" />
	<?php endif; ?>
</head>

<body <?php body_class('no-scrolled'); ?> data-home="<?php echo home_url(); ?>" data-path="<?php bloginfo('template_directory'); ?>/">