<?php get_header();?>
<?php
global $pid;
the_post();
$pid = get_the_ID();
$post = get_post($pid);
?>
<main id="top">
	<section class="horizon horizon--header" data-horizon>
		<div class="container">
			<figure class="single__thumbnail" data-rellax data-rellax-speed="-2">
				<?php the_post_thumbnail( 'big_1200x700', array( 'class' => 'cover-img hide@small' ) ); ?>
				<?php the_post_thumbnail( 'medium_800x600', array( 'class' => 'cover-img show@small' ) ); ?>
			</figure>
		</div>
	</section>

	<section class="horizon horizon--content bg-white" data-horizon>
		<div class="container">
			<div class="row">
				<div class="gr-3 gr-2@large hide@small no-gutter-right">
					<aside class="aside aside__date" data-area-name="desktop-date">
						<div class="single__date" data-mutable="small-down" data-desktop-area="desktop-date" data-mobile-area="mobile-date">
							<h4 class="single__date__day"><?php echo date_i18n('d'); ?></h4>
							<span class="single__date__month"><?php echo date_i18n('F'); ?></span>
							<span class="single__date__year"><?php echo date_i18n('Y'); ?></span>
						</div>
					</aside>
				</div>

				<div class="gr-9 gr-10@large gr-12@small no-gutter-left gutter@small">
					<section class="single__body" >
						<div class="aside__date show@tablet" data-area-name="mobile-date"></div>
						<div class="single__header">
							<p class="single__meta"><?php echo wp_get_post_terms($pid, 'category')[0]->name; ?></p>
							<h1 class="single__title"><?php the_title() ?></h1>
						</div>
						<div class="single__content"><?php the_content() ?></div>
					</section>
				</div>
			</div>
		</div>
	</section>
	<?php global $imagenes; $imagenes=get_field('galeria_imagenes'); ?>
	<?php if(!empty($imagenes)): ?>
	<?php get_template_part('partials/single/galeria','imagenes' );?>
	<?php endif; ?>
	<?php get_template_part('partials/single/otras','noticias'); ?>
</main>
<?php get_footer(); ?>
