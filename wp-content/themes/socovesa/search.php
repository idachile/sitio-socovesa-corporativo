<?php get_header(); ?>
<?php global $wp_query; ?>
<main>
	<section class="horizon">
		<div class="container">
			<div class="row">
				<div class="gr-6 gr-12@tablet">
					<h1 class="single__title"><?php the_field('titulo_busqueda', 'options'); ?></h1>
					<?php
					if($bajada = get_field('bajada_busqueda', 'options')):
						$bajada = str_replace('%total%', $wp_query->found_posts, $bajada);
						$bajada = str_replace('%termino%', $wp_query->query["s"], $bajada);
						echo '<div class="single__excerpt">'. apply_filters('the_content', $bajada). '</div>';
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="horizon horizon--content">
		<div class="container">
			<div class="row">
				<div class="gr-8 gr-12@tablet">

					<?php if ( have_posts() ) :?>
						<section class="single__content">
							<?php while ( have_posts() ) : the_post(); ?>
								<article class="box box--search space-bottom">
									<p class="box__meta">
										<?php
											$categoryName = wp_get_post_terms($post->ID, 'category');
											if(!empty($categoryName[0]->name)):
												echo $categoryName[0]->name;
											else:
												$postName = get_post_type($post->ID);
												$postName = get_post_type_object($postName);
												echo $postName->labels->singular_name;
											endif;
										?>
									</p>
									<h3 class="box__title no-margin-top"><a href="<?php the_permalink();?>" title="Ir a <?php the_title();?>" class="no-touch" data-touch="false"><?php the_title();?></a></h3>
									<div class="box__excerpt">
										<?php if(get_the_excerpt()){ the_excerpt();}else{echo random_text(rand(100,200));} ?>
									</div>
								</article>
							<?php endwhile;?>
							<?php echo get_pagination($wp_query); ?>
						</section>
					<?php else : ?>
						<?php if($mensaje_error = get_field('mensaje_error_busqueda', 'options')): ?>
						<section class="form-parent" data-parent>
							<div class="response">
								<figure class="response__icon"><span class="icon icon-close"></span></figure>
								<div class="response__body">
									<h3 class="response__title"><?php the_field('titulo_error_busqueda', 'options'); ?></h3>
									<?php echo '<div class="response__excerpt">'. apply_filters('the_content', $mensaje_error) .'</div>'; ?>
								</div>
							</div>
						</section>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>