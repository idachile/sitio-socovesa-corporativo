<?php
class formularios{
  function __constructor(){
  }
  function handler($input,$value, $pid = false){
    $input['required_ida']=$input['required_ida']==1?"required":"";
    
		if ($input["type"]=='textarea'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->textarea($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='text'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      if($input["name"]=='rut'){
        echo $this->rut($input,$value);
      }elseif($input["name"]=='tema'){
        echo $this->tema($input,$pid);
      }elseif($input["name"]=='telefono'){
        echo $this->telefono($input,$value);
      }else{
        echo $this->input($input,$value);
      }
      echo '</div>';
    }
    if ($input["type"]=='checkbox' or $input["type"]=='true_false'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->checkbox($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='date_picker'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->date_picker($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='password'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->password($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='email'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->email($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='file'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'" data-role="deploy-target">';
      echo $this->file($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='select'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->select($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='oembed'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      echo $this->oembed($input,$value);
      echo '</div>';
    }
    if ($input["type"]=='repeater'){
      echo '<div class="form-control '.$input["wrapper"]["class"].'">';
      $this->repeater($input, $pid);
      echo '</div>';
    }

  }

  function input($data,$value){
    $style = '';
    if($data['prepend']=="disabled"){
      $style = 'style="border:0;background:#fff;"';
    }
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input '.$style.' data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="text" name="'.$data['key'].'" value="'.$value.'" placeholder="'.$data['placeholder'].'" data-custom-validation="onlyString" '.$data['required_ida'].' />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  function telefono($data,$value){
    $style = '';
    if($data['prepend']=="disabled"){
      $style = 'style="border:0;background:#fff;"';
    }
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input '.$style.' data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="tel" name="'.$data['key'].'" value="'.$value.'" placeholder="'.$data['placeholder'].'" maxlength="12" data-custom-validation="onlyNumbers" '.$data['required_ida'].' />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
	function checkbox($data,$value){
		$out =  '';
    if($value!=0){
      $checked="checked";
    }
    if($data['tooltip']==true){
      // $tooltip = ' <i class="fas fa-question-circle" data-tooltips="'.$data['instructions'].'"></i>';
      $instructions = '';
    }
    else{
      // $tooltip="";
      $instructions = '<small class="form-control__text"> '.$data['instructions'].'</small>';
    }

		$out .= 	'<label class="form-control__label" style="width:50%; float:left;padding: .5em 0;">'.$data['label'].'</label>';
		$out .= 	'<label class="form-switch form-switch--block" style="width:50%; float:left;">';
		$out .= 		'<input data-name="'.$data['name'].'" type="checkbox" '.$checked.' name="'.$data['key'].'" value="2">';
		$out .= 		'<span class="form-switch__input form-switch__input--toggle" data-ontrue="si" data-onfalse="no"></span>';
		$out .= 	'</label>';
		$out .= $instructions;
		return $out;
	}
  function password($data,$value){
    if($data['name'] == "clave_repeat"){
      $equal =' data-custom-validation="equalTo" data-sample="clave" ';
    }
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input data-name="'.$data['name'].'" tabindex="1'. $data['menu_order'] .'" class="form-control__field" type="password"  id="'.$data['name'].'" name="'.$data['name'].'" '.$equal.' placeholder="'.$data['placeholder'].'" '.$data['required_ida'].'  />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  function oembed($data,$value){
    global $post;
    preg_match('/src="(.+?)"/', $value, $matches);
    $src  = str_replace('?feature=oembed','',$matches[1]);
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="text" name="'.$data['key'].'" value="'.$src.'" placeholder="'.$data['placeholder'].'" '.$data['required_ida'].' />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    if($src) {$out .= '<div>'.$value.'</div>';}
    return  $out;
  }
  function rut($data,$value){
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="text" name="'.$data['key'].'" value="'.$value.'" placeholder="'.$data['placeholder'].'" '.$data['required_ida'].' data-custom-validation="validRut" data-autoformat="true"   />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  function file($data,$value){
    $required = '';
    if ($value == ''){
      $required = $data['required_ida'];
    }
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<div class="form-control__file">';
    $out .= '<button class="form-control__file__btn">Subir archivo';
    $out .= '<input data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__file__input" type="file" data-custom-validation="fileValidate" data-max-size="'.$data['max_size'].'MB"  data-error-message="Archivo demasiado pesado" accept="'.$data['accept_file'].'" name="'.$data['key'].'" placeholder="'.$data['placeholder'].'" '.$required.'/>';
    $out .= '</button>';
    $out .= '<span class="form-control__file__text" data-role="file-text">Ningún archivo seleccionado</span>';
    $out .= '</div>';
    $out .= '<span class="form-control__text"> '.$data['instructions'].'</span>';

    if ($value){
      $out .= '<div><img src="'.$value['icon'].'" width="3%" alt="icono de archivo"/> <a href="'.$value['url'].'" title="Descargar archivo">'.$value['title'].'</a> </div>';
    }

    return  $out;
  }
  function date_picker($data,$value){
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="date" name="'.$data['key'].'" value="'.$value.'" placeholder="'.$data['placeholder'].'" '.$data['required_ida'].' />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  function email($data,$value){
    $out  = '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<input data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field" type="email" name="'.$data['key'].'" value="'.$value.'" placeholder="'.$data['placeholder'].'" '.$data['required_ida'].' />';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  function select($data,$value){
    $choices = '<option value="" selected>Selecciona una opción</option>';;
    foreach($data["choices"] as $key => $text){

      if($key==$value){
        $seleccionada=" selected ";
      }
      else{
        $seleccionada=" ";
      }

      $choices .= '<option '.$seleccionada.'value="'.$key.'">'.$text.'</option>';
    }

    if($data['tooltip']==true){
      $tooltip = ' <i class="fas fa-question-circle" data-tooltips="'.$data['instructions'].'"></i>';
      $instructions = '';
    }
    else{
      $tooltip="";
      $instructions = '<small class="form-control__text"> '.$data['instructions'].'</small>';
    } 
    $out  = '<label class="form-control__label">'.$data['label'].$tooltip.'</label>';
    $out .= '<select data-ponderacion="'.$data['wrapper']['id'].'" data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field--select form-control__field " name="'.$data['key'].'" '.$data['required_ida'].'>'.$choices.'</select>';
    $out .=  $instructions;

    return  $out;
  }
  function textarea($data,$value){
    $out = '<div style="position: relative">';
    $out  .= '<label class="form-control__label">'.$data['label'].'</label>';
    $out .= '<textarea data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" name="'.$data['key'].'" rows="'.$data['rows'].'" '.$data['required_ida'].' data-textcounter data-role="textcounter" maxlength="'.$data['maxlength'].'" class="form-control__field" placeholder="'.$data['placeholder'].'" data-autoresize>'.$value.'</textarea>';
    if(!empty($data['maxlength'])):
      $out .= '<p class="form-control__text form-control__text__countdown" style="position: absolute; bottom: .25rem; right: .5rem;">Caracteres restantes: <strong data-role="countdown">'.(intval($data['maxlength']) - strlen($value)).'</strong></p>';
    endif;
    $out .= '</div>';
    $out .= '<small class="form-control__text"> '.$data['instructions'].'</small>';
    return  $out;
  }
  
  function tema($data,$pid){

    $opciones = get_field('tema_a_consultar', $pid);

    $choices = '<option value="" selected disabled>'.$data['placeholder'].'</option>';
    $active_select = '';
    foreach($opciones as $opcion){
      $choices .= '<option value="'.$opcion['tema'].'">'.$opcion['tema'].'</option>';
      if($opcion['adjuntar_archivo']):
        $active_select = $opcion['tema'];
      endif;
    }

    if(isset($data['tooltip']) && $data['tooltip']==true){
      $tooltip = ' <i class="fas fa-question-circle" data-tooltips="'.$data['instructions'].'"></i>';
      $instructions = '';
    }
    else{
      $tooltip="";
      $instructions = '<small class="form-control__text"> '.$data['instructions'].'</small>';
    }
    $out  = '<label class="form-control__label">'.$data['label'].$tooltip.'</label>';
    $out .= '<select  data-name="'.$data['name'].'" tabindex="'.$data['menu_order'].'" class="form-control__field--select form-control__field " name="'.$data['key'].'" '.$data['required_ida'].' data-role="deploy-select" data-deployon="'.$active_select.'">'.$choices.'</select>';
    $out .=  $instructions;

    return  $out;
  }
  function wysiwyg(){
  }
  function radio(){
  }
}
global $form;
$form = new formularios();
?>
