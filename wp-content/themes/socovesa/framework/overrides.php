<?php
//Theme support
add_action('after_setup_theme', 'catsup_setup');
function catsup_setup()
{
	if (get_field("option_feed", "option") == true) {
		add_theme_support('automatic-feed-links');
	}
	if (get_field("option_thumbnails", "option") == true) {
		add_theme_support('post-thumbnails');
	}
	if (get_field("option_html5", "option") == true) {
		add_theme_support('html5');
	}
	if (get_field("option_custom_header", "option") == true) {
		add_theme_support('custom-header');
	}
	if (get_field("option_post_format", "option") == true) {
		add_theme_support('post-formats', array('aside', 'video', 'image'));
	}
	if (get_field("option_title_tag", "option") == true) {
		add_theme_support('title-tag');
	}
	if (get_field("option_page_excerpt", "option") == true) {
		add_post_type_support('page', 'excerpt');
	}
	if (get_field("enable_admin_bar", "option") == false && !current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}

	if (get_field("option_menus", "option") == true) {
		add_theme_support('menus');
		register_nav_menus(array(
			"menu-principal" => "Este es el principal header",
			"footer-principal" => "Este es el menu principal del footer",

			"main-menu" => "Este es el principal header - English",
			"main-footer" => "Este es el menu principal del footer - English",
		));
	}
}

add_action('customize_register', 'catsup_customize_register');
function catsup_customize_register($wp_customize)
{
	// $wp_customize->remove_section( 'title_tagline' );
	$wp_customize->remove_section('static_front_page');
	$wp_customize->remove_section('custom_css');
}

//																																							Tamaños de thumbnails
// @todo hacerlo configurable desde el admin
add_image_size('thumb-landing-ebook', 540, 540, true); // Imagen destacada de landing Ebook
add_image_size('dossier_1280x700', 1280, 700, true);
add_image_size("big_1024x576", 1024, 576, true);
add_image_size("portada_1024x360", 1024, 360, true);
add_image_size("medium_640x360", 640, 360, true);
add_image_size("medium_560x590", 560, 590, true);
add_image_size("medium_400x450", 400, 450, true);
add_image_size("medium_500x800", 500, 800, true);
add_image_size("small_360x202", 360, 202, true);
add_image_size("avatar_3x4", 360, 540, true);
add_image_size("avatar_360x450", 360, 450, true);
add_image_size("avatar_100", 100, 100, true);
add_image_size("square_360", 360, 360, true);
add_image_size("square_300", 300, 300, true);
add_image_size("banner_1136x243", 1136, 243, true);
add_image_size("slider_1280x627", 1280, 627, true);
add_image_size("slider_1024x820", 1024, 820, true);


/**
 * Quita los menus por defecto de la barra superior
 */
add_action('admin_bar_menu', 'catsup_remove_top_nodes', 999);
function catsup_remove_top_nodes()
{
	global $wp_admin_bar;
	$wp_admin_bar->remove_node('new-post');
	$wp_admin_bar->remove_node('new-page');
	$wp_admin_bar->remove_node('new-revista');
	$wp_admin_bar->remove_node('new-user');
	$wp_admin_bar->remove_node('new-media');
	$wp_admin_bar->remove_node('new-tablepress');
	$wp_admin_bar->remove_node('new-link');
	$wp_admin_bar->remove_node('new-content');
	$wp_admin_bar->remove_node('comments');
	$wp_admin_bar->remove_node('wp-logo');
}
//																																							soporte para SVG
function custom_myme_types($mime_types)
{
	$mime_types['json'] = 'application/json';
	$mime_types['svg'] = 'image/svg+xml';
	return $mime_types;
}
add_filter('upload_mimes', 'custom_myme_types', 1, 1);

if (function_exists('acf_add_options_page')) {
	$opciones_catsup =  acf_add_options_page(
		array(
			'page_title' => 'Opciones Sitio',
			'menu_title' => 'Opciones Sitio',
			'menu_slug' => 'opciones_sitio',
			'capability' => 'edit_posts',
			'position' => false,
			'parent_slug' => '',
			'icon_url' => false,
			'redirect' => true,
			'post_id' => 'options',
			'autoload' => false
		)
	);

	acf_add_options_sub_page(array(
		'menu_title' 	=> 'Generales',
		'page_title' 	=> 'Generales',
		'parent_slug' 	=> $opciones_catsup['menu_slug'],
	));

	acf_add_options_sub_page(
		array(
			'page_title' 	=> 'Globales',
			'menu_title' 	=> 'Globales',
			'parent_slug' 	=> $opciones_catsup['menu_slug']
		)
	);

	acf_add_options_sub_page(array(
		'menu_title' 	=> 'Formularios',
		'page_title' 	=> 'Opciones globales de Formularios',
		'parent_slug' 	=> $opciones_catsup['menu_slug'],
	));

	$opciones_traduccion =  acf_add_options_page(
		array(
			'page_title' => 'Opciones Traducción',
			'menu_title' => 'Opciones Traducción',
			'menu_slug' => 'opciones_traduccion',
			'capability' => 'edit_posts',
			'position' => false,
			'parent_slug' => '',
			'icon_url' => 'dashicons-translation',
			'redirect' => true,
			'post_id' => 'options',
			'autoload' => false
		)
	);

	acf_add_options_sub_page(array(
		'menu_title' 	=> 'General',
		'page_title' 	=> 'Opciones generales de Traducción',
		'parent_slug' 	=> $opciones_traduccion['menu_slug'],
	));
	acf_add_options_sub_page(array(
		'menu_title' 	=> 'Global',
		'page_title' 	=> 'Opciones globales de Traducción',
		'parent_slug' 	=> $opciones_traduccion['menu_slug'],
	));
}

function set_robots()
{
	$robot = '';
	if ((strpos(home_url(), 'dev.') !== false) || (strpos(home_url(), 'qa.') !== false)) :
		$robot = '<meta name="robots" content="noindex, nofollow">';
	endif;

	echo $robot;
}


/**
 * Get rid of tags on posts.
 */
function unregister_tags()
{
	unregister_taxonomy_for_object_type('post_tag', 'post');
	// unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action('init', 'unregister_tags');


//////////
// Disable support for comments and trackbacks in post types
function disable_comments_post_types_support()
{
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if (post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'disable_comments_post_types_support');

add_action('admin_menu', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
	remove_menu_page('edit-comments.php');
}


//////////
// Close comments on the front-end
function disable_comments_status()
{
	return false;
}
add_filter('comments_open', 'disable_comments_status', 20, 2);
add_filter('pings_open', 'disable_comments_status', 20, 2);


//////////
// Hide existing comments
function disable_comments_hide_existing_comments($comments)
{
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'disable_comments_hide_existing_comments', 10, 2);




///////////
// Redirect any user trying to access comments page
function disable_comments_admin_menu_redirect()
{
	global $pagenow;
	if ($pagenow === 'edit-comments.php') :
		wp_redirect(admin_url());
		exit;
	endif;
}
add_action('admin_init', 'disable_comments_admin_menu_redirect');


/////////
// Remove comments metabox from dashboard
function disable_comments_dashboard()
{
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'disable_comments_dashboard');


//////////
// Remove comments links from admin bar
function disable_comments_admin_bar()
{
	if (is_admin_bar_showing()) :
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	endif;
}
add_action('init', 'disable_comments_admin_bar');




/////////////////////// Incrusta Estilos y Javascript en el <head>
remove_action('wp_head', 'print_emoji_detection_script', 7);
add_action("wp_enqueue_scripts", "incrustar_scripts", 20);
function incrustar_scripts()
{
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', [], '1', false);
		wp_enqueue_script('jquery');
		wp_register_script('mainJS', get_bloginfo('template_directory') . '/dist/js/main.min.js', ['jquery'], '1', true);
		wp_enqueue_script('mainJS');

		wp_register_script('canvasjs',  get_bloginfo('template_directory') . "/dist/js/libs/canvasjs.min.js", [], '1', true);
		wp_register_script('chart_spline_area',  get_bloginfo('template_directory') . "/dist/js/extra/chart_spline_area.js", [], '1', true);
		wp_register_script('chart_doughnout',  get_bloginfo('template_directory') . "/dist/js/extra/chart_doughnout.js", [], '1', true);
		wp_register_script('chart_column',  get_bloginfo('template_directory') . "/dist/js/extra/chart_column.js", [], '1', true);
		wp_register_script('chart_multitype',  get_bloginfo('template_directory') . "/dist/js/extra/chart_multitype.js", [], '1', true);
		wp_register_script('chart_simpleline',  get_bloginfo('template_directory') . "/dist/js/extra/chart_simple_line.js", [], '1', true);

		wp_register_script('angular_scripts',  get_bloginfo('template_directory') . "/angular/angular_scripts.js", [], '1', true);

		if (is_page('inversionistas')) :
			wp_enqueue_script('canvasjs');
			wp_enqueue_script('chart_spline_area');
			wp_enqueue_script('chart_doughnout');
			wp_enqueue_script('chart_column');
			wp_enqueue_script('chart_column');
			wp_enqueue_script('chart_multitype');
		endif;

		wp_register_script('jquery_ui_datepicker',  get_bloginfo('template_directory') . "/dist/js/libs/jquery-ui.min.js", ['jquery'], '1', true);

		wp_register_script('rellax', get_bloginfo('template_directory') . '/dist/js/libs/relax.vanilla.js', ['mainJS'], '1', true);
	}
}


//// metemos el estilo principal en el footer para dejar feliz a pagespeed insights
add_action('wp_print_styles', 'incrustar_estilos');
function incrustar_estilos()
{
	wp_enqueue_style('mainCSS', get_bloginfo('template_directory') . '/dist/css/main.css', [], '1', 'all');
	wp_enqueue_style('fontMontserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800', [], '1', 'all', true);
}

add_filter('jetpack_implode_frontend_css', '__return_false');
add_action('wp_print_styles', 'sacar_estilos_extra');
function sacar_estilos_extra()
{
	if (!is_admin()) {
		// Saco los estilos de jetpack en el front
		wp_deregister_style('AtD_style'); // After the Deadline
		wp_deregister_style('jetpack_likes'); // Likes
		wp_deregister_style('jetpack_related-posts'); //Related Posts
		wp_deregister_style('jetpack-carousel'); // Carousel
		wp_deregister_style('grunion.css'); // Grunion contact form
		wp_deregister_style('the-neverending-homepage'); // Infinite Scroll
		wp_deregister_style('infinity-twentyten'); // Infinite Scroll - Twentyten Theme
		wp_deregister_style('infinity-twentyeleven'); // Infinite Scroll - Twentyeleven Theme
		wp_deregister_style('infinity-twentytwelve'); // Infinite Scroll - Twentytwelve Theme
		wp_deregister_style('noticons'); // Notes
		wp_deregister_style('post-by-email'); // Post by Email
		wp_deregister_style('publicize'); // Publicize
		wp_deregister_style('sharedaddy'); // Sharedaddy
		wp_deregister_style('sharing'); // Sharedaddy Sharing
		wp_deregister_style('stats_reports_css'); // Stats
		wp_deregister_style('jetpack-widgets'); // Widgets
		wp_deregister_style('jetpack-slideshow'); // Slideshows
		wp_deregister_style('presentations'); // Presentation shortcode
		wp_deregister_style('jetpack-subscriptions'); // Subscriptions
		wp_deregister_style('tiled-gallery'); // Tiled Galleries
		wp_deregister_style('widget-conditions'); // Widget Visibility
		wp_deregister_style('jetpack_display_posts_widget'); // Display Posts Widget
		wp_deregister_style('gravatar-profile-widget'); // Gravatar Widget
		wp_deregister_style('widget-grid-and-list'); // Top Posts widget
		wp_deregister_style('jetpack-widgets'); // Widgets
	}
}

/*Async load scripts*/
// Async load
function st_async_load_scripts($url)
{
	if (strpos($url, '#asyncscript') === false) {
		return $url;
	} elseif (is_admin()) {
		return str_replace('#asyncscript', '', $url);
	}
	return str_replace('#asyncscript', '', $url) . "' async='async" . "' defer='defer";
}
add_filter('clean_url', 'st_async_load_scripts', 11, 1);


add_filter('script_loader_tag', 'add_id_to_script', 10, 3);
function add_id_to_script($tag, $handle, $src)
{
	if ('angular_scripts' === $handle) {
		$tag = '<script type="module" src="' . $src . '"></script>';
	}
	return $tag;
}


/////////////////////// Saca los query strings de los css y js, ayuda al page speed
add_filter('style_loader_src', 'vc_remove_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'vc_remove_wp_ver_css_js', 9999);
function vc_remove_wp_ver_css_js($src)
{
	if (strpos($src, 'ver=')) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}

/**
 * Agrega clases personalidas en el <body>
 * @param array $classes clases para el elemento body
 * @return array
 */
function tema_body_classes($classes)
{
	if (!is_singular()) {
		$classes[] = 'hfeed';
	}
	return $classes;
}
add_filter('body_class', 'tema_body_classes');



function google_api_key()
{
	return get_field('google_api_key', 'options');
}


function my_acf_init()
{
	acf_update_setting('google_api_key', google_api_key());
}
add_action('acf/init', 'my_acf_init');



// add_filter('pre_site_transient_update_core','remove_core_updates');
// add_filter('pre_site_transient_update_plugins','remove_core_updates');
// add_filter('pre_site_transient_update_themes','remove_core_updates');
add_action('admin_head', 'remove_update_nag');

function remove_update_nag()
{
	remove_action('admin_notices', 'update_nag', 3);
}
setlocale(LC_ALL, 'es_CL');

add_action('acf/render_field_settings/type=table', 'catsup_render_field_settings_table');
function catsup_render_field_settings_table($field)
{
	acf_render_field_setting($field, array(
		'label'			=> __('Table Footer'),
		'instructions'	=> 'Opcional',
		'name'			=> 'no_label',
		'type'			=> 'true_false',
		'ui'			  => 1,
		'default'   => true
	), true);
}


add_action('pre_get_posts', 'modify_main_query');
function modify_main_query($query)
{
	if (!is_admin()) :
		if ($query->is_search()) {
			$query->set('post_type', array('post', 'persona', 'page'));
			$query->set('posts_per_page', 10);
			$query->set('post_status', 'publish');
			$query->set('post__not_in', array(361, 1155, 1157, 75, 78, 79)); //contactos / exitos / errores
		}

		if ($query->is_home() && $query->is_main_query()) {
			$query->set('post_type', array('post'));
			$query->set('posts_per_page', 10);
			$query->set('post_status', 'publish');

			if (isset($_GET['month']) && $_GET['month']) :
				$query->set('monthnum', $_GET['month']);
			endif;

			if (isset($_GET['year']) && $_GET['year']) :
				$query->set('year', $_GET['year']);
			endif;

			if (isset($_GET['tipo']) && $_GET['tipo']) :
				$query->set('tax_query', array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => $_GET['tipo'])));
			endif;

			if ($query->is_home()) {
				$noticias_id = 360;
				$destacado = get_field('destacado', $noticias_id);

				if (!empty($destacado)) {
					$query->set('post__not_in', array($destacado[0]));
				}
			}
		}
	endif;
}

function region_in_array($region_slug, $region_array)
{
	$return = false;
	if (!empty($region_array)) :
		foreach ($region_array as $region) {
			if ($region['region'] == $region_slug) {
				$return = true;
			}
		}
	endif;

	return $return;
}

function get_title_by_region($region_slug, $region_array)
{
	$title = false;
	if (!empty($region_array)) :
		foreach ($region_array as $region) {
			if ($region['region'] == $region_slug) {
				$title = $region['titulo'];
			}
		}
	endif;

	return $title;
}

function region_is_bold($region_slug, $region_array)
{
	$return = 'normal';
	if (!empty($region_array)) :
		foreach ($region_array as $region) {
			if ($region_slug == $region['region'] && $region['activa']) {
				$return = 'bold';
			}
		}
	endif;

	return $return;
}

function region_is_active($region_slug, $region_array)
{
	$return = 'preview';
	if (!empty($region_array)) :
		foreach ($region_array as $region) {
			if ($region_slug == $region['region'] && $region['activa']) {
				$return = 'active';
			}
		}
	endif;

	return $return;
}

function has_children($post_id)
{
	$query = new WP_Query(array('post_parent' => $post_id, 'post_type' => 'any', 'posts_per_page' => -1, 'post_status' => 'publish'));
	return $query->have_posts();
}

/**
 * Págnia con generador de nonces para saltarse el cache con ESI Varnish
 */
add_action('init', 'esi_nonce');
function esi_nonce()
{
	add_rewrite_rule('esi-nonce$', 'index.php?esi=nonce', 'top');
}
/// anade los query vars customizados a la wp_query
add_filter('query_vars', 'esi_query_vars');
function esi_query_vars($vars)
{
	$vars[] = 'esi';
	return $vars;
}

// setea el redirect al template de el archivo de sedes
add_filter('template_redirect', 'esi_template_redirect');
function esi_template_redirect()
{
	if (get_query_var('esi') === 'nonce') {
		get_template_part('framework/esi-nonce');
		exit;
	}
}
?>