<?php
class persona{
	function __constructor(){ }

	function query($query){
		$args = shortcode_atts(array(
			'post_type' =>  'post',
			'post_status' =>  'publish',
			'paged' =>  false,
			'tax_query' => false,
			'meta_query' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post__in'=>false,
			'post__not_in'=>false,
			'offset' => false,
			'author'=>false
		), $query);

		$q = new WP_Query($args);
		return $q;
	}


  	function get($args, $setting){
		$q = $this->query($args);
		$lineas = ceil($q->post_count / 3); //contador de líneas
		$data['lineas_tablet'] = ceil($q->post_count / 2); //contador de líneas

		$final_random_imagenes = $setting['random_imagenes'];

		$insert_random_bg = $insert_random_img = false;

		$out = "";
		$i=0;
		$random_count =  rand(1,5);
		$random_box = rand(0,1);

		$data['counter'] = $q->found_posts;
		$data['persona'] = 0;
		$data['persona_linea'] = 1;
		$data['persona_linea_tablet'] = 1;
		$data['random_box'] = 0;
		$contador = 1;

		$contador_linea = 0;

		if( $q->have_posts() ):
			$data['linea'] = 1;
			$color = '';
			while( $q->have_posts() ) : $q->the_post();

				$random_box = rand(0,1);

				$data['title']=get_the_title($q->post->ID);
				$data['background_image']=get_the_post_thumbnail_url($q->post->ID, 'grid_300');
				$data['permalink']=get_permalink($q->post->ID);
				$data['expand']=get_field('expand',$q->post->ID);
				$data['bajada']= has_excerpt($q->post->ID) ? get_the_excerpt($q->post->ID) : cut_string_to($q->post->post_content, 240);
				$data['cargo']=strtoupper(get_field('cargo',$q->post->ID));

				$data['random_count'] =$random_count;
				// $data['linea']=$linea;
				$data['lineas']=$lineas;

				$data['random_count'] =$random_count;
				$data['number']=$contador;

				if($i <= 2 && !$insert_random_img && $random_box == 0):
					if(empty($final_random_imagenes)):
						$final_random_imagenes = $setting['random_imagenes'];
					endif;
					
					if(!empty($final_random_imagenes)){
						shuffle($final_random_imagenes);
						$data['random_imagen'] = array_shift($final_random_imagenes);
					}

					$out .= $this->gridbox_random_image($data);
					$insert_random_img = true;
					$contador++;
					$data['number']++;
					$data['random_box']++;
					$contador_linea++;
				endif;

				if((($contador % 5) == 1) && $contador != 1){
					$data['random_box'] = 1;
				}
				
				if($i <= 2 && !$insert_random_bg && $random_box == 1):
					if($color==='bg_1' && isset($color)){
						$color='bg_2';
					}
					else if($color==='bg_2' && isset($color)){
						$color='bg_1';
					}
					if(!isset($color)){
						$color = 'bg_'.rand(1,2);
					}

					$data['color']=$color;
					$out .= $this->gridbox_random_background($data);
					$insert_random_bg = true;
					$contador++;
					$data['number']++;
					$data['random_box']++;
					$contador_linea++;
				endif;

				if((($contador % 5) == 1) && $contador != 1){
					$data['random_box'] = 1;
				}

				if($insert_random_img == false && $insert_random_bg == false){
					$contador_linea++;
				}

				if($contador_linea >= 5):
					$data['linea']++;
					$contador_linea = 1;
				else:
					$contador_linea++;
				endif;

				if((($contador - 1) % 5 ) == 0){
					$data['persona_linea']++;
				}
				
				$data['persona']++;
				$data['persona_linea_tablet'] = ceil( $data['persona'] / 2);
				$out .= $this->gridbox_persona($data);

				//reset
				if($i == 2):
					$i = 0;
					// $linea++;
					$insert_random_bg = false;
					$insert_random_img = false;
				else:
					$i++;
				endif;

				if((($contador % 5) == 1) && $contador != 1){
					$data['random_box'] = 1;
				}

				// printme($data['random_box']);
				
				$contador++;
				$data['random_box']++;

			endwhile;
		endif;
		wp_reset_query();
		

		return $out;
	}

	  
  function gridbox_persona($data){
	  ($data['number'] % 5) == 1 ? $start = true : $start = false;

		$lineas = $data['lineas'];
		$start_linea = ceil($lineas / 2);

		if($lineas > 1 && $data['linea'] >= $start_linea && $data['linea'] > 1):
			$horizon_data = '-bottom';
		else:			
			$horizon_data = '-top';
		endif;

		if(!empty($data['random_box']) && $data['random_box'] >=3):
			$vertical_data = 'right';
		else:
			$vertical_data = 'left';
		endif;

		if($data['linea'] > $start_linea && $data['number'] != 1 && $start):
			$horizon_data = '-bottom';			
		endif;

		if($data['number'] != 1 && $start):	
			$vertical_data = 'left';
		endif;



		$data['expand'] = $vertical_data.$horizon_data;


		if(($data['persona'] % 2) == 0){
			$vertical_data_tablet = 'right';
		}else{
			$vertical_data_tablet = 'left';
		}

		$middle_lineas_tablet = ceil($data['lineas_tablet'] / 2);

		if($data['persona_linea_tablet'] <= $middle_lineas_tablet){
			$horizon_data_tablet = '-top';
		}else{
			$horizon_data_tablet = '-bottom';
		}

		$data['expand_tablet'] = $vertical_data_tablet . $horizon_data_tablet;

	 $out = "";
   //  $out .='<div data-rand_box="'.$data['random_box'].'"  data-rand_count="'.$data['random_count'].'" data-grid-num="'.$data['number'].'" data-grid-linea="'.$data['linea'].'" class="grid-item">';
    $out .='<div data-grid-num="'.$data['number'].'" data-grid-linea="'.$data['linea'].'" data-random="'.$data['random_box'].'" data-persona="'.$data['persona'].'" class="grid-item">';
    $out .='  <article class="box box--grid box--expand" data-bg="background-image: url('.$data['background_image'].')" data-expand="'.$data['expand'].'" data-expand-tablet="'.$data['expand_tablet'].'">';
	 $out .='    <div class="box__body text-light">';
	 $out .=			'<p class="box__meta text-upper">'.$data['cargo'].'</p>';
	 $out .='      <h3 class="box__title">'.$data['title'].'</h3>';
	 if(!empty($data['bajada'])):
    $out .='      <div class="box__excerpt">';
    $out .=           $data['bajada'];
	 $out .='      </div>';
	 endif;
    $out .='    </div>';
    $out .='  </article>';
    $out .='</div>';
    return $out;
  }
  function gridbox_random_image($data){
    $out = "";
	//  $out .='<div data-rand_box="'.$data['random_box'].'"  data-rand_count="'.$data['random_count'].'" data-grid-num="'.$data['number'].'" class="grid-item hide@phablet">';
	 $out .='<div data-grid-num="'.$data['number'].'" data-grid-linea="'.$data['linea'].'" data-resto="'.$data['number'] % 5 .'" class="grid-item hide@tablet">';
	 if(!empty($data['random_imagen'])):
	 $out .='			<article class="box box--grid" style="background-image: url('.$data['random_imagen']['url'].')"></article>';
	 else:
	 $out .='			<article class="box box--grid" style="background-image: url(https://unsplash.it/500/500)"></article>';
	 endif;
    $out .='</div>';
    return $out;
  }
  function gridbox_random_background($data){
    $color_or_empty = array('bg_1'=>'bg-main-light', 'bg_2'=>'bg-dark');
	 $out = "";
	 if(!empty($data['color']) && isset($data['color'])):
	 $out .='<div data-grid-num="'.$data['number'].'" data-grid-linea="'.$data['linea'].'" data-resto="'.$data['number'] % 5 .'" class="grid-item '.$color_or_empty[$data['color']].' hide@tablet"></div>';
	 endif;

    return $out;
  }



  	function set_grid($personas, $imagenes){
		// $merged = array();

		$cant_personas = count($personas);
		$cant_random = ceil($cant_personas / 2);
		$cant_lineas = ceil($cant_personas / 3);
		// $x = 0;
		for($x=0; $x<$cant_random; $x++):
			$random_value[] = $x;
		endfor;

		// $merged = array_merge($personas, $random_value);

		$cant_elementos = $cant_personas + $cant_random;

		for($x=0; $x<$cant_random / 2; $x++):

			$random_1 = rand(1,2);
			$random_2 = rand(3,4);
			$random_1 == $random_2 ? $random_2 = rand(1,5) : $random_2 = $random_2;

			$numeros_random[$x][0] = $random_1;
			$numeros_random[$x][1] = $random_2;
		endfor;
		
		$linea = 0;
		$columna = 0;
		$random_1 = $random_2 = 0;
		for($corredor_elementos = 0; $corredor_elementos < $cant_elementos; $corredor_elementos++):

			if( (($corredor_elementos % 5) == 0) && ($corredor_elementos > 0) ):
				$columna = 1;
				$linea ++;
			else:
				$columna++;
			endif;
			$elementos_totales[$linea][$columna - 1]['contador'] = $corredor_elementos;
			$elementos_totales[$linea][$columna - 1]['linea'] = $linea;
			$elementos_totales[$linea][$columna - 1]['lineas'] = $cant_lineas-1;
			$elementos_totales[$linea][$columna - 1]['columna'] = $columna-1;

		endfor;


		$contador = 0;
		foreach($elementos_totales as $key => $fila):
			foreach($fila as $bloque):
				if(!empty($numeros_random[$key]) && !empty($numeros_random[$key][0])):
					$elementos_totales[$bloque['linea']][$bloque['columna']]['random_1'] = $numeros_random[$key][0];
				else:
					$elementos_totales[$bloque['linea']][$bloque['columna']]['random_1'] = '';
				endif;
				
				if(!empty($numeros_random[$key]) && !empty($numeros_random[$key][1])):
					$elementos_totales[$bloque['linea']][$bloque['columna']]['random_2'] = $numeros_random[$key][1];
				else:
					$elementos_totales[$bloque['linea']][$bloque['columna']]['random_2'] = '';
				endif;
				
				$contador++;
			endforeach;
		endforeach;

		$contador_persona = $contador_global = 0;
		foreach($elementos_totales as $key => $value):
			foreach($value as $cont => $elemento):
				$elementos_totales[$elemento['linea']][$elemento['columna']]['cont'] = $contador_global;

				if(!empty($elemento['random_1']) && ($cont == $elemento['random_1']) ){
					
					if(empty($final_random_imagenes)):
						$final_random_imagenes = $imagenes;
					endif;
					
					if(!empty($final_random_imagenes)){
						shuffle($final_random_imagenes);
						$imagen_celda = array_shift($final_random_imagenes);
						$elementos_totales[$elemento['linea']][$elemento['columna']]['imagen'] = $imagen_celda;
					}

					$elementos_totales[$elemento['linea']][$elemento['columna']]['persona'] = '';
					$elementos_totales[$elemento['linea']][$elemento['columna']]['tipo'] = 'imagen';
				}elseif(!empty($elemento['random_2']) && ($cont == $elemento['random_2']) ){
					if(empty($final_random_imagenes)):
						$final_random_imagenes = $imagenes;
					endif;
					
					if(!empty($final_random_imagenes)){
						shuffle($final_random_imagenes);
						$imagen_celda = array_shift($final_random_imagenes);
						$elementos_totales[$elemento['linea']][$elemento['columna']]['imagen'] = $imagen_celda;
					}

					$elementos_totales[$elemento['linea']][$elemento['columna']]['persona'] = '';
					$elementos_totales[$elemento['linea']][$elemento['columna']]['tipo'] = 'imagen';
				}else{
					$elementos_totales[$elemento['linea']][$elemento['columna']]['post_id'] = $personas[$contador_persona] .'</b>';
					$elementos_totales[$elemento['linea']][$elemento['columna']]['persona'] = $contador_persona + 1;
					$elementos_totales[$elemento['linea']][$elemento['columna']]['persona_linea_tablet'] = ceil(($contador_persona + 1) / 2);
					$elementos_totales[$elemento['linea']][$elemento['columna']]['tipo'] = 'persona';
					$contador_persona++;
				}
				$contador_global++;

				// $clean_array[] = $elemento;
			endforeach;
		endforeach;

		return $elementos_totales;
	}

	function get_grilla_personas($args, $settings){
		$personas = array();
		$print = '';
		if(!empty($args)):

			$personas_query = new WP_Query($args);
			if($personas_query->have_posts()):
				while($personas_query->have_posts()): $personas_query->the_post();
					$personas[] = $personas_query->post->ID;
				endwhile;
			endif;
			wp_reset_query();

			$grilla = $this->set_grid($personas, $settings['random_imagenes']);
			if(!empty($grilla)):
				foreach($grilla as $fila):
					foreach($fila as $celda):
						if($celda['tipo'] == 'persona'):
							$print .= $this->get_grid_persona($celda);
						else:							
							$print .= $this->get_grid_imagen($celda);
						endif;
					endforeach;
				endforeach;
			endif;
		endif;
		return $print;
	}


	function get_grid_persona($persona){
		$out = $vertical_data = $horizon_data = '';

		$start = false; 
		$celda = $persona['cont'] + 1;
		$linea = $persona['linea'] + 1;
		$lineas = $persona['lineas'] + 1;
		$columna = $persona['columna'] + 1;
		$persona_num = $persona['persona'];


		($celda % 5) == 1 ? $start = true : $start = false;
		$start_linea = ceil($lineas / 2);

		if($lineas > 1 && $linea >= $start_linea && $linea > 1):
			$horizon_data = '-bottom';
		else:			
			$horizon_data = '-top';
		endif;

		if(!empty($columna) && $columna > 3):
			$vertical_data = 'right';
		else:
			$vertical_data = 'left';
		endif;

		if($linea > $start_linea && $celda != 1 && $start):
			$horizon_data = '-bottom';			
		endif;

		if($celda != 1 && $start):	
			$vertical_data = 'left';
		endif;

		$persona['expand'] = $vertical_data.$horizon_data;

		if(($persona_num % 2) == 0):
			$vertical_data_tablet = 'right';
		else:
			$vertical_data_tablet = 'left';
		endif;

		$middle_lineas_tablet = ceil($persona['persona_linea_tablet'] / 2);

		if($persona['persona_linea_tablet'] <= $middle_lineas_tablet){
			$horizon_data_tablet = '-top';
		}else{
			$horizon_data_tablet = '-bottom';
		}

		$persona['title']=get_the_title($persona['post_id']);
		$persona['background_image']=get_the_post_thumbnail_url($persona['post_id'], 'grid_300');
		$persona['permalink']=get_permalink($persona['post_id']);
		$persona['bajada']= has_excerpt($persona['post_id']) ? get_the_excerpt($persona['post_id']) : cut_string_to(get_the_content($persona['post_id']), 240);
		$persona['cargo'] = get_field('cargo', (int)$persona['post_id']);

		$persona['expand_tablet'] = $vertical_data_tablet . $horizon_data_tablet;


		$out .='<div data-grid-num="'.$celda.'" data-grid-linea="'.$linea.'" data-random="'.$columna.'" data-persona="'.$persona_num.'" class="grid-item">';
		$out .='  <article class="box box--grid box--expand" data-bg="background-image: url('.$persona['background_image'].')" data-expand="'.$persona['expand'].'" data-expand-tablet="'.$persona['expand_tablet'].'">';
		$out .='    <div class="box__body text-light">';
		$out .=			'<p class="box__meta text-upper">'.$persona['cargo'].'</p>';
		$out .='      <h3 class="box__title">'.$persona['title'].'</h3>';
		if(!empty($persona['bajada'])):
		$out .='      <div class="box__excerpt">';
		$out .=           $persona['bajada'];
		$out .='      </div>';
		endif;
		$out .='    </div>';
		$out .='  </article>';
		$out .='</div>';

		return $out;
	}

	function get_grid_imagen($celda){
		$out = "";
		$celda_num = $celda['cont'] + 1;
		$linea = $celda['linea'] + 1;
		$columna = $celda['columna'] + 1;

		$out .='<div data-grid-num="'.$celda_num.'" data-grid-linea="'.$linea.'" data-columna="'.$columna .'" class="grid-item hide@tablet">';
		if(!empty($celda['imagen'])):
		$out .='			<article class="box box--grid" style="background-image: url('.$celda['imagen']['url'].')"></article>';
		else:
		$out .='			<article class="box box--grid" style="background-image: url(https://unsplash.it/500/'.($celda_num + 500).')"></article>';
		endif;
		$out .='</div>';
		return $out;
	}
}
?>