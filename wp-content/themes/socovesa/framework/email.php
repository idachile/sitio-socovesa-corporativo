<?php
class mail_bullet{
  /**
  * Setea el contenido de un email a html
  * se usa en send_custom_email
  */
  function set_html_content_type(){
    return 'text/html';
  }
  function send($setting) {
    global $phpmailer;
    add_filter('wp_mail_content_type', array($this, 'set_html_content_type'));
    wp_mail($setting['target'], $setting['subject'], $setting['html'], $setting['headers'], $setting['attachment']);
    remove_filter('wp_mail_content_type', array($this, 'set_html_content_type'));
  }

  function setup_email_normal(){
    global  $data;
    $setting = get_field('respuestas_y_notificaciones', $data['pid']);
    $setting_object = get_field_object('datos_de_contacto', $data['pid']);
    foreach($setting_object['sub_fields'] as $fields){
      $replace[$fields['name']]= $data[$fields['key']];
    }
    $replace['logo'] = $setting['img_email'];
    $replace['border'] = $setting['color_mail'];
    $email_html = $setting['mensaje_administrador'];
    // email admin
    if(  $setting['email_administrador']==true):
      foreach ($replace as $dato_key => $dato_value) {
        $email_html = str_replace('%recipient.'.$dato_key.'%', $dato_value, $email_html);
      }
      foreach($setting['notificaciones'] as $notificacion):
        $send_admin_mail['subject']   =   $setting['asunto_administrador'];
        $send_admin_mail['html']      =   str_replace('%recipient.nombreadmin%',  $notificacion["nombre"], $email_html);
        $send_admin_mail['target']    =   $notificacion['nombre'] .' <'. $notificacion['email'] .'>';
        $send_admin_mail['headers']   =   array( 'From: '. $replace['nombre'] .' <'. $replace['email'] .'>',  'X-Sender: '. $replace['nombre']  .' <'. $replace['email'] .'>');
        $this->send($send_admin_mail);
      endforeach;
    endif;
    // email usuario
    $email_html = $setting['mensaje_usuario'];
    foreach ($replace as $dato_key => $dato_value) {
      $email_html = str_replace('%recipient.'.$dato_key.'%', $dato_value, $email_html);
    }

    $send_mail_usuario['subject']   =   $setting['asunto_usuario'];
    $send_mail_usuario['html']      =   $email_html;
    $send_mail_usuario['target']    =   $replace['nombre'] .' <'. $replace['email'] .'>';
    $send_mail_usuario['headers']   =   array('From: '. $setting['nombre_reply'] .' <'. $setting['email_reply'] .'>', 'X-Sender: '. $setting['nombre_reply'] .' <'. $setting['email_reply'] .'>');
    $this->send($send_mail_usuario);
  }

  function setup_email_contacto(){
    global  $data;
    $setting = get_field('respuestas_y_notificaciones', $data['pid']);
    $setting_object = get_field_object('datos_de_contacto', $data['pid']);
    $temas_consulta = get_field('tema_a_consultar', $data['pid']);

    foreach($setting_object['sub_fields'] as $fields){
      $replace[$fields['name']]= $data[$fields['key']];
    }

    $replace['logo'] = $setting['img_email'];
    $replace['border'] = $setting['color_mail'];
    $email_html = $setting['mensaje_administrador'];
    // email admin
    if(  $setting['email_administrador']==true):
      // $replace['archivo_descargable'] = $data['archivo_url'];
      foreach ($replace as $dato_key => $dato_value) {
        $email_html = str_replace('%recipient.'.$dato_key.'%', $dato_value, $email_html);
      }
      foreach($temas_consulta as $notificacion):
        if($notificacion['tema'] == $data['field_5d28895835328']):
          $send_admin_mail['subject']   =   $setting['asunto_administrador'];
          $send_admin_mail['html']      =   str_replace('%recipient.nombreadmin%',  $notificacion["nombre"], $email_html);
          $send_admin_mail['target']    =   $notificacion['nombre'] .' <'. $notificacion['email'] .'>';
          $send_admin_mail['headers']   =   array( 'From: '. $replace['nombre'] .' <'. $replace['email'] .'>',  'X-Sender: '. $replace['nombre']  .' <'. $replace['email'] .'>');
          $send_admin_mail['attachment'] = get_attached_file($replace['archivo']);
          $this->send($send_admin_mail);
        endif;
      endforeach;
    endif;
    // email usuario
    $email_html = $setting['mensaje_usuario'];
    foreach ($replace as $dato_key => $dato_value) {
      $email_html = str_replace('%recipient.'.$dato_key.'%', $dato_value, $email_html);
    }

    $send_mail_usuario['subject']   =   $setting['asunto_usuario'];
    $send_mail_usuario['html']      =   $email_html;
    $send_mail_usuario['target']    =   $replace['nombre'] .' <'. $replace['email'] .'>';
    $send_mail_usuario['headers']   =   array('From: '. $setting['nombre_reply'] .' <'. $setting['email_reply'] .'>', 'X-Sender: '. $setting['nombre_reply'] .' <'. $setting['email_reply'] .'>');
    $this->send($send_mail_usuario);
  }
}
?>
