<?php
////Menus Walker
class custom_sub_walker extends Walker_Nav_Menu{

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul class=\"submenu\">\n";
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID . ' no-touch';

        /*grab the default wp nav classes*/
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        /*if the current item has children, append the dropdown class*/
        if ( $args->has_children )
			$class_names .= ' menu-item--has-sub-menu';

        /*if there aren't any class names, don't show class attribute*/
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';



        $output .= $indent . '<li' . $id . $value . $class_names .' data-role="touch-submenu">';

        $atts = array();
        $atts['target'] = ! empty( $item->target )  ? $item->target : '';
        $atts['title']  = ! empty( $item->title )   ? $item->title  : '';
        $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';


        /*if the current menu item has children and it's the parent, set the dropdown attributes*/
        if ( $args->has_children && $depth === 0 ) {
            $atts['href']           = $item->url;
        } else {
            $atts['href'] = ! empty( $item->url ) ? $item->url : '';
        }

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;

        $item_output .= '<a'. $attributes .' data-touch="false"><span>';

        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

    /*  if the current menu item has children and it's the parent item, append the fa-angle-down icon*/
        $item_output .= ( $args->has_children && $depth === 0 ) ? '</span></a><span class="click-handler" data-role="touch-submenu-deployer"></span>' : '</span></a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    }


    public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;

        $id_field = $this->db_fields['id'];

        if ( is_object( $args[0] ) )
            $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

class MainWalker extends Walker_Nav_Menu{

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		 $indent = str_repeat( "\t", $depth );
		 $output .= "\n$indent<div class=\"submenu\"><div class=\"submenu__veil\"><div class=\"container no-gutter@medium\"><ul class=\"submenu__list\">\n";
	}
  	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul></div></div></div>\n";
  	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		 $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		 $class_names = $value = '';

		 $classes = empty( $item->classes ) ? array() : (array) $item->classes;
		 $classes[] = 'menu-item-' . $item->ID . ' no-touch';

		 /*grab the default wp nav classes*/
		 $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

		 /*if the current item has children, append the dropdown class*/
		 if ( $args->has_children )
		  $class_names .= ' menu-item--has-sub-menu';

		 /*if there aren't any class names, don't show class attribute*/
		 $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		 $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		 $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';



		 $output .= $indent . '<li' . $id . $value . $class_names .' data-role="touch-submenu">';

		 $atts = array();
		 $atts['target'] = ! empty( $item->target )  ? $item->target : '';
		 $atts['title']  = ! empty( $item->title )   ? $item->title  : '';
		 $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';


		 /*if the current menu item has children and it's the parent, set the dropdown attributes*/
		 if ( $args->has_children && $depth === 0 ) {
			  $atts['href']           = $item->url;
		 } else {
			  $atts['href'] = ! empty( $item->url ) ? $item->url : '';
		 }

		 $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		 $attributes = '';
		 foreach ( $atts as $attr => $value ) {
			  if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
			  }
		 }

		 $item_output = $args->before;

		 if($args->has_children && $depth == 0)
		 	$item_output .= '<div class="menu-item">';
		 $item_output .= '<a'. $attributes .' data-touch="false"><span>';

		 $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

	/*  if the current menu item has children and it's the parent item, append the fa-angle-down icon*/
		 $item_output .= ( $args->has_children && $depth === 0 ) ? '</span></a><span class="click-handler" data-role="touch-submenu-deployer"></span>' : '</span></a>';
		 if($args->has_children && $depth == 0)
		 	$item_output .= '</div>';
		 $item_output .= $args->after;

		 $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	}


	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
		 if ( ! $element )
			  return;

		 $id_field = $this->db_fields['id'];

		 if ( is_object( $args[0] ) )
			  $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

		 parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}

class custom_sub_walker_footer_col1 extends Walker_Nav_Menu {

    public function start_lvl( &$output, $depth = 1, $args = array() ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul class=\" sub-menu\">\n";
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';
        // $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes = array();
        $classes[] = 'footer-bar__link ';

        /*grab the default wp nav classes*/
        $class_names = join( ' ', apply_filters( '', array_filter( $classes ), $item, $args ) );

        /*if the current item has children, append the dropdown class*/
        // if ( $args->has_children )
		// 	$class_names .= ' menu-item--has-sub-menu';

        /*if there aren't any class names, don't show class attribute*/
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';



        $output .= $indent . '<li' . $id . $value . $class_names .' data-role="touch-sub-menu">';

        $atts = array();
        $atts['target'] = ! empty( $item->target )  ? $item->target : '';
        $atts['title']  = ! empty( $item->title )   ? $item->title  : '';
        $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';


        /*if the current menu item has children and it's the parent, set the dropdown attributes*/
        if ( $args->has_children && $depth === 0 ) {
            $atts['href']           = $item->url;
        } else {
            $atts['href'] = ! empty( $item->url ) ? $item->url : '';
        }

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;

        $item_output .= '<a'. $attributes .'>';

        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

    /*  if the current menu item has children and it's the parent item, append the fa-angle-down icon*/
        $item_output .= ( $args->has_children && $depth === 0 ) ? ' </a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    }


    public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;

        $id_field = $this->db_fields['id'];

        if ( is_object( $args[0] ) )
            $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}


/**
 * Devuelve el HTML de la paginacion de una $wp_query
 * @param  object $query $wp_query a la cual paginar
 * @param  string  $prev  Texto para el boton anterior
 * @param  string  $next  Texto para el boton siguiente
 * @return string HTML de la paginacion
 */
function get_pagination( $query = false, $prev = 'Anterior', $next = 'Siguiente' ) {
    global $wp_query, $wp_rewrite, $paged;
    if ( !$query ) { $query = $wp_query; }
    $paged = $query->query_vars['paged'] > 1 ? $current = $query->query_vars['paged'] : $current = 1;
    // opciones generales para los links de paginacion, la opcion "format" puede esar en español7
    // solo si es que esta activo el filtro para cambiar esto
    $pagination = array(
        'base' => @add_query_arg('paged', '%#%'),
        'format' => '/pagina/%#%',
        'total' => $query->max_num_pages,
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'mid_size' => 2,
        'type' => 'array',
    );
    $items = "";
    $pageLinksArray = paginate_links($pagination);

    if( !empty( $pageLinksArray ) ){
        // reviso si es que existe un link "anterior", lo saco del array y lo guardo en variable
        $prevLink = '';
        if( preg_match('/'. $prev .'/i', $pageLinksArray[0]) ){
            $prevLink = preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="no-touch" data-touch="false" title="Página anterior" rel="prev" ', array_shift($pageLinksArray));
            $prevLink = preg_replace('/(?<=\"\>)(.*?)(?=\<\/)/', '', $prevLink);
        }
        // lo mismo para el link "siguiente"
        $nextLink = '';
        if( preg_match('/'. $next .'/i', end($pageLinksArray)) ){
            $nextLink = preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="no-touch" data-touch="false" title="Página siguiente" rel="next" ', array_pop($pageLinksArray));
            $nextLink = preg_replace('/(?<=\"\>)(.*?)(?=\<\/)/', '', $nextLink);
        }
        // se ponen los links "anterior" y "siguiente" dentro del html deseado
        $items .= '<li class="paginator__item prev">' . $prevLink . '</li>';
        //se itera sobre los links de paginas
        foreach( (array)$pageLinksArray as $pageLink ){
            // se itera sobre el resto de los links con el fin de cambiar y/o agregar clases personalizadas
            // si estoy en la pagina
            if( preg_match('/current/i', $pageLink) ){
                $items .= '<li class="paginator__item current">';
                $items .=   preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' rel="nofollow" ', $pageLink);
                $items .= '</li>';
            }
            else {
                $items .= '<li class="paginator__item">';
                $items .=   preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="no-touch" data-touch="false" rel="nofollow" ', $pageLink);
                $items .= '</li>';
            }
        }
        $items .= '<li class="paginator__item next">' . $nextLink . '</li>';
    }

    $out  = '<div class="flex-center hat">';
    $out .=     '<div class="paginator">';
    $out .=         $items;
    $out .=     '</div>';
    $out .= '</div>';

    return $out;
}


function get_siblings_navigation($post_id = 0, $parent_id){
	$print = '';
	$new_query = new WP_Query(array(
		'post_type' => 'page',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'post_parent' => $parent_id,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	));
	
	if($new_query->have_posts()):
		$print  = 	'<nav class="nav-bar__internal hide@medium">';
		$print .= 		'<div class="container" data-area-name="desktop-aux">';
		$print .= 			'<ul id="menu-parent-'.$parent_id.'" class="internav" data-mutable="tablet-down" data-desktop-area="desktop-aux" data-mobile-area="mobile-aux">';
		while($new_query->have_posts()): $new_query->the_post();
			$new_query->post->ID == $post_id ? $current = 'current' : $current = '';
			if(!has_children($new_query->post->ID) || ($new_query->post->ID == 361 || $new_query->post->ID == 75)):
				$print .= 			'<li class="internav__item '.$current.'"><a href="'.get_permalink($new_query->post->ID).'" class="no-touch" data-touch="false">'.get_the_title($new_query->post->ID).'</a></li>';
			else:
				$print .= 			'<li class="internav__item has-children '.$current.'" data-role="touch-submenu"><a href="'.get_permalink($new_query->post->ID).'" class="no-touch" data-touch="false">'.get_the_title($new_query->post->ID).'</a><span class="click-handler" data-role="touch-submenu-toggler"></span>';
				$print .=				'<ul class="submenu">';
				$print .=					get_little_children($new_query->post->ID);
				$print .=				'</ul>';
				$print .=			'</li>';
			endif;
		endwhile;
		$print .= 			'</ul>';
		$print .= 		'</div>';	
		$print .= 	'</nav>';
	endif;

	wp_reset_query();

	return $print;
}

function get_little_children($parent_id){
	global $pid;
	$current_id = $pid;
	$print = '';
	$new_query = new WP_Query(array(
		'post_type' => 'page',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'post_parent' => $parent_id,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	));
	if($new_query->have_posts()):
        while($new_query->have_posts()): $new_query->the_post();
        $new_query->post->ID == $current_id ? $current = 'current' : $current = '';
		$print .= '<li class="submenu__item '.$current.'"><a href="'.get_permalink($new_query->post->ID).'">'.get_the_title($new_query->post->ID).'</a></li>';
		endwhile;
	endif;
	wp_reset_query();

	return $print;
}


function single_nav_classes( $classes, $item) {	
	$elementos_menu = array('Equipo', 'equipo');
	foreach($elementos_menu as $menu_item):
		if ( is_singular('persona') && $item->title == $menu_item) {
			$classes[] = 'current-menu-item';
		}
	endforeach;
	return $classes;
}
add_filter( 'nav_menu_css_class', 'single_nav_classes', 10, 2 );
?>