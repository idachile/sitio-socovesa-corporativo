<?php
$api_rest = new API_Rest();
class API_Rest {
  //constructor
	function __construct(){
		add_action('rest_api_init', array( $this, 'set_endpoints' ));
	}

	function set_endpoints(){
		///////////////
		//FORMULARIOS
		///////////////

		register_rest_route('check', 'rest', [
			'methods' => 'POST',
			'callback' => [$this, 'check_rest']
		]);

		register_rest_route('procesar', 'contacto', [
			'methods' => 'POST',
			'callback' => [$this, 'procesar_contacto']
		]);

		register_rest_route('procesar', 'contacto_inversion', [
			'methods' => 'POST',
			'callback' => [$this, 'procesar_contacto_inversion']
		]);

		register_rest_route('obtener', 'ipsa', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_ipsa']
		]);

		register_rest_route('agregar', 'ipsa', [
			'methods' => 'POST',
			'callback' => [$this, 'agregar_ipsa']
		]);

		register_rest_route('eliminar', '/ipsa/(?P<id>\d+)', [
			'methods' => 'GET',
			'callback' => [$this, 'eliminar_ipsa']
		]);

		register_rest_route('obtener', 'valores', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_ipsa']
		]);

		register_rest_route('obtener', 'accion', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_accion']
		]);

		register_rest_route('obtener', 'accion_lastyear', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_accion_lastyear']
		]);

		register_rest_route('obtener', 'accion_lastsemester', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_accion_lastsemester']
		]);

		register_rest_route('obtener', 'accion-bd', [
			'methods' => 'GET',
			'callback' => [$this, 'obtener_accion_bd']
		]);

		register_rest_route('obtener', 'dias-mes', [
			'methods' => 'GET',
			'callback' => [$this, 'get_dias_by_mes']
		]);

		register_rest_route('obtener', 'valores-bolsa', [
			'methods' => 'POST',
			'callback' => [$this, 'get_valores_bolsa']
		]);

		register_rest_route('procesar', 'suscripcion', [
			'methods' => 'POST',
			'callback' => [$this, 'procesar_suscripcion']
		]);

		register_rest_route('get', 'repositorio/split', [
			'methods' => 'GET',
			'callback' => [$this, 'get_repositorio_split']
		]);

		register_rest_route('get', 'repositorio/full', [
			'methods' => 'GET',
			'callback' => [$this, 'get_repositorio_full']
		]);
	}

	function obtener_ipsa(WP_REST_Request $request){
		global $wpdb;
		// $data = $request->get_params();
		$valores = $wpdb->get_results('SELECT * FROM wp_valor_bursatil ORDER by id ASC');
		// $valores = get_field('field_5c79471cdc1b0', 12);
		// $valores = $valores[$data['id']]["datos"];
		$i=0;
		foreach ($valores as $valor){
			$datos["IPSA"][$i]["label"]=$valor->fecha;
			$datos["IPSA"][$i]["y"]= (int) $valor->ipsa;
			$datos["Socovesa S.A"][$i]["label"]=$valor->fecha;
			$datos["Socovesa S.A"][$i]["y"]= (int) $valor->socovesa;
			$i++;
		}
		return $datos;
		exit;
	}

	function eliminar_ipsa(WP_REST_Request $request){
		global $wpdb;
		$data = $request->get_params();
		clean_data_form($data);
		$id = $data["id"];
		$wpdb->query('DELETE FROM wp_valor_bursatil WHERE id = '.$id);
		wp_redirect($_SERVER['HTTP_REFERER'],  302);
		exit;
	}

	function get_repositorio_full(WP_REST_Request $request){
		$data = $request->get_params();
		return get_documentos_completos_repositorio($data['pid']);

		exit;
	}

	function get_repositorio_split(WP_REST_Request $request){
		$id = 4283;
		return get_documentos_agrupados_repositorio($id);
		exit;
	}

	function agregar_ipsa(WP_REST_Request $request){
		global $wpdb;
		$data = $request->get_params();
		clean_data_form($data);
		$inserts = array('fecha' => $data['fecha'], 'ipsa' => $data['ipsa'], 'puntos' => $data['puntos'],  'socovesa' => $data['socovesa'],  'precio' => $data['precio']);
		$wpdb->insert('wp_valor_bursatil',$inserts);
		wp_redirect($_SERVER['HTTP_REFERER'],  302);
		exit;
	}

	function obtener_accion(WP_REST_Request $request){
		global $wpdb;
		// $data = $request->get_params();

		// $valores = $wpdb->get_results('SELECT * FROM wp_valor_bursatil ORDER by id ASC');

		$valores = get_valores_bolsa_santiago();

		// $valores = get_field('field_5c79471cdc1b0', 12);
		// $valores = $valores[$data['id']]["datos"];
		$i=0;
		foreach ($valores as $valor){
			$datos["precio"][$i]["label"]=$valor->Date;
			$datos["precio"][$i]["y"]= (int) $valor->Adj_Close;
			$i++;
		}
		return $datos;
		exit;
	}

	function obtener_accion_lastyear(WP_REST_Request $request){
		global $wpdb;
		$datos = array();
		$valores = get_valores_bolsa_santiago();

		$last_year = strtotime(date("d-m-Y") . '-1 year');

		$i = 0;
		if(!empty($valores)):
			foreach ($valores as $valor){
				$valor_time = strtotime( date('Y-m-d', strtotime($valor->Date)) );
				if($valor_time >= $last_year):
					$datos["precio"][$i]["label"]=$valor->Date;
					$datos["precio"][$i]["y"]= (int) $valor->Adj_Close;
					$i++;
				endif;
			}
		endif;
		return $datos;
		exit;
	}

	function obtener_accion_lastsemester(WP_REST_Request $request){
		global $wpdb;
		$valores = get_valores_bolsa_santiago();

		$last_year = strtotime(date("d-m-Y") . '-6 months');

		$i = 0;
		foreach ($valores as $valor){
			$valor_time = strtotime( date('Y-m-d', strtotime($valor->Date)) );
			if($valor_time >= $last_year):
				$datos["precio"][$i]["label"]=$valor->Date;
				$datos["precio"][$i]["y"]= (int) $valor->Adj_Close;
				$i++;
			endif;
		}
		return $datos;
		exit;
	}

	function obtener_accion_bd(WP_REST_Request $request){
		global $wpdb;
		// $data = $request->get_params();

		$valores = $wpdb->get_results('SELECT * FROM wp_valor_bursatil ORDER by id ASC');

		// $valores = get_field('field_5c79471cdc1b0', 12);
		// $valores = $valores[$data['id']]["datos"];
		$i=0;
		foreach ($valores as $valor){
			$datos["precio"][$i]["label"]=$valor->fecha;
			$datos["precio"][$i]["timestamp"]=strtotime($valor->fecha);
			$datos["precio"][$i]["y"]= (int) $valor->precio;
			$i++;
		}
		return $datos;
		exit;
	}

   function procesar_contacto(WP_REST_Request $request){
      date_default_timezone_set('America/Santiago');
      global  $wpdb,$data;
		$data_pre = $request->get_params();

		foreach($data_pre as $key => $value){
			$data[$key] = strip_tags($value);
		}

		clean_data_form($data);

		if($data['st_verify'] != ''):
			header('HTTP/1.0 403 Forbidden');
			exit;
		endif;

		if(!isset( $data['st_nonce'] ) && !wp_verify_nonce( $data['st_nonce'], 'enviar_contacto')):
			header('HTTP/1.0 403 Forbidden');
			exit;
		endif;


		if( !filter_var($data['field_5d287e9c2f804'], FILTER_VALIDATE_EMAIL)){
			header('HTTP/1.0 403 Forbidden');
			exit;
		}

		$data['archivo_url'] = 'No aplica';
		 $finfo = new finfo(FILEINFO_MIME_TYPE);
		if( $_FILES['field_5d28897235329']['size'] == 0 ):
			$data['field_5d28897235329'] = '';
		else:
			$file_id = upload_custom_file( $_FILES['field_5d28897235329'] );
			$data['field_5d28897235329'] = $file_id;
			$data['archivo_url'] = '<li><strong>Archivo:</strong> <a href="'.wp_get_attachment_url($file_id).'" download>Descargar</a></li>';
		endif;

		$pid =  $data['pid'];
		$contacto_id = wp_insert_post(array(
			'ID'=>0,
			'post_title' => "Contacto de ". $data['field_5d287e9c2f555'] .' '. $data['field_5d287e9c2f6ae'],  //nombre_proyecto = nombre + apellido
			'post_content' => serialize($data),
			'post_status' => 'draft',
			'post_type' => 'contacto',
			'post_author' => 0,
			'ping_status' => 'close',
			'comment_status' => 'close'
		));

		wp_set_post_terms($contacto_id, $data['tipo_contacto'], 'tipo_contacto');

      $allmeta_key = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = $pid");
      foreach ($allmeta_key as $meta) {
			update_post_meta($contacto_id, $meta->meta_key, $meta->meta_value);
      }

      $fields = get_field_object($data["grupo"],$data['pid']);
      foreach ($fields['sub_fields'] as $input){
			$update[$input["key"]]=$data[$input["key"]];
		}

		update_field($data["grupo"], $update, $contacto_id);

		if($contacto_id){
			$bullet = new mail_bullet();
			$bullet->setup_email_contacto();
			wp_redirect( get_permalink($pid) . "exito/",  302);
		}else{
			wp_redirect( get_permalink($pid) . "error/",  302);
		}
      exit;
	}

	function procesar_contacto_inversion(WP_REST_Request $request){
      date_default_timezone_set('America/Santiago');
      global  $wpdb,$data;
		$data_pre = $request->get_params();

		foreach($data_pre as $key => $value){
			$data[$key] = strip_tags($value);
		}

		clean_data_form($data);

		if($data['st_verify'] != ''):
			header('HTTP/1.0 403 Forbidden');
			exit;
		endif;

		if(!isset( $data['st_nonce'] ) && !wp_verify_nonce( $data['st_nonce'], 'enviar_contacto')):
			header('HTTP/1.0 403 Forbidden');
			exit;
		endif;


		if( !filter_var($data['field_5c6333915fc81'], FILTER_VALIDATE_EMAIL)){
			header('HTTP/1.0 403 Forbidden');
			exit;
		}

      $pid =  $data['pid'];
		$contacto_id = wp_insert_post(array(
			'ID'=>0,
			'post_title' => "Contacto de ". $data['field_5c63337b5fc7f'] .' '. $data['field_5c6333855fc80'],  //nombre_proyecto = nombre + apellido
			'post_content' => serialize($data),
			'post_status' => 'draft',
			'post_type' => 'contacto',
			'post_author' => 0,
			'ping_status' => 'close',
			'comment_status' => 'close'
		));

		wp_set_post_terms($contacto_id, $data['tipo_contacto'], 'tipo_contacto');

      $allmeta_key = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = $pid");
      foreach ($allmeta_key as $meta) {
			update_post_meta($contacto_id, $meta->meta_key, $meta->meta_value);
      }

      $fields = get_field_object($data["grupo"],$data['pid']);
      foreach ($fields['sub_fields'] as $input){
			$update[$input["key"]]=$data[$input["key"]];
      }
      update_field($data["grupo"], $update, $contacto_id);

		if($contacto_id){
			$bullet = new mail_bullet();
			$bullet->setup_email_normal();
			wp_redirect( get_permalink($pid) . "exito/",  302);
		}else{
			wp_redirect( get_permalink($pid) . "error/",  302);
		}
      exit;
   }

	function upload_user_file( $file = array() ) {
		require_once( ABSPATH . 'wp-admin/includes/admin.php' );

		$file_return = wp_handle_upload( $file, array('test_form' => false ) );

		if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
			return false;
		} else {

			$filename = $file_return['file'];

			$attachment = array(
				'post_mime_type' => $file_return['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content' => '',
				'post_status' => 'inherit',
				'guid' => $file_return['url']
			);

			$attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );

			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
			wp_update_attachment_metadata( $attachment_id, $attachment_data );

			if( 0 < intval( $attachment_id ) ) {
				return $attachment_id;
			}
		}

		return false;
	}

	function get_dias_by_mes(WP_REST_Request $request){
		$data = $request->get_params();
		clean_data_form($data);
		$dias = array();
		$data_html = '';

		if(!empty($data['year']) && !empty($data['month'])):
			$month = $data['month'];
			$year = $data['year'];

			for($d=1; $d<=31; $d++):
				$time=mktime(12, 0, 0, $month, $d, $year);
				if (date('m', $time)==$month)
						$dias[]=date('d', $time);
			endfor;

			if(!empty($dias)):
				foreach($dias as $day):
					if($data['year'] == $data['cyear'] && $data['month'] == $data['cmonth']):
						if($day <= date('d')):
							$data_html .= '<option value="'.$day.'">'.$day.'</option>';
						endif;
					else:
						$data_html .= '<option value="'.$day.'">'.$day.'</option>';
					endif;
				endforeach;
			endif;
		endif;

		return array('data' => $data, 'status' => 200, 'data_html' => $data_html);
	}

	function get_valores_bolsa(WP_REST_Request $request){
		$data = $request->get_params();

		clean_data_form($data);

		$response = array('status' => 404);

		if(!empty($data)):
			$valores_bolsa = get_valores_bolsa_santiago();

			foreach($valores_bolsa as $valor):
				if($valor->Date == $data['year'].'-'.$data['month'].'-'.$data['day']):
					$found = $valor;
				endif;
			endforeach;

			if(!empty($found)):
				$response = array('opening_price' => print_numerical($found->Open), 'closing_price' => print_numerical($found->High), 'total' => print_numerical($found->Adj_Close), 'status' => 200);
			endif;

		endif;

		return $response;
	}


	function procesar_suscripcion(WP_REST_Request $request){
		$data_pre = $request->get_params();
		foreach($data_pre as $key => $value){
			$data[$key] = strip_tags($value);
		}
		clean_data_form($data);

		if($data['st_verify'] != ''):
				header('HTTP/1.0 403 Forbidden');
				exit;
			endif;

			if(!isset( $data['st_nonce'] ) && !wp_verify_nonce( $data['st_nonce'], 'enviar_newsletter')):
				header('HTTP/1.0 403 Forbidden');
				exit;
		endif;

		if( !filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
			header('HTTP/1.0 403 Forbidden');
			exit;
		}

		$list_id = get_field("id_de_lista", "options");

      if (!$list_id) {
			wp_die('Ingrese el identificador de la lista');
      }

      $dato_suscriptor = array('id' => $list_id, 'merge_vars' => array('FNAME' => $data['nombre']), 'email' => array('email' => $data['email']), 'double_optin' => false, 'send_welcome' => true);

      $mailchimp = new Ninja_mailchimp();

      if (!$mailchimp->api_key) {
			wp_die('Ingrese el el API key');
      }

		try{
			$mailchimp->api->call('lists/subscribe', $dato_suscriptor);
			$mensaje_html = get_ajax_response_success($data['pid'], 'exito');
		}catch (Exception $e){
			$mensaje_html = get_ajax_response_success($data['pid'], 'error');
		}

		return array('status' => 'success', 'data' => $data, 'response_html' => $mensaje_html);
	}
}
?>
