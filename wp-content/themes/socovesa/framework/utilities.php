<?php
/////////////////////
////////////utilities
////////////////////

/**
* Anade o mezcla un array de tax_query existente con otro devolviendo el resultado
* @param  mixed - $old_tax_query - tax_query actual, puede estar vacio
* @param  array - $aditional_tax_query - tax_query que agregar
* @param  string $relation = relation del tax_query, default: "AND"
* @return array - tax_query formateado y mesclado
*/
function merge_tax_query( $old_tax_query, $aditional_tax_query, $relation = 'AND' ){
  if( !$old_tax_query || !is_array( $old_tax_query ) ){
    return array( $aditional_tax_query );
  }
  if( !isset($old_tax_query['relation']) || !$old_tax_query['relation'] ){
    $old_tax_query['relation'] = $relation;
  }
  $old_tax_query[] = $aditional_tax_query;
  return $old_tax_query;
}


/**DISABLED
* Agrega clase font-size-medium al 1º <p> en el the_content para las páginas :
*/
function first_paragraph($content){
  if(is_page()) {
    return preg_replace('/<p([^>]+)?>/', '<p$1 class="first-paragraph">', $content, 1);
  }else{
    return $content;
  }
}
// add_filter('the_content', 'first_paragraph');


/**
* cut_string_to
* @param  [string] $string     [Texto a cortar]
* @param  [int] $charnum       [Numero de caracteres máximo para el texto]
* @param  string $sufix        [Sufijo para el texto cortado]
* @return [string]             [Texto cortado]
*/
function cut_string_to( $string, $charnum, $sufix = ' ...' ){
  $string = strip_tags( $string );
  if( strlen($string) > $charnum ){
    $string = substr($string, 0, ($charnum - strlen( $sufix )) ) . $sufix;
  }
  return mb_convert_encoding($string, "UTF-8");
}

/**
* [printme]
* Imprime en pantalla cualquier cosa entre <pre>
* @param  [mixed] $thing [description]
* @return void
*/
function printme( $thing ){
  echo '<pre>';
  print_r( $thing );
  echo '</pre>';
}

/**
* [ensure_url]
* Convierte un string con forma de url en una URL valida, si ya es una URL valida entonces se devuelve tal cual
* @param  [type] $proto_url [description]
* @return [type]            [description]
*/
function ensure_url( $proto_url, $protocol = 'http' ){
  // se revisa si es un link interno primero
  if( substr($proto_url, 0, 1) === '/' ){
    return $proto_url;
  }
  if (filter_var($proto_url, FILTER_VALIDATE_URL)) {
    return $proto_url;
  }
  elseif( substr($proto_url, 0, 7) !== 'http://' || substr($proto_url, 0, 7) !== 'https:/' ){
    $url = $protocol . '://' . $proto_url;
  }
  // doble chequeo de validacion de URL
  if ( ! filter_var($url, FILTER_VALIDATE_URL) ) {
    return '';
  }
  return $url;
}


function upload_custom_file( $file_data, $mimes = null ){
  if ( ! function_exists( 'wp_handle_upload' ) ) { require_once( ABSPATH . 'wp-admin/includes/file.php' ); }

  $fotoUpload = wp_handle_upload( $file_data, array( 'mimes' => $mimes, 'test_form' => false ) );
  $filename = $fotoUpload['file'];
  $wp_filetype = wp_check_filetype(basename($filename), null );
  $wp_upload_dir = wp_upload_dir();
  $attachment = array(
    'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $filename ),
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
    'post_content' => '',
    'post_status' => 'inherit'
  );
  $attach_id = wp_insert_attachment( $attachment, $filename );
  require_once(ABSPATH . 'wp-admin/includes/image.php');
  $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
  wp_update_attachment_metadata( $attach_id, $attach_data );

  return $attach_id;
}

/**
* Devuelve la URL de un attachment o false si no se encuentra el attachment
* @param  [int] $id   ID del attachment
* @param  [string] $size Nombre del tamano a sacar
* @return [string] URL de la imagen en el tamano solicitado (false si es que falla)
*/
function get_image_src( $id, $size ){
  $img_data = wp_get_attachment_image_src( $id, $size );
  if( empty($img_data) ){ return false; }
  return $img_data[0];
}

/**
* Revisa si el script dado ya se encuentra en la cola de impresion
* si no es asi lo mete en la cola
* @param  [string] $script_name [nombre del script a incluir]
* @return void
*/
function needs_script( $script_name ){
  if( !wp_script_is( $script_name ) ){
    wp_enqueue_script( $script_name );
  }
}

/**
* Devuelve el nombre del rol de un usuario
* @param  [object] $user Objeto de usuario de wordpress
* @return [string]
*/
function get_user_role( $user ) {
  $user_roles = $user->roles;
  $user_role = array_shift($user_roles);
  return $user_role;
}


/**
* [object_to_array]
* Devuelve el nombre del rol de un usuario
* @param  [object] recibe un objeto o un array con obejtos y lo transforma recursivamente en solo array
* @return [array]
*/
function object_to_array($obj) {
  if(is_object($obj)) $obj = (array) $obj;
  if(is_array($obj)) {
    $new = array();
    foreach($obj as $key => $val) {
      $new[$key] = object_to_array($val);
    }
  }
  else $new = $obj;
  return $new;
}


/**
* Devuelve la extension del archivo
* @param  string $file_path - PATH al archivo
* @return string - Extension del archivo
*/
function parse_mime_type( $file_path ) {
  $chunks = explode('/', $file_path);
  return substr(strrchr( array_pop($chunks) ,'.'),1);
}

/**
* Devuelve el peso del archivo formateado
* @param  string $attachment_file_path - PATH al archivo
* @return string - Tamano formateado en kb
*/
function get_attachment_size( $attachment_file_path ) {
  return size_format( filesize( $attachment_file_path ) );
}

function get_file_size($fileid, $decimal = 2){
  $filepath = get_attached_file($fileid);
  $bytes = filesize($filepath);
  $s = array('b', 'Kb', 'Mb', 'Gb');
  $e = floor(log($bytes)/log(1024));
  return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
}
function convert_size($bytes, $decimal = 2){
  $s = array('b', 'Kb', 'Mb', 'Gb');
  $e = floor(log($bytes)/log(1024));
  return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
}


function get_file_id_by_url($image_url) {
  global $wpdb;
  $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
  return $attachment[0];
}
/**
* Devuelve el un post_meta solicitado de todos los post
* @param  string $key meta_key
* @param  string  $type  post type
* @param  string  $status  status
* @return string HTML de la paginacion
*/
function get_meta_values( $key = '', $type = 'post', $status = 'publish' ) {
  global $wpdb;
  if( empty( $key ) )
  return;
  $r = $wpdb->get_results( $wpdb->prepare( "
  SELECT p.ID, pm.meta_value FROM {$wpdb->postmeta} pm
  LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
  WHERE pm.meta_key = '%s'
  AND p.post_status = '%s'
  AND p.post_type = '%s'
  ", $key, $status, $type ));

  foreach ( $r as $my_r )
  $metas[$my_r->ID] = $my_r->meta_value;

  return $metas;
}

add_shortcode('random_text', 'random_text_shortcode');
function random_text_shortcode( $atts, $content = ''){
  $long = $atts["long"];
  $string = 'Originalmente, fue la denominación de una institución educativa de la Atenas clásica: la Academia fundada por Platón, que debía su nombre a un héroe legendario de la mitología griega, Academo. Estaba ubicada en el demo de Kolonos,4 a un kilómetro al noroeste de la ciudad, en unos terrenos adquiridos por Platón alrededor del 384 a. C., donde existía un olivar, un parque y un gimnasio. La instrucción allí impartida incluía el estudio de las matemáticas, la dialéctica y las ciencias naturales. Hubo otras instituciones similares en la antigüedad, como el Liceo aristotélico; aunque ninguna se denominó La escuela de Atenas, título de uno de los frescos de Rafael en las estancias vaticanas donde se representa ucrónicamente a los sabios de la Antigüedad con los rostros de sus contemporáneos (1510-1512). Las instituciones científicas5 de la antigua Alejandría (Museo, Biblioteca) o las reuniones de intelectuales de la antigua Roma (como los del círculo de Mecenas o los de la corte de Augusto, o el Ateneo o "escuela romana" de Adriano), que hasta cierto punto compartían funciones con aquéllas, a veces son denominadas "academias", aunque no es habitual.6 La Academia platónica y las demás instituciones culturales consideradas "paganas" por los cristianos subsistieron hasta el año 529 cuando el emperador bizantino Justiniano I ordenó su clausura.';
  return cut_string_to($string,$long);
}

function random_text($long){
  $string = 'Originalmente, fue la denominación de una institución educativa de la Atenas clásica: la Academia fundada por Platón, que debía su nombre a un héroe legendario de la mitología griega, Academo. Estaba ubicada en el demo de Kolonos,4 a un kilómetro al noroeste de la ciudad, en unos terrenos adquiridos por Platón alrededor del 384 a. C., donde existía un olivar, un parque y un gimnasio. La instrucción allí impartida incluía el estudio de las matemáticas, la dialéctica y las ciencias naturales. Hubo otras instituciones similares en la antigüedad, como el Liceo aristotélico; aunque ninguna se denominó La escuela de Atenas, título de uno de los frescos de Rafael en las estancias vaticanas donde se representa ucrónicamente a los sabios de la Antigüedad con los rostros de sus contemporáneos (1510-1512). Las instituciones científicas5 de la antigua Alejandría (Museo, Biblioteca) o las reuniones de intelectuales de la antigua Roma (como los del círculo de Mecenas o los de la corte de Augusto, o el Ateneo o "escuela romana" de Adriano), que hasta cierto punto compartían funciones con aquéllas, a veces son denominadas "academias", aunque no es habitual.6 La Academia platónica y las demás instituciones culturales consideradas "paganas" por los cristianos subsistieron hasta el año 529 cuando el emperador bizantino Justiniano I ordenó su clausura.';
  return cut_string_to($string,$long);
}

function lorem_ipsum($long = 240){
  $string = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit nam sequi inventore obcaecati excepturi pariatur quibusdam veritatis, nisi facere ipsam officiis molestiae nobis ab ullam repellendus fugiat tempora architecto magni. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque maxime tempore, aperiam consequuntur incidunt ullam laudantium beatae corrupti voluptatibus labore, dolorum vitae non. Fugit commodi consequuntur, eligendi unde a modi? Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates fuga exercitationem quisquam expedita sit earum itaque ducimus doloremque quas aut! Molestiae ipsa necessitatibus deserunt ducimus perferendis beatae provident quaerat sunt?';
  return cut_string_to($string,$long);
}

//live preview
// function ci_customizer_live_preview(){
//     wp_enqueue_script( 'ci_themecustomizer', get_template_directory_uri().'/scripts/theme-customizer.js', array( 'jquery','customize-preview' ), '', true );
// }
// add_action( 'customize_preview_init', 'ci_customizer_live_preview' );


//FUNC:
//Load template part, use inside echo
function load_template_part($template_name, $part_name=null) {
  ob_start();
  get_template_part($template_name, $part_name);
  $var = ob_get_contents();
  ob_end_clean();
  return $var;
}

/**
* [generate_hs_urls]
* @param  [object]     $post_object
* @return [array]      URLs para compartir el post
*/
function generate_share_urls( $post_object, $custom_message = false, $target = false, $content_field = false ){
  $shortlink = wp_get_shortlink( $post_object->ID );
  if( $target ){
    $shortlink = $shortlink . '#' . $target;
  }

  $title = urlencode( get_the_title( $post_object->ID ) );

  $image_url = '';
  if( has_post_thumbnail( $post_object->ID ) ){
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post_object->ID ), 'full' );
    $image_url = $image_url[0];
  }
  $fb_link = 'http://www.facebook.com/sharer.php?u=' . get_permalink( $post_object->ID );
  $twt_link = 'https://twitter.com/intent/tweet?text=' . $title . '+' . get_permalink( $post_object->ID );
  $google_link = 'https://plus.google.com/share?url=' . get_permalink( $post_object->ID );
  $linkedin_link = 'https://www.linkedin.com/cws/share?url=' . get_permalink( $post_object->ID );
  $whatsapp_link = 'whatsapp://send?text==' . get_permalink( $post_object->ID );

  return array(
    'facebook' => $fb_link,
    'twitter' => $twt_link,
    'google' => $google_link,
    'linkedin' => $linkedin_link,
    'shortlink' => $shortlink,
    'whatsapp' => $whatsapp_link,
    'permalink' => get_permalink( $post_object->ID )
  );
}

function gmap_url($address){
  $address = urlencode($address);
  $address = ensure_url('www.google.com/maps/place/' . $address);
  return $address;
}

function print_floatval($number){
  return number_format(floatval($number), 5, '.', '');
}

function print_numerical($number){
  return number_format(floatval($number), 2, ',', '.');
}

function print_decimal($number, $decimal = 0){
  return number_format(floatval($number), floatval($decimal), ',', '.');
}

function precio($precio, $decimal = 0){
  return '$' . print_decimal($precio, $decimal);
}

function clean_string($string) {
  $string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.
  return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function clean_telefono($string) {
	$string = str_replace(' ', '', strtolower($string)); // Replaces all spaces with hyphens.
	$string = str_replace('-', '', strtolower($string)); // Replaces all spaces with hyphens.
	$string = str_replace('_', '', strtolower($string)); // Replaces all spaces with hyphens.
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 }

function clean_slug($string) {
  $string = str_replace('-', ' ', $string); // Replaces all spaces with hyphens.
  return str_replace('_', ' ', $string); // Replaces all spaces with hyphens.
}

function ida_array_alphabet(){
  return array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
}

function ida_replace_accented($string){
  $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

  return strtr( $string, $unwanted_array );
}

function sanitize_string($string){
  $toclean = ida_replace_accented($string);
  $freshandclean = clean_string(strtolower($toclean));

  return $freshandclean;
}

function sort_eventos($a, $b) {
  return $a['fecha_inicio'] - $b['fecha_inicio'];
}

function get_embed_video($videourl) {
  $url = wp_oembed_get(ensure_url($videourl));
  return $url;
}

function get_json_content( $content ){
  return json_encode($content);
}

function get_placeholder_image($size = 'full', $class = 'elastic-img'){
  $image = get_field('img_placeholder', 'options');
  return wp_get_attachment_image($image, $size, '', array('class'=>$class));
}
function get_placeholder_image_url($size = 'full'){
  $image = get_field('img_placeholder', 'options');
  return wp_get_attachment_image_url($image, $size, '');
}

function get_current_date(){
  $dia = date_i18n('j', strtotime(current_time('d/m/Y')));
  $mes = date_i18n('F', strtotime(current_time('d/m/Y')));
  $ano = date_i18n('Y', strtotime(current_time('d/m/Y')));
  $fecha = $dia . ' de ' . ucfirst($mes) . ' de ' . $ano;
  return $fecha;
}

function get_celular_codigos(){
  return '<option value="9">9</option><option value="8">8</option><option value="7">7</option><option value="6">6</option><option value="5">5</option>';
}

function get_telefono_codigos(){
  global $current_lang;
  if($current_lang == 'es'){
    $n = 'Norte';
    $c = 'Central';
    $s = 'Sur';
  }else{
    $n = 'North';
    $c = 'Center';
    $s = 'South';
  }
  return '<optgroup label="'.$n.'">
  <option value="58">58</option><option value="57">57</option><option value="55">55</option><option value="51">51</option><option value="52">52</option><option value="51">51</option><option value="53">53</option></optgroup><optgroup label="'.$c.'"><option value="2">2</option><option value="33">32</option><option value="34">34</option><option value="35">35</option><option value="39">39</option><option value="72">72</option><option value="75">75</option><option value="71">71</option><option value="73">73</option></optgroup><optgroup label="'.$s.'"><option value="42">42</option><option value="43">43</option><option value="41">41</option><option value="45">45</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="67">67</option><option value="61">61</option></optgroup>';
}

add_shortcode('boton', 'generate_button');
function generate_button( $atts, $content ){
  $settings = shortcode_atts(array(
    'align' => 'left'
  ), $atts);

  if( !$content ){ return false; }

  $align = '';

  // primero sacamos la alineacion real
  switch ($settings['align']) {
    case 'left':
    $align = 'flex-left';
    break;
    case 'center':
    $align = 'flex-center';
    break;
    case 'right':
    $align = 'flex-right';
    break;

    default:
    $align = 'flex-left';
    break;
  }


  $html = '<div class="'. $align .'">';
  $html .= preg_replace('/\s(href=[\'|"][^\'"]+[\'|"])/', ' $1 class="button main full"', $content);
  $html .= '</div>';

  return $html;
}

add_shortcode('txt', 'generate_short_text');
function generate_short_text($atts, $content){
  $printer = '';
  $settings = shortcode_atts(array(
    'abv' => '',
  ), $atts);

  if($settings['abv'] != ''){
    $short_text = '<span class="show@small">'.$settings['abv'].'</span>';
  }else{
    $short_text = '<span class="show@small">'.cut_string_to($content, 8).'</span>';
  }


  if( !$content ){ return false; }

  $printer .=			$short_text;
  $printer .=			'<span class="hide@small">'.$content.'</span>';

  return $printer;
}

function print_multimg($args){
  $at_id = $args['id'];
  $pid = $args['pid'];
  $images = '';
  $counting = 0;

  if( !empty($at_id) && !empty($args['sizes']) ){
    foreach($args['sizes'] as $size){
      $images .= wp_get_attachment_image($at_id, $size, '', array('class' => $args['classes'][$counting]));
      $counting++;
    }
  }elseif( !empty($pid) && !empty($args['sizes']) ){
    foreach($args['sizes'] as $size){
      $images .= get_the_post_thumbnail($at_id, $size, array('class' => $args['classes'][$counting]));
      $counting++;
    }
  }
  return $images;
}

function dist_img($string){
  return get_template_directory_uri() . '/dist/img/' . $string;
}

function get_data_monthnum($month_name){
	$months = array('enero' => 1, 'febrero' => 2, 'marzo' => 3, 'abril' => 4, 'mayo' => 5, 'junio' => 6, 'julio' => 7, 'agosto' => 8, 'septiembre' => 9, 'octubre' => 10, 'noviembre' => 11, 'diciembre' => 12);
	// printme($months);
	if(array_key_exists($month_name, $months)){
		return strtr( $month_name, $months );
	}
}

function get_months(){
	$months = array (
		1 	=> "Enero",
		2 	=> "Febrero",
		3 	=> "Marzo",
		4 	=> "Abril",
		5 	=> "Mayo",
		6 	=> "Junio",
		7 	=> "Julio",
		8 	=> "Agosto",
		9 	=> "Septiembre",
		10 	=> "Octubre",
		11 	=> "Noviembre",
		12 	=> "Diciembre",
	);

	return $months;
}

function get_months_alt(){
	$months = array (
		1 	=> array(
			'name' => 'Enero',
			'num' => '01'
		),
		2 	=> array(
			'name' => 'Febrero',
			'num' => '02'
		),
		3 	=> array(
			'name' => 'Marzo',
			'num' => '03'
		),
		4 	=> array(
			'name' => 'Abril',
			'num' => '04'
		),
		5 	=> array(
			'name' => 'Mayo',
			'num' => '05'
		),
		6 	=> array(
			'name' => 'Junio',
			'num' => '06'
		),
		7 	=> array(
			'name' => 'Julio',
			'num' => '07'
		),
		8 	=> array(
			'name' => 'Agosto',
			'num' => '08'
		),
		9 	=> array(
			'name' => 'Septiembre',
			'num' => '09'
		),
		10 	=> array(
			'name' => 'Octubre',
			'num' => '10'
		),
		11 	=> array(
			'name' => 'Noviembre',
			'num' => '11'
		),
		12 	=> array(
			'name' => 'Diciembre',
			'num' => '12'
		)
	);

	return $months;
}

function sanitize_slug($string){
  $toclean = sanitize_string($string);
  $freshandclean = str_replace(' ','-',strtolower($toclean));

  return $freshandclean;
}

function file_size_format($size){
  $units = array('Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB');
  return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2).' '.$units[$i];
}
function get_filesize($file){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $file);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // $data = curl_exec($ch);
  $filesize = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
  curl_close($ch);
  return file_size_format($filesize);
}


/**
 * Va a buscar un $post en base al slug (post_name) pasado
 * @param  string $slug - Slug o post_name del post que se quiere rescatar
 * @param  string $field - (opcional) Campo especifico que se desea del post.
 *                         Puede ser cualquiera del post object
 * @return mixed - false si no encuentra el post, $post_object (object) si es que lo encuentra
 */
function get_post_by_slug( $slug, $field = false ){
	global $wpdb;
	$pid = $wpdb->get_var($wpdb->prepare("
		 SELECT ID
		 FROM $wpdb->posts
		 WHERE post_name = %s
	", $slug));

	if( !$pid ){ return false; }

	$post_obj = get_post( $pid );

	if( $field && $post_obj ) {
		 return $post_obj->{$field};
	}

	return get_post( $pid );
}

/**
 * Devuelve el permalink de un post o pagina desde el slug
 * @param  [string] $slug  - Slug de la pagina o post
 * @return string
 */
function get_link_by_slug( $slug ){
	return get_permalink( get_post_by_slug( $slug, 'ID' ) );
}

function get_valores_bolsa_santiago(){
  $arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
); 
	$valores = get_transient('valores_bolsa_santiago_2019_2');
  //@todo if($array_uf==false) ¿de donde viene $array_uf?
	if($valores === false):
    $valores = file_get_contents('https://servicioscms.bolsadesantiago.com/DatosGraficos/DatosGraficos-ChartIQ/SOCOVESA-DAYS.js', false, stream_context_create($arrContextOptions));
		set_transient('valores_bolsa_santiago_2019_2', $valores,  86400); //24 hrs.
	endif;

	return str_ireplace("'", "", json_decode($valores));
}


/**
 * Genera el calendario de eventos
 * debe mostrar los dias en donde hay algo pasando
 * @return string
 */
function generate_events_calendar($data){
	global $wpdb;

	$available_dates = array();

	// consulta directa es menos cara
	$eventos = $wpdb->get_results("
		SELECT ID
		FROM $wpdb->posts
		WHERE post_status = 'publish'
		AND post_type = 'evento'
	");

	if( !empty($eventos) ){
		foreach( $eventos as $evento ){

			$available_dates[] = get_field('fecha', $evento->ID);
		}
	}

	needs_script('jquery_ui_datepicker');
	wp_localize_script( 'jquery_ui_datepicker', 'calendar_dates', $available_dates );
	wp_enqueue_style('jquery_ui_css', get_bloginfo('template_directory') . '/dist/css/jquery-ui.css', [], '1', 'all');

	$cal = '<div class="calendar-box" '.$data.'></div>';

	return $cal;
}

function get_ajax_response_success($pid, $status = 'error'){
	$response = '';
	$status == 'error' ? $icon = 'close' : $icon = 'check';
	$secciones = get_field('secciones', $pid);
	if(!empty($secciones)):
		foreach($secciones as $modulo):
			if($modulo['acf_fc_layout'] == 'modulo_sidebar'):
				$subsecciones = $modulo['secciones_sidebar'];
				if(!empty($subsecciones)):
					foreach($subsecciones as $sub):
						if($sub['acf_fc_layout'] == 'newsletter'):
							$response  = '<div class="response response--mini">';
							$response .= 	'<figure class="response__icon"><span class="icon icon-'.$icon.'"></span></figure>';
							$response .= 	'<div class="response__body">';
							$response .= 		'<h3 class="response__title">'.$sub['mensaje_'.$status.'_titulo'].'</h3>';
							if(!empty($sub['mensaje_'.$status.'_texto'])):
								$response .= 	'<div class="response__excerpt">'.apply_filters('the_content', $sub['mensaje_'.$status.'_texto']).'</div>';
							endif;
							$response .= 	'</div>';
							$response .= '</div>';
						endif;
					endforeach;
				endif;
			endif;
		endforeach;
	endif;

	return $response;
}

function get_calendar_box($post, $highlight = false){
	$box = '';
	if(!empty($post)):
		$pid = $post->ID;
		$archivos = get_field('archivos', $pid);
		$highlight == true ? $box_class = 'box--calendar--highlight' : $box_class = '';
		$highlight == true ? $list_class = 'light' : $list_class = '';

		$box  =	'<article class="box box--calendar '.$box_class.'">';

		if($highlight == true):
			$box .=	'<div class="box__head"><div class="box__date">';
			$box .=		'<span class="box__date__day">'.date_i18n('d',strtotime(get_field('fecha', $pid))).'</span>';
			$box .=		'<span class="box__date__month">'.date_i18n('M',strtotime(get_field('fecha', $pid))).'</span>';
			$box .=	'</div></div>';
			$box .=	'<div class="box__body">';
		else:
			$box .=	'<p class="box__meta">'.date_i18n('d/m/Y', strtotime(get_field('fecha', $pid))).'</p>';
		endif;

		$box .=			'<h3 class="box__title no-margin-top">'.get_the_title($post).'</h3>';

		if(has_excerpt($post)):
			$box .=		'<div class="box__excerpt">'.apply_filters('the_content', get_the_excerpt($post)).'</div>';
		endif;

		if(!empty($archivos)):
			$box .=			'<ul class="list__files '.$list_class.'">';

			foreach(get_field('archivos', $pid) as $file):
				if(!empty($file['archivo'])): $archivo = $file['archivo'];
					$box .=			'<li class="list__file__item">';
					$box .=				'<a href="'.$archivo['url'].'" download title="Descargar '.$archivo['title'].'">';
					$box .=					'<h4 class="list__file__title">'.$archivo['title'].'</h4>';
					$box .=					'<span class="list__file__details">'.convert_size($archivo['filesize']).'</span>';
					$box .=				'</a>';
					$box .=			'</li>';
				endif;
			endforeach;

			$box .=			'</ul>';
		endif;

		if($highlight == true):
			$box .=		'</div>';
		endif;
		$box .=	'</article>';
	endif;

	return $box;
}

add_action('acf/render_field_settings', 'catsup_render_field_settings_required');
function catsup_render_field_settings_required( $field ) {
	acf_render_field_setting( $field, array(
		'label'			=> __('Requerido IDA'),
		'instructions'	=> 'Activar solo si el campo necesita funciones de validación',
		'name'			=> 'required_ida',
		'type'			=> 'true_false',
		'ui'			  => 1,
    'default'   => false
	), true);

}

add_filter('acf/prepare_field', 'catsup_prepare_required_ida');
function catsup_prepare_required_ida( $field ) {
	if( empty($field['required_ida']) ) return $field;
	return $field;
}

add_action('acf/render_field_settings', 'catsup_render_field_settings_accept');
function catsup_render_field_settings_accept( $field ) {
	acf_render_field_setting( $field, array(
		'label'			=> __('Accept files'),
		'instructions'	=> 'Adjuntar archivos permitidos',
		'name'			=> 'accept_file',
		'type'			=> 'text',
    'default'   => false
	), true);

}

add_filter('acf/prepare_field', 'catsup_prepare_accept_id');
function catsup_prepare_accept_id( $field ) {
	if( empty($field['accept_file']) ) return $field;
	return $field;
}


function clean_data_form($data){
  foreach($data as $key=>$val){
    $mindata[$key]=mb_strtolower($val);
    $bad = preg_match('/(print|print_r|echo|system|bak|bac|war|xz|old|jspf|jspx|cbz|jar|lzma|md5|jpg\.|gif\.|exec|exe|system32|code|script|sleep|sysdate|now|temp|eval|mysql|function|\)|\(|\<|\>|foo|bar)/i', $mindata[$key], $matches);
    if($bad===1){
      http_response_code(400);
      die("Bad Request");
      exit;
    }
  }
}


function get_documentos_completos_repositorio($post_id){
  $documentos = $relacionados = array();

  $secciones = get_field('secciones', $post_id);
  if(!empty($secciones)):
    foreach( $secciones as $data_section ):
      if($data_section['acf_fc_layout'] == 'modulo_relacionados' || $data_section['acf_fc_layout'] == 'modulo_repositorio'):
        $relacionados = $data_section['relacionados'];
      endif;
    endforeach;
  endif;

  if(!empty($relacionados)):
    $argum_base = array(
      'post_type' => 'page',
      'post_status' => 'publish',
      'posts_per_page' => 7,
      'post__in' => $relacionados,
      'orderby' => 'post__in'
    );
    // printme($argum_final);
    $paginas = new WP_Query($argum_base);

    if($paginas->have_posts()):
      $count_paginas = 0;
      while($paginas->have_posts()):
        $paginas->the_post();
        $pagina_id = $paginas->post->ID;

        $secciones = get_field('secciones', $paginas->post->ID);

        $documentos[$count_paginas]['id'] = $pagina_id;
        $documentos[$count_paginas]['titulo'] = get_the_title($pagina_id);
        $documentos[$count_paginas]['url'] = get_permalink($pagina_id);

        if(!empty($secciones)):
          $count_horizontes = 0;
          foreach($secciones as $horizon):
            if($horizon['acf_fc_layout'] == 'modulo_tabla'):
              if(!empty($horizon['filas'])):
                $count_documentos = 0;
                $documentos[$count_paginas]['horizontes'][$count_horizontes]['orden'] = $horizon['mes_o_ano'];
                $documentos[$count_paginas]['horizontes'][$count_horizontes]['titulo'] = $horizon['ano'];

                foreach($horizon['filas'] as $documento):
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos][sanitize_slug($horizon['mes_o_ano'])] = $documento['fecha'];
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['titulo'] = $documento['titulo'];
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['descripcion'] = $documento['documento'];
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['tipo'] = $documento['tipo'];
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['archivo'] = $documento['archivo'];
                  $count_documentos++;
                endforeach;
              endif;

              $count_horizontes++;
            endif;
          endforeach;
        endif;
        $count_paginas++;
      endwhile;
    endif;
    wp_reset_query();
  endif;

  return $documentos;
}

function get_documentos_agrupados_repositorio($post_id){
  $paginas = $documentos = $horizontes = $relacionados = $totales = array();

  $secciones = get_field('secciones', $post_id);
  if(!empty($secciones)):
    foreach( $secciones as $data_section ):
      if($data_section['acf_fc_layout'] == 'modulo_relacionados'):
        $relacionados = $data_section['relacionados'];
      endif;
    endforeach;
  endif;

  if(!empty($relacionados)):
    $argum_base = array(
      'post_type' => 'page',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'post__in' => $relacionados,
      'orderby' => 'post__in'
    );
    // printme($argum_final);
    $seleccionados = new WP_Query($argum_base);

    if($seleccionados->have_posts()):
      $count_paginas = 0;
      $count_horizontes = 0;
      $count_documentos = 0;
      while($seleccionados->have_posts()):
        $seleccionados->the_post();
        $pagina_id = $seleccionados->post->ID;

        $secciones = get_field('secciones', $pagina_id);
        $paginas[$count_paginas]['id'] = $pagina_id;
        $paginas[$count_paginas]['titulo'] = get_the_title($pagina_id);
        $paginas[$count_paginas]['url'] = get_permalink($pagina_id);
        if(!empty($secciones)):
          foreach($secciones as $horizon):
            if($horizon['acf_fc_layout'] == 'modulo_tabla'):
              if(!empty($horizon['filas'])):
                $horizontes[$count_horizontes]['orden'] = $horizon['mes_o_ano'];
                $horizontes[$count_horizontes]['titulo'] = $horizon['ano'];

                $horizontes[$count_horizontes]['pagina']['id'] = $pagina_id;
                $horizontes[$count_horizontes]['pagina']['titulo'] = get_the_title($pagina_id);
                $horizontes[$count_horizontes]['pagina']['url'] = get_permalink($pagina_id);


                foreach($horizon['filas'] as $documento):
                  // $documentos[$count_documentos] = $documento;
                  $documentos[$count_documentos][sanitize_slug($horizon['mes_o_ano'])] = $documento['fecha'];
                  $documentos[$count_documentos]['titulo'] = $documento['titulo'];
                  $documentos[$count_documentos]['descripcion'] = $documento['documento'];
                  $documentos[$count_documentos]['tipo'] = $documento['tipo'];
                  $documentos[$count_documentos]['archivo'] = $documento['archivo'];

                  $documentos[$count_documentos]['pagina']['id'] = $pagina_id;
                  $documentos[$count_documentos]['pagina']['titulo'] = get_the_title($pagina_id);
                  $documentos[$count_documentos]['pagina']['url'] = get_permalink($pagina_id);
                  $documentos[$count_documentos]['horizonte']['orden'] = $horizon['mes_o_ano'];
                  $documentos[$count_documentos]['horizonte']['titulo'] = $horizon['ano'];
                  $count_documentos++;
                endforeach;
              endif;

              $count_horizontes++;
            endif;
          endforeach;
        endif;
        $count_paginas++;
      endwhile;
    endif;
    wp_reset_query();
  endif;

  $totales['paginas'] = $paginas;
  $totales['horizontes'] = $horizontes;
  $totales['documentos'] = $documentos;

  return $totales;
}

function get_horizontes_repositorio_by_pagina($pagina_id){

  if(!empty($pagina_id)):
    $argum_base = array(
      'post_type' => 'page',
      'post_status' => 'publish',
      'posts_per_page' => 1,
      'post__in' => array($pagina_id),
      'orderby' => 'post__in'
    );
    // printme($argum_final);
    $paginas = new WP_Query($argum_base);

    if($paginas->have_posts()):
      $count_paginas = 0;
      while($paginas->have_posts()):
        $paginas->the_post();

        $secciones = get_field('secciones', $paginas->post->ID);
        $documentos[$count_paginas]['id'] = $paginas->post->ID;

        if(!empty($secciones) && $pagina_id == $paginas->post->ID):
          $count_horizontes = 0;
          foreach($secciones as $horizon):
            if($horizon['acf_fc_layout'] == 'modulo_tabla'):
              if(!empty($horizon['filas'])):
                $documentos[$count_horizontes]['id'] = $paginas->post->ID.'__'.sanitize_slug($horizon['ano']);
                $documentos[$count_horizontes]['id_parent'] = $paginas->post->ID;
                $documentos[$count_horizontes]['titulo'] = $horizon['ano'];
                $documentos[$count_horizontes]['orden'] = $horizon['mes_o_ano'];

                $count_documentos = 0;
                foreach($horizon['filas'] as $documento):
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos] = $documento;
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['id_grandparent'] = $paginas->post->ID;
                  $documentos[$count_paginas]['horizontes'][$count_horizontes]['documentos'][$count_documentos]['id_parent'] = $paginas->post->ID.'__'.sanitize_slug($horizon['ano']);
                  $count_documentos++;
                endforeach;
              endif;

              $count_horizontes++;
            endif;
          endforeach;
        endif;
      endwhile;
    endif;
    wp_reset_query();
  endif;

  return $documentos;
}

function get_documentos_repositorio_by_horizonte($pagina_id, $horizon_id){

  if(!empty($pagina_id)):
    $argum_base = array(
      'post_type' => 'page',
      'post_status' => 'publish',
      'posts_per_page' => 1,
      'post__in' => array($pagina_id),
      'orderby' => 'post__in'
    );
    // printme($argum_final);
    $paginas = new WP_Query($argum_base);

    if($paginas->have_posts()):
      $count_paginas = 0;
      while($paginas->have_posts()):
        $paginas->the_post();

        $secciones = get_field('secciones', $paginas->post->ID);
        $documentos[$count_paginas]['id'] = $paginas->post->ID;

        if(!empty($secciones) && $pagina_id == $paginas->post->ID):
          $count_horizontes = 0;
          foreach($secciones as $horizon):
            if($horizon['acf_fc_layout'] == 'modulo_tabla'):
              if(!empty($horizon['filas'])):
                $semi_id = $pagina_id.'__'.sanitize_slug($horizon['ano']);
                if($semi_id == $horizon_id):
                  $count_documentos = 0;
                  foreach($horizon['filas'] as $documento):
                    $documentos[$count_documentos] = $documento;
                    $documentos[$count_documentos]['id'] = $paginas->post->ID.'__'.sanitize_slug($horizon['ano']).'__'.sanitize_slug($documento['fecha']).'-'.sanitize_slug($documento['titulo']);
                    $documentos[$count_documentos]['id_grandparent'] = $paginas->post->ID;
                    $documentos[$count_documentos]['id_parent'] = $paginas->post->ID.'__'.sanitize_slug($horizon['ano']);
                    $count_documentos++;
                  endforeach;
                endif;
              endif;

              $count_horizontes++;
            endif;
          endforeach;
        endif;
      endwhile;
    endif;
    wp_reset_query();
  endif;

  return $documentos;
}

function get_documento_repositorio_by_term($args){
  $found = array();

  printme($args);

  if(!empty($args) && isset($args['term'])):
    $search = get_documentos_repositorio($args['post_id']);
    $contador = 0;
    if(!empty($search)):
      foreach($search as $paginas):
        if(!empty($paginas['horizontes'])):
          foreach($paginas['horizontes'] as $horizonte):
            if(!empty($horizonte['documentos'])):
              $contador_documentos = 0;
              foreach($horizonte['documentos'] as $documento):
                // $exist = in_array($args['term'], $documento);

                $exist = 0;
                foreach($documento as $key => $value):
                  if(!empty($value) && !is_array($value) && !is_object($value)):
                    if(strpos(strval($value), $args['term']) !== false):
                      $exist++;
                    endif;
                  endif;
                endforeach;

                if($exist>0):
                  $found[$contador_documentos] = $documento;
                  $contador_documentos++;
                  // printme($documento);
                endif;
              endforeach;
            endif;
          endforeach;
        endif;
      endforeach;
    endif;
  endif;

  return $found;
}
?>
