<?php
get_header();
the_post();
?>
<main>

	<section id="one" class="horizon horizon--bg margin-vertical-triple"
		style="background-image: url('https://unsplash.it/g/1200/600?gravity=center'); margin: 3rem 0!important;">
		<div class="horizon__veil">
			<div class="container">
				<div class="row">
					<div class="gr-6 gr-12@book gutter-double@book">
						<h2 class="horizon__title gutter-right-double">Documentos de interés</h2>
						<div class="horizon__excerpt font-justified gutter-right-double">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
								labore et dolore magna aliqua.</p>
							<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
								pariatur.</p>
							<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
								id est laborum.</p>
						</div>
					</div>
					<div class="gr-6 gr-12@book gutter-double@book margin-top-big">
						<ul class="box__downloads no-margin-bottom no-gutter text-small ">
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Administración</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Estatutos</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Manual de manejo de
								información de interés para el mercado</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Operaciones con partes
								relacionadas</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Procedimientos de
								denuncias Ley 20.393</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Prácticas de Gobierno
								Corporativo</a>
							</li>
							<li class="w-bold no-gutter-important">
								<i class="font-color-grey-lightest icon icon-download"><a href="#"
										class="font-color-grey-lightest link-decorated gutter-left"></i>Código de Ética y Conducta
								Organizacional</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="two" class="horizon horizon--float">

		<div class="container">
			<div class="row heels-small">
				<div class="gr-8 gr-10@book gr-12@tablet gr-centered">
					<h2 class="horizon__title title-center">Nuestras marcas en números</h2>
					<div class="horizon__excerpt text-center">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
							labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
							nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
		</div>

		<div data-equalize="target" data-mq="tablet-down" data-eq-target="[data-eq]">

			<div class="container container--float no-gutter hide@tablet">
				<div class="row">
					<div class="gr-5 gr-6@medium gr-12@tablet no-gutter">
						<div class="float__square bg-dark" data-eq></div>
					</div>
					<div class="gr-7 gr-6@medium hide@tablet no-gutter">
						<div class="float__square" data-eq data-role="dinamic-image"
							style="background-image: url(http://unsplash.it/1200/700?gravity=east)"></div>
					</div>
				</div>
			</div>

			<div class="container no-gutter@tablet">

				<div class="slider slider--diapo slider--blocks" data-module="slider" data-transition="false">
					<div class="slider__items" data-role="slider-list">

						<div class="slider__slide current" data-role="slider-slide"
							data-bg-img="http://unsplash.it/1200/700?gravity=east">
							<div class="row">
								<div class="gr-5 gr-6@medium gr-12@tablet no-gutter-vertical">
									<article class="box box--square bg-dark" data-eq>
										<div class="box__body gr-12@book gutter-double@tablet">
											<h2 class="box__title title-decorated">Slide 1</h2>
											<div class="box__excerpt">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
													nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											</div>
											<div class="box__action">
												<a href="#" class="button button--ghost-white" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@medium gr-12@tablet no-gutter@tablet">
									<figure class="box__figure show@tablet">
										<img src="http://unsplash.it/1200/700?gravity=east" alt="" class="cover-img">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide"
							data-bg-img="http://unsplash.it/1200/700?gravity=west">
							<div class="row">
								<div class="gr-5 gr-6@medium gr-12@tablet no-gutter-vertical">
									<article class="box box--square bg-dark" data-eq>
										<div class="box__body gr-12@book gutter-double@tablet">
											<h2 class="box__title title-decorated">Slide 2</h2>
											<div class="box__excerpt">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
													nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											</div>
											<div class="box__action">
												<a href="#" class="button button--ghost-white" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@medium gr-12@tablet no-gutter@tablet">
									<figure class="box__figure show@tablet">
										<img src="http://unsplash.it/1200/700?gravity=west" alt="" class="cover-img">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide"
							data-bg-img="http://unsplash.it/1200/700?gravity=south">
							<div class="row">
								<div class="gr-5 gr-6@medium gr-12@tablet no-gutter-vertical">
									<article class="box box--square bg-dark" data-eq>
										<div class="box__body gr-12@book gutter-double@tablet">
											<h2 class="box__title title-decorated">Slide 3</h2>
											<div class="box__excerpt">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
													nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
											</div>
											<div class="box__action">
												<a href="#" class="button button--ghost-white" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@medium gr-12@tablet no-gutter@tablet">
									<figure class="box__figure show@tablet">
										<img src="http://unsplash.it/1200/700?gravity=south" alt="" class="cover-img">
									</figure>
								</div>
							</div>
						</div>
					</div>
					<div class="arrows-container gr-5 gr-6@medium no-gutter@medium gr-12@tablet">
						<div class="slider__arrows">
							<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
								data-direction="prev"></button>
							<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
								data-direction="next"></button>
						</div>
					</div>
					<div class="bullets-container prefix-5 prefix-6@medium gr-12@tablet prefix-0@tablet">
						<div class="slider__bullets light">
							<button class="slider__bullet slider__bullet--current" data-role="slider-bullet"
								data-target="0"></button>
							<button class="slider__bullet" data-role="slider-bullet" data-target="1"></button>
							<button class="slider__bullet" data-role="slider-bullet" data-target="2"></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="three" class="horizon horizon--marcas" data-equalize="target" data-mq="false"
		data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-6 gr-12@tablet no-gutter">
					<div class="float__square bg-dark" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row">
				<div class="gr-4 gr-5@book gr-12@tablet flex-center">
					<article class="box box--simple text-light">
						<div class="box__body">
							<span class="box__counter">01</span>
							<h2 class="box__title title-decorated medium">Diseño y venta de valor</h2>
							<div class="box__excerpt">
								<p>Cada equipo de Empresas Socovesa está llamado a poner en el centro de la toma de decisiones
									de diseño, las necesidades del habitar de las personas. Poder transmitir el valor de esas
									ideas y atributos que dieron forma a un proyecto de calidad, es el proceso que da un cierre
									perfecto a un buen diseño. </p>
							</div>
						</div>
					</article>
				</div>
				<div class="gr-7 prefix-1 prefix-0@book gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=center" alt="" class="cover-img" />
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section id="four" class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-6 gr-12@tablet prefix-6 prefix-0@tablet no-gutter">
					<div class="float__square bg-main" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row col-reverse@tablet">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=south" alt="" class="cover-img" />
					</figure>
				</div>
				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book flex-center">
					<article class="box box--simple text-light">
						<div class="box__body">
							<span class="box__counter">02</span>
							<h2 class="box__title title-decorated medium">Transformación digital de la construcción</h2>
							<div class="box__excerpt">
								<p>Un buen diseño debe concluir en una buena ejecución. Para aumentar la precisión y
									predictibilidad del proceso constructivo, en 2017 nace el Bim Lab de Empresas Socovesa. </p>
								<p>Esta unidad de investigación y desarrollo estudia, testea y modela proyectos inmobiliarios en
									3D con la metodología Building Intelligence Modelling, conocida mundialmente por su capacidad
									de diseñar edificaciones mejor pensadas y anticiparse a los desafíos de la obra.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="five" class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 gr-12@tablet prefix-2 prefix-0@tablet no-gutter">
					<div class="bg-grey-lightest" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row col-reverse@tablet">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=south" alt="" class="cover-img" />
					</figure>
				</div>
				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book flex-center">
					<article class="box box--simple">
						<div class="box__body">
							<!-- <span class="box__counter">02</span> -->
							<h2 class="box__title title-decorated medium">Comunicar valor</h2>
							<div class="box__excerpt">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
									labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
									commodo consequat.</p>
							</div>
							<div class="box__action hat-small">
								<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="six" class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 gr-12@tablet prefix-2 prefix-0@tablet no-gutter">
					<div class="bg-light-ice" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row col-reverse@tablet">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=south" alt="" class="cover-img" />
					</figure>
				</div>
				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book flex-center">
					<article class="box box--simple">
						<div class="box__body">
							<span class="box__counter">02</span>
							<h2 class="box__title title-decorated medium">Poner al cliente en la conversación</h2>
							<div class="box__excerpt">
								<p>Nuestro negocio comienza y termina en el cliente. Y esto es más que un simple decir: como
									compañia hemos fortalecido
									la capacidad de poner a la persona y sus necesidades al centro de cada decisión
									organizacional.</p>
							</div>
							<!-- <div class="box__action hat-small">
                <a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
              </div> -->
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="seven" class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
		data-horizon>
		<div class="container container--float no-gutter">
			<div class="row">
				<div class="gr-6 prefix-6 gr-12@tablet prefix-0@tablet no-gutter">
					<figure class="horizon__bg" style="background-image: url(http://unsplash.it/1200/600?gravity=east)"
						data-eq></figure>
				</div>
			</div>
		</div>
		<div class="container full-height margin--top no-gutter@tablet">
			<div class="row relative" data-eq>
				<div class="gr-7 gr-12@tablet no-gutter@tablet">
					<article class="box box--float box--float--left bg-white">
						<div class="box__body">
							<h2 class="box__title title-decorated">Design thinking</h2>
							<div class="box__excerpt">
								<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde officiis esse ut nam possimus
									tenetur. Quibusdam iure iusto nobis qui quia quisquam, illo dolor, molestias magnam, animi
									tempora! Libero, eaque?.</p>
							</div>
							<div class="box__action hat-small">
								<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="eight" class="horizon horizon--normal" data-equalize="target" data-mq="tablet-down"
		data-eq-target="[data-eq]">
		<div class="container container--float full-absolute-fam@tablet">
			<div class="container container--float-full no-gutter">
				<div class="row">
					<div class="gr-12 gutter-triple-top no-gutter@medium">
						<div class="float__square bg-light-ice" data-eq></div>
					</div>
				</div>
			</div>
		</div>
		<section class="container takeoff">
			<div class="row takeoff__head" data-eq>
				<div class="gr-8 gr-7@book gr-6@tablet gr-12@small no-gutter">
					<figure class="takeoff__figure"><img src="http://unsplash.it/800/450?gravity=south" alt=""
							class="cover-img" /></figure>
				</div>
				<div class="gr-4 gr-5@book gr-6@tablet gr-12@small gutter-double flex-center">
					<h2 class="takeoff__title">Desarrollo sostenible y vinculación con el entorno</h2>
				</div>
			</div>
			<div class="row takeoff__body">
				<div class="gr-10 gr-11@book gr-12@tablet prefix-2 prefix-1@book prefix-0@tablet no-gutter">
					<div class="takeoff__excerpt">
						<div class="row">
							<div class="gr-6 gr-12@small">
								<p>Las personas valoran el habitar doméstico tanto como ser parte de un sistema público más
									amplio y complejo: el barrio y la ciudad. Reconocer este doble vínculo implica diseñar
									proyectos pensando de adentro hacia afuera y de afuera hacia adentro.</p>
							</div>
							<div class="gr-6 gr-12@small">
								<p>Al mismo tiempo, estamos conscientes de la relevancia de una relación directa y transparente
									con los vecinos y la comunidad a la hora de alcanzar una mayor legitimidad en el desarrollo
									urbano e inmobiliario. Nuestras puertas están siempre abiertas al diálogo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

	<section id="nine" class="horizon horizon--normal" data-equalize="target" data-mq="tablet-down"
		data-eq-target="[data-eq]">
		<div class="container container--float full-absolute-fam@tablet">
			<div class="container container--float-full no-gutter">
				<div class="row">
					<div class="gr-12 gutter-triple-top no-gutter@medium">
						<div class="float__square bg-light-grey" data-eq></div>
					</div>
				</div>
			</div>
		</div>
		<section class="container takeoff">
			<div class="row takeoff__head col-reverse@small" data-eq>
				<div class="gr-4 gr-5@book gr-6@tablet gr-12@small gutter-double flex-center">
					<h2 class="takeoff__title inverted">Modernización organizacional y cambio de estructura</h2>
				</div>
				<div class="gr-8 gr-7@book gr-6@tablet gr-12@small no-gutter">
					<figure class="takeoff__figure">
						<img src="http://unsplash.it/800/450?gravity=north" alt="" class="cover-img" />
					</figure>
				</div>
			</div>
			<div class="row takeoff__body">
				<div class="gr-10 gr-11@book gr-12@tablet suffix-2 suffix-1@book suffix-0@tablet no-gutter">
					<div class="takeoff__excerpt">
						<div class="row">
							<div class="gr-6 gr-12@small">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora. Estos pilares son establecidos entre todas las áreas de
									Empresas Socovesas que participan en el proceso de planning, ya que sus características
									traspasarán todos los aspectos del proyecto.</p>
							</div>
							<div class="gr-6 gr-12@small">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

	<section id="ten" class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
		data-horizon>
		<div class="container full-height no-gutter@tablet">
			<div class="row floater no-left@tablet gutter-vertical-double">
				<div class="gr-12 no-gutter@tablet">
					<figure class="horizon__bg" style="background-image: url(http://unsplash.it/1200/800?gravity=west)">
					</figure>
				</div>
			</div>
			<div class="row relative">
				<div class="gr-5 gr-8@book gr-12@tablet no-gutter">
					<article class="box box--float box--float--top box--float--big bg-white">
						<div class="box__body">
							<!-- <span class="box__counter">02</span> -->
							<h2 class="box__title title-decorated">Directorio</h2>
							<div class="box__excerpt">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
							</div>
							<div class="box__action font-righted">
								<a href="#" class="button button--main" title="">Ver equipo</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="eleven" class="horizon horizon--float" data-equalize="target" data-mq="false"
		data-eq-target="[data-eq]">
		<div class="container full-height no-gutter@tablet">
			<div class="row floater no-left@tablet gutter-vertical-double">
				<div class="gr-12 no-gutter@tablet">
					<figure class="horizon__bg" style="background-image: url(http://unsplash.it/1200/800?gravity=west)"
						data-eq></figure>
				</div>
			</div>
			<div class="row relative">
				<div class="gr-5 gr-8@book gr-12@tablet prefix-7 prefix-4@book prefix-0@tablet no-gutter">
					<article class="box box--float box--float--top box--float--big bg-white" data-eq>
						<div class="box__body">
							<h2 class="box__title title-decorated title-decorated-black">Una empresa de diseño centrada en el
								habitar</h2>
							<div class="box__excerpt">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="twelve" class="horizon horizon--float" data-equalize="target" data-mq="tablet-down"
		data-eq-target=".box__body">
		<div class="container no-gutter@book">
			<div class="row">
				<div class="gr-6 gr-12@small">
					<article class="box box--people">
						<figure class="box__figure">
							<img src="http://unsplash.it/g/300/300/?gravity=east" alt="" class="cover-img" />
						</figure>
						<div class="box__body">
							<span class="box__meta">Presidente</span>
							<h2 class="box__title">Javier Gras Rudloff</h2>
							<div class="box__excerpt">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor
									diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de
									planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
							</div>
					</article>
				</div>
				<div class="gr-6 gr-12@small">
					<article class="box box--people">
						<figure class="box__figure">
							<img src="http://unsplash.it/g/300/300/?gravity=east" alt="" class="cover-img" />
						</figure>
						<div class="box__body">
							<span class="box__meta">Director</span>
							<h2 class="box__title">Lorenzo Ipsum Dolores</h2>
							<div class="box__excerpt">
								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis dolorem maiores nisi
									praesentium error ut quis aspernatur, cupiditate adipisci molestiae velit ipsum. Quas sunt
									perferendis dolores commodi sapiente praesentium possimus.</p>
							</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="thirteen" class="horizon horizon--normal bg-light-ice">
		<div class="container">
			<div class="row">
				<div class="gr-6 gr-12@tablet">
					<article class="box box--square">
						<div class="box__body no-gutter">
							<h2 class="box__title title-regular">Análisis 3d</h2>
							<div class="box__excerpt">
								<p>Se entiende el proyecto en su contexto. Se evalúa la composición sociodemográfica y las
									dinámicas del entorno: cercanía al transporte público, comercio, servicios y todo lo que
									tenga relación con la calidad de vida de los usuarios.</p>
								<p>Se estudia el mercado en cuanto a superficie, mix, distribución, terminaciones, facilities,
									espacios comunes, fachada, precios y otras características relevantes.
									Por último, se lleva a cabo un acercamiento cualitativo y cuantitativo al consumidor
									potencial y final. El objetivo es conocer su estructura familiar, su etapa de vida, sus
									dinámicas, sus ideas y todos los anhelos que podamos interpretar al momento de diseñar. </p>
							</div>
						</div>
					</article>
				</div>
				<div class="gr-6 prefix-0@medium gr-12@tablet flex-middle flex-center">
					<figure class="gutter-double">
						<img src="<?php echo get_template_directory_uri(); ?>/dist/img/content/icono-diseno-3d.svg"
							alt="Imagen de " class="elastic-img">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section id="fourteen" class="horizon horizon--marcas no-gutter@tablet">
		<div class="container heels@tablet">
			<div class="row">
				<div class="gr-8 gr-10@medium gr-12@small gr-centered">
					<h2 class="horizon__title title-center">Te presentamos nuestro programa de apoyo educacional</h2>
					<div class="horizon__excerpt">
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque eius hic voluptates quas architecto
							qui? Cum adipisci impedit reiciendis, commodi eligendi dolor quaerat vel ea nulla saepe deleniti
							cupiditate molestias.</p>
					</div>
				</div>
			</div>
		</div>
		<section class="horizon horizon--marcas">
			<div class="container container--float-full no-gutter">
				<div class="row row--absolute">
					<div class="gr-10 no-gutter gr-12@medium">
						<div class="float__square bg-light-ice"></div>
					</div>
				</div>
			</div>
			<div class="container full-height">
				<div class="slider slider--diapo slider--blocks" data-module="slider" data-transition="false">
					<div class="slider__items" data-role="slider-list" data-equalize="target" data-mq="medium-down"
						data-eq-target=".row">
						<div class="slider__slide current" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">2018</h2>
											<div class="box__excerpt">
												<p>Empresas Socovesa demuestra una vez más su capacidad para adaptarse a los
													ciclos económicos del país y
													supera el nivel
													de promesas netas comprometidas en 2017, al mismo tiempo que conserva los
													equilibrios inmobiliarios dentro
													de los parámetros
													definidos.</p>
												<p>Este año la Compañía inicia un proceso de modernización de la compañía, a
													partir el cual se decide que,
													entre el 1 de enero
													de 2019 y los siguientes cinco años, la matriz y todas las filiales - con
													excepción de Socovesa Sur que
													seguirá operando en
													Temuco - instalarán sus oficinas en un mismo edificio corporativo en la comuna
													de providencia.</p>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=south">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">1979</h2>
											<div class="box__excerpt">
												<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque facilis
													delectus mollitia et, ipsam,
													nihil praesentium, temporibus saepe officiis asperiores aliquam? Iste, minus
													perspiciatis error maxime
													sunt at similique. Quis.</p>
												<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque facilis
													delectus mollitia et, ipsam,
													nihil praesentium, temporibus saepe officiis asperiores aliquam? Iste, minus
													perspiciatis error maxime
													sunt at similique. Quis.</p>
											</div>
											<div class="box__action hat-small">
												<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=center">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">1983</h2>
											<div class="box__excerpt">
												<p>Empresas Socovesa demuestra una vez más su capacidad para adaptarse a los
													ciclos económicos del país y
													supera el nivel
													de promesas netas comprometidas en 2017, al mismo tiempo que conserva los
													equilibrios inmobiliarios dentro
													de los parámetros
													definidos.</p>
												<p>Este año la Compañía inicia un proceso de modernización de la compañía, a
													partir el cual se decide que,
													entre el 1 de enero
													de 2019 y los siguientes cinco años, la matriz y todas las filiales - con
													excepción de Socovesa Sur que
													seguirá operando en
													Temuco - instalarán sus oficinas en un mismo edificio corporativo en la comuna
													de providencia.</p>
											</div>
											<div class="box__action hat-small">
												<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=north">
									</figure>
								</div>
							</div>
						</div>
					</div>
					<div class="container container--float dates-container gr-8 prefix-2 gr-12@book font-centered no-gutter">
						<div class="slider__dates bg-white">
							<a class="slider__date slider__date--prev" data-role="slider-arrow" data-direction="prev"></a>
							<a class="slider__date slider__bullet--current" data-role="slider-bullet" data-target="0">1965</a>
							<a class="slider__date" data-role="slider-bullet" data-target="1">1979</a>
							<a class="slider__date" data-role="slider-bullet" data-target="2">1983</a>
							<a class="slider__date slider__date--next" data-role="slider-arrow" data-direction="next"></a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

	<section id="fiveteen" class="horizon horizon--marcas">
		<div class="container">
			<h2 class="horizon__title title-center">Ubicación de nuestros proyectos</h2>
			<div class="tab__wrapper hat-small" data-role="tab-parent">
				<div class="tab__controls" data-role="tab-controls">
					<button class="tab__deployer no-touch active" data-func="tabControl"
						data-target="tab-almagro"><span>Almagro</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-socovesa"><span>Socovesa</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-pilares"><span>Pilares</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-desarrollo-comercial"><span>Desarrollo Comercial</span></button>
				</div>
				<div class="tab__contents" data-equalize="target" data-mq="small-down" data-eq-target=".row">
					<div class="tab__target active" data-tab-name="tab-almagro">

						<div class="row" data-role="ciudad-parent" data-color-active="#E75D5D" data-color-preview="#F19E9E">
							<div class="gr-3 gr-4@medium gr-12@small flex-center">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>

					</div>
					<div class="tab__target" data-tab-name="tab-socovesa">
						<div class="row" data-role="ciudad-parent" data-color-active="#98dff1" data-color-preview="#cceff9">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab__target" data-tab-name="tab-pilares">
						<div class="row" data-role="ciudad-parent" data-color-active="#ff9016" data-color-preview="#ffbc73">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab__target" data-tab-name="tab-desarrollo-comercial">
						<div class="row" data-role="ciudad-parent" data-color-active="#9695d3" data-color-preview="#c0bfe5">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="sixteen" class="horizon horizon--marcas" data-equalize="target" data-mq="false"
		data-eq-target="[data-eq]" data-eq>
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-5 gr-6@medium hide@tablet no-gutter">
				</div>
				<div class="gr-7 gr-6@medium gr-12@tablet no-gutter">
					<div class="float__square bg-light-pink" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height no-gutter@tablet" data-rellax data-rellax-speed="3">
			<div class="horizon__logo flex-right flex-center@small">
				<img src="https://placehold.it/120x120?text=almagro" class="elastic-img" alt="Ir a " />
			</div>
			<div class="row" data-equalize="target" data-mq="small-down" data-eq-target="[data-equalize]">
				<div class="gr-6 gr-7@large gr-8@medium gr-12@small no-gutter@medium">
					<article class="box box--area bg-white" data-equalize>
						<div class="box__body">
							<h2 class="box__title title-decorated-dark">Vivir superior</h2>
							<div class="box__excerpt">
								<p>Almagro es el mayor desarrollador en altura de la Región Metropolitana y Antofagasta. ¿Cómo
									se consigue un liderazgo así? Con más de 40 años de historia, exigentes criterios de diseño
									que piensan en el habitar, una construcción de calidad, respetando el entorno urbano y una
									perfomance de alto nivel en todos los puntos de contacto con los clientes. </p>
								<p>Esta filial de Empresas Socovesa es una compañía de alta percepción de valor, que sin dejar
									su histórico en los segmentos alto y de lujo, ha fortalecido su portafolio en comunas como
									Ñuñoa, Santiago Centro y San Miguel.</p>
								<p>En Almagro hacemos las cosas bien por dentro y por fuera. Más de 16.000 viviendas en Santiago
									y regiones respaldan esta experiencia.</p>
							</div>
						</div>
					</article>
				</div>

				<div class="gr-5 prefix-1 gr-4@large prefix-0@medium gr-12@small no-gutter@medium">
					<div class="box box--media">
						<video data-equalize data-role="video-player"
							src="<?php echo get_template_directory_uri(); ?>/dist/samples/sample-video.mp4"
							type="video/mp4"></video>
						<div class="video__control">
							<button class="video__control__deployer" data-role="video-player-deployer"></button>
						</div>
					</div>
				</div>
			</div>

			<div class="slider slider--single hat" data-module="slider" data-transition="false">
				<div class="slider__items" data-role="slider-list">

					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=north" class="cover-img hide@tablet" alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=north" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=north" class="cover-img show@phablet" alt="title" />
						</figure>
					</div>
					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=south" class="cover-img hide@tablet" alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=south" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=south" class="cover-img show@phablet" alt="title" />
						</figure>
					</div>
					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=center" class="cover-img hide@tablet"
								alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=center" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=center" class="cover-img show@phablet"
								alt="title" />
						</figure>
					</div>


				</div>
				<div class="slider__arrows">
					<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
						data-direction="prev"></button>
					<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
						data-direction="next"></button>
				</div>
				<div class="slider__bullets">
					<button class="slider__bullet slider__bullet--current" data-role="slider-bullet"
						data-target="0"></button>
					<button class="slider__bullet" data-role="slider-bullet" data-target="1"></button>
					<button class="slider__bullet" data-role="slider-bullet" data-target="2"></button>
				</div>
			</div>
			<div class="horizon__action flex-center hat">
				<a href="#" class="button button--ghost-dark button--full" title="Ir a ">Ir a almagro.cl</a>
			</div>
		</div>
	</section>

	<section id="seventeen" class="horizon horizon--marcas" data-equalize="target" data-mq="false"
		data-eq-target="[data-eq]" data-eq>
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-7 gr-6@medium gr-12@tablet no-gutter">
					<div class="float__square bg-light-purple" data-eq></div>
				</div>
				<div class="gr-5 gr-6@medium hide@tablet no-gutter">
				</div>
			</div>
		</div>
		<div class="container full-height no-gutter@tablet" data-rellax data-rellax-speed="3">
			<div class="horizon__logo flex-left flex-center@small">
				<img src="https://placehold.it/300x70?text=socovesa" class="elastic-img" alt="Ir a " />
			</div>
			<div class="row col-reverse@small" data-equalize="target" data-mq="small-down"
				data-eq-target="[data-equalize]">
				<div class="gr-5 suffix-1 gr-4@large suffix-0@medium gr-12@small no-gutter@medium">
					<div class="box box--media">
						<video data-equalize data-role="video-player"
							src="<?php echo get_template_directory_uri() ?>/dist/samples/sample-video.mp4"
							type="video/mp4"></video>
						<div class="video__control">
							<button class="video__control__deployer" data-role="video-player-deployer"></button>
						</div>
					</div>
				</div>
				<div class="gr-6 gr-7@large gr-8@medium gr-12@small no-gutter@medium">
					<article class="box box--area bg-white" data-equalize>
						<div class="box__body">
							<h2 class="box__title title-decorated-dark">Todo es diseño</h2>
							<div class="box__excerpt">
								<p>Las personas son el origen y la razón de ser del buen diseño. Para esta filial de Empresas
									Socovesa, el diseño de una casa o un departamento lo es todo. Debe ser funcional, estético, y
									al mismo tiempo, ser capaz de ofrecer espacios amplios y luminosos, a la medida de la vida
									actual.</p>
								<p>Para eso, su modelo de trabajo integra información desde diferentes lugares: la demografía,
									una observación atenta de los cambios sociales, la geografía y el territorio, las
									conductas, la psicología y los distintos estilos de vida.</p>
								<p>El foco de Socovesa está tanto en el segmento tradicional de casas, como en el segmento de
									casas con subsidio y recientemente, en el segmento de edificios pericentrales. Su estrategia
									busca diferenciación a través del diseño centrado en el habitar de las personas. </p>
								<p>La experiencia de compra tiene un valor crucial en Socovesa: gran parte de la energía del
									equipo está dedicada a entender profundamente quiénes son los compradores y a afinar las
									propuestas de valor de los proyectos. Solo así se logra el calce perfecto.</p>
							</div>
						</div>
					</article>
				</div>

			</div>

			<div class="slider slider--single hat" data-module="slider" data-transition="false">
				<div class="slider__items" data-role="slider-list">
					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=north" class="cover-img hide@tablet" alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=north" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=north" class="cover-img show@phablet" alt="title" />
						</figure>
					</div>
					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=south" class="cover-img hide@tablet" alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=south" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=south" class="cover-img show@phablet" alt="title" />
						</figure>
					</div>
					<div class="slider__slide current" data-role="slider-slide">
						<figure class="slider__figure">
							<img src="https://unsplash.it/1024/512/?gravity=center" class="cover-img hide@tablet"
								alt="title" />
							<img src="https://unsplash.it/800/600/?gravity=center" class="cover-img show@tablet hide@phablet"
								alt="title" />
							<img src="https://unsplash.it/576/768/?gravity=center" class="cover-img show@phablet"
								alt="title" />
						</figure>
					</div>
				</div>
				<div class="slider__arrows">
					<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
						data-direction="prev"></button>
					<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
						data-direction="next"></button>
				</div>
				<div class="slider__bullets">
					<button class="slider__bullet slider__bullet--current" data-role="slider-bullet"
						data-target="0"></button>
					<button class="slider__bullet" data-role="slider-bullet" data-target="1"></button>
					<button class="slider__bullet" data-role="slider-bullet" data-target="2"></button>
				</div>
			</div>
			<div class="horizon__action flex-center hat">
				<a href="#" class="button button--ghost-dark button--full" title="Ir a ">Ir a socovesa.cl</a>
			</div>
		</div>
	</section>

	<section id="eighteen" class="horizon horizon--marcas no-gutter@tablet">
		<div class="container heels@tablet">
			<div class="row">
				<div class="gr-8 gr-10@medium gr-12@small gr-centered">
					<h2 class="horizon__title title-center">Te presentamos nuestro programa de apoyo educacional</h2>
					<div class="horizon__excerpt font-center">
						<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolore provident itaque est ea quos. Odit
							recusandae, tempora est aperiam dignissimos ab? Blanditiis debitis culpa ab quasi aut illo.
							Nostrum, animi!</p>
					</div>
				</div>
			</div>
		</div>
		<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
			<div class="container container--float-full no-gutter" data-eq>
				<div class="row">
					<div class="gr-10 no-gutter gr-12@tablet">
						<div class="float__square bg-light-ice" data-eq></div>
					</div>
				</div>
			</div>
			<div class="container full-height" data-rellax data-rellax-speed="3">
				<div class="row heels heels-not@small" data-equalize="target" data-mq="small-down"
					data-eq-target="[data-equalize]">
					<div class="gr-4 gr-5@book gr-12@tablet">
						<article class="box box--simple" data-equalize>
							<div class="box__body">
								<h2 class="box__title title-decorated-dark medium">Coaching vocacional</h2>
								<div class="box__excerpt">
									<p>Elegir una carrera técnica o profesional es un desafío para cualquier estudiante. Con las
										mentorías 1 a 1 y gracias al aporte de los trabajadores de Empresas Socovesa, los alumnos
										de los Colegios Emprender pueden tomar esa decisión con tranquilidad, Lo que más valoran
										quienes han vivido esta experiencia, es contar con la oportunidad de vivir un día laboral
										al interior de la compaía, justamente en sus áreas de interés.</p>
								</div>
							</div>
						</article>
					</div>
					<div class="gr-7 prefix-1 prefix-0@book gr-12@tablet flex-center">
						<figure class="box__figure">
							<img src="http://unsplash.it/700/400?gravity=center" alt="" class="cover-img" />
						</figure>
					</div>
				</div>

				<div class="grid grid-cols-5 grid-cols-2@tablet grid-cols-1@phablet heels" id="people-grid">
					<div class="grid-item">
						<img src="http://unsplash.it/400/400?gravity=center" alt="" class="cover-img" />
					</div>
					<div class="grid-item">
						<img src="http://unsplash.it/400/400?gravity=south" alt="" class="cover-img" />
					</div>
					<div class="grid-item">
						<img src="http://unsplash.it/400/400?gravity=west" alt="" class="cover-img" />
					</div>
					<div class="grid-item">
						<img src="http://unsplash.it/400/400?gravity=east" alt="" class="cover-img" />
					</div>
					<div class="grid-item">
						<img src="http://unsplash.it/400/400" alt="" class="cover-img" />
					</div>
				</div>
			</div>
		</section>
	</section>

	<section id="nineteen" class="horizon horizon--marcas" data-equalize="target" data-mq="false"
		data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 prefix-2 gr-12@tablet prefix-0@tablet no-gutter">
					<div class="float__square bg-light-ice" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height" data-rellax data-rellax-speed="3">
			<div class="row heels heels-not@small col-reverse@tablet" data-equalize="target" data-mq="tablet-down"
				data-eq-target="[data-equalize]">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=center" alt="" class="cover-img" />
					</figure>
				</div>

				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book">
					<article class="box box--simple" data-equalize>
						<div class="box__body">
							<h2 class="box__title title-decorated-dark medium">Fondos de emprendimiento</h2>
							<div class="box__excerpt">
								<p>Desde 2017, Empresas Socovesa destina un fondo anual para hacer realidad los emprendimientos
									de los estudiantes de los colegios Emprender. Además existen instancias de voluntariado
									corporativo para entregar asesoría en la defisión y seguimiento del plan de negocios de los
									participantes seleccionados.</p>
							</div>
						</div>
					</article>
				</div>
			</div>

			<div class="row" data-equalize="target" data-mq="small-down" data-eq-target="[data-equal-media]">
				<div class="gr-4 gr-5@book gr-6@tablet gr-12@small">
					<div class="box box--media">
						<video data-equal-media data-role="video-player"
							src="<?php echo get_template_directory_uri(); ?>/dist/samples/sample-video.mp4"
							type="video/mp4"></video>
						<div class="video__control">
							<button class="video__control__deployer" data-role="video-player-deployer"></button>
						</div>
					</div>
				</div>

				<div class="gr-7 gr-6@tablet gr-12@small prefix-1 prefix-0@book">
					<div class="slider slider--single" data-module="slider" data-transition="false">
						<div class="slider__items" data-role="slider-list" data-equal-media>

							<div class="slider__slide current" data-role="slider-slide">
								<figure class="slider__figure">
									<img src="https://unsplash.it/1024/512/?gravity=north" class="cover-img hide@tablet"
										alt="title" />
									<img src="https://unsplash.it/800/600/?gravity=north"
										class="cover-img show@tablet hide@phablet" alt="title" />
									<img src="https://unsplash.it/576/768/?gravity=north" class="cover-img show@phablet"
										alt="title" />
								</figure>
							</div>
							<div class="slider__slide current" data-role="slider-slide">
								<figure class="slider__figure">
									<img src="https://unsplash.it/1024/512/?gravity=south" class="cover-img hide@tablet"
										alt="title" />
									<img src="https://unsplash.it/800/600/?gravity=south"
										class="cover-img show@tablet hide@phablet" alt="title" />
									<img src="https://unsplash.it/576/768/?gravity=south" class="cover-img show@phablet"
										alt="title" />
								</figure>
							</div>
							<div class="slider__slide current" data-role="slider-slide">
								<figure class="slider__figure">
									<img src="https://unsplash.it/1024/512/?gravity=center" class="cover-img hide@tablet"
										alt="title" />
									<img src="https://unsplash.it/800/600/?gravity=center"
										class="cover-img show@tablet hide@phablet" alt="title" />
									<img src="https://unsplash.it/576/768/?gravity=center" class="cover-img show@phablet"
										alt="title" />
								</figure>
							</div>
						</div>
						<div class="slider__arrows">
							<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
								data-direction="prev"></button>
							<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
								data-direction="next"></button>
						</div>
						<div class="slider__bullets">
							<button class="slider__bullet slider__bullet--current" data-role="slider-bullet"
								data-target="0"></button>
							<button class="slider__bullet" data-role="slider-bullet" data-target="1"></button>
							<button class="slider__bullet" data-role="slider-bullet" data-target="2"></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="twenty" class="horizon horizon--normal">
		<div class="container">
			<div class="row minibanner">
				<div class="gr-6 gr-12@tablet minibanner__info">
					<div class="minibanner__info__col">
						<h2 class="minibanner__title">Socovesa S.A.</h2>
						<p>Nemotécnico: Socovesa</p>
					</div>
					<div class="minibanner__info__col">
						<p>Valor acción</p>
						<p class="minibanner__title">430,000</p>
					</div>
				</div>
				<div class="gr-6 gr-12@tablet minibanner__mensaje">
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus a quidem repudiandae quos dolore
						ullam
						repellat
						adipisci expedita accusamus! Repudiandae saepe soluta, nemo unde cum iste fugit reiciendis libero
						molestias.</p>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
