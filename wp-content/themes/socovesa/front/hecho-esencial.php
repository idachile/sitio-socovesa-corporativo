<?php
get_header();
the_post();
?>
<main>

  <section id="one" class="horizon horizon--main" data-horizon>
		<section class="main__top">
			<figure class="main__top__figure">
				<div class="main__top__bg" style="background-image: url('https://unsplash.it/1400/700?gravity=center')"></div>
			</figure>
			<div class="main__top__body">
				<div class="container">
					<div class="main__top__content">
						<h1 class="main__title">El buen diseño es un buen negocio</h1>
						<div class="main__excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

  <section id="two" class="horizon horizon--main hat heels">
    <div class="container">
      <h1 class="horizon__title heels">Hechos esenciales</h1>
      <div class="row bg-main-lightest">
        <form action="#" class="flex-wrap no-border">
          <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
            <select id="year" class="form-control__field--select form-control__field form-control__select icon-arrow_drop_down">
              <option value="">Año</option>
              <option>2019</option>
              <option>1994</option>
            </select>
          </div>
          <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
            <!-- <label for="month">Mes</label> -->
            <select id="month" class="form-control__field--select form-control__field">
              <option value="">Mes</option>
              <option>Marzo</option>
              <option>Mayo</option>
            </select>
          </div>
          <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
            <!-- <label for="docs">Tipo de documento</label> -->
            <select id="docs" class="form-control__field--select form-control__field">
              <option value="">Tipo de documento</option>
              <option>Texto</option>
              <option>Video</option>
            </select>
          </div>
          <div class="gr-3 gr-12@medium gutter-double gutter@medium font-centered">
            <div class="box__action no-margin">
              <a class="button button--main gutter-left-triple gutter-right-triple" href="#">Filtrar</a>
            </div>
          </div>
        </form>
      </div>
    </div>
	</section>

  <section id="three" class="horizon">
    <div class="container">
      <div class="row">
        <h1 class="horizon__title hat heels">Año 2016</h1>
      </div>
      <div class="row bg-main-lightest font-color-main-regular text-uppercase">
        <div class="gr-2 prefix-1">
          <h4 class="no-margin">Mes</h4>
        </div>
        <div class="gr-6">
          <h4 class="no-margin">Documento</h4>
        </div>
        <div class="gr-3">
          <h4 class="no-margin">Descargar</h4>
        </div>
      </div>
      <article class="box box--post">
        <div class="row">
          <div class="gr-2 prefix-1 flex flex-center">
            <div class="box">
              <h4 class="box__dates text-uppercase no-margin">Mayo</h4>
            </div>
          </div>
          <div class="gr-6 flex flex-center">
            <div class="box__body">
              <h4 class="box__title">Hechos esenciales</h4>
              <div class="box__excerpt">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
          </div>
          <div class="gr-3 flex flex-center">
            <div class="box__pdf box--overlay gutter-left-double">
              <strong class="app-brand__logo">Descargar PDF</strong>
              <span>12.5 MB</span>
            </div>
          </div>
        </div>
      </article>
      <div class="row box box--post no-gutter">
        <div class="gr-2 prefix-1 flex flex-center">
          <article class="box box--post">
						<div class="box__body">
							<h4 class="title no-margin">Mayo</h4>
						</div>
					</article>
        </div>
        <div class="gr-6 flex flex-center">
          <article class="box box--post">
						<div class="box__body">
							<h4 class="box__title">Hechos esenciales</h4>
							<div class="box__excerpt">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>
					</article>
        </div>
        <div class="gr-3 flex flex-center">
          <article class="box box--post">
						<div class="box__body">
							<h4 class="box__title no-margin">Descargar PDF</h4>
              <div class="box__excerpt">
								<p>12.5 mb</p>
							</div>
							<!-- <div class="box__action">
								<a href="#" class="link">Ver más</a>
							</div> -->
						</div>
					</article>
        </div>
      </div>
    </div>
  </section>

</main>
<?php get_footer(); ?>
