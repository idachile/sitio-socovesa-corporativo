<?php
get_header();
the_post();
?>
<main id="top">
	<section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--offset full-height no-gutter@tablet">
			<div class="row floater no-left@tablet">
				<div class="gr-12 no-gutter@tablet">
					<figure class="horizon__bg" style="background-image: url(http://unsplash.it/1200/800?gravity=west)"
						data-eq></figure>
				</div>
			</div>
			<div class="row relative" data-eq>
				<div class="gr-6 prefix-6 gr-8@book prefix-4@book gr-12@tablet prefix-0@tablet no-gutter">
					<article class="box box--float box--float--offset bg-white">
						<div class="box__body">
							<h2 class="box__title title-decorated">Una empresa de diseño centrado en el habitar</h2>
							<div class="box__excerpt">
								<p>Ser una empresa líder en la industria por más de 50 años exige tener objetivos claros. Para
									Empresas Socovesa mejorar la vida de las personas con productos inmobiliarios de calidad,
									enfocados en las necesidades del habitar de cada segmento, es una meta y una inspiración.</p>
								<p>Por eso, en la última década decidimos evolucionar para convertirnos en una compañía de
									diseño única, experta en crear proyectos que piensan en cómo las personas usan y valoran los
									espacios.</p>
								<p>Para nosotros el diseño va más allá del arte. Integra conocimientos y disciplinas como la
									Arquitectura, la Ingeniería y las Ciencias Sociales, y requiere de métodos de trabajo
									creativos, innovadores y colaborativos en cada etapa del proceso</p>
								<p>Diseñar centrados en las personas es la filosofía que nos permite mantener nuestro propósito:
									mejorar la calidad de vida de la gente, con espacios bien diseñados y de calidad</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--float " data-equalize="target" data-mq="tablet-down" data-eq-target=".box__body">
		<div class="container no-gutter@book">
			<div class="row">
				<div class="gr-6 gr-12@small">
					<article class="box box--people">
						<figure class="box__figure">
							<img src="http://unsplash.it/g/300/300/?gravity=east" alt="" class="cover-img" />
						</figure>
						<div class="box__body">
							<span class="box__meta">Vicepresidente</span>
							<h2 class="box__title">Justino Negrón Bornard</h2>
							<div class="box__excerpt">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor
									diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de
									planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
							</div>
						</div>
					</article>
				</div>
				<div class="gr-6 gr-12@small">
					<article class="box box--people">
						<figure class="box__figure">
							<img src="http://unsplash.it/g/300/300/?gravity=west" alt="" class="cover-img" />
						</figure>
						<div class="box__body">
							<span class="box__meta">Director</span>
							<h2 class="box__title">Fernando Barros Tocornal</h2>
							<div class="box__excerpt">
								<p>En función de lo anterior, se proponen y definen los ejes conceptuales que sostienen una
									propuesta de valor
									diferenciadora.
									Estos pilares son establecidos entre todas las áreas de Empresas Socovesas que participan en
									el proceso de
									planning,
									ya que sus características traspasarán todos los aspectos del proyecto.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-6 gr-12@tablet prefix-6 prefix-0@tablet no-gutter ">
					<div class="float__square bg-main" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row col-reverse@tablet">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure" data-rellax data-rellax-speed="1" data-rellax-percentage="0.1">
						<img src="http://unsplash.it/700/400?gravity=south" alt="" class="cover-img" />
					</figure>
				</div>
				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book flex-center">
					<article class="box box--simple text-light">
						<div class="box__body">
							<span class="box__counter">02</span>
							<h2 class="box__title title-decorated medium">Transformación digital de la construcción</h2>
							<div class="box__excerpt">
								<p>Un buen diseño debe concluir en una buena ejecución. Para aumentar la precisión y predictibilidad del proceso constructivo, en 2017 nace el Bim Lab de Empresas Socovesa. </p>
								<p>Esta unidad de investigación y desarrollo estudia, testea y modela proyectos inmobiliarios en 3D con la metodología Building Intelligence Modelling, conocida mundialmente por su capacidad de diseñar edificaciones mejor pensadas y anticiparse a los desafíos de la obra.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 no-gutter gr-12@tablet">
					<div class="float__square bg-light-grey" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height" data-rellax data-rellax-speed="1">
			<div class="row">
				<div class="gr-4 gr-5@book gr-12@tablet flex-center">
					<article class="box box--simple">
						<div class="box__body">
							<h2 class="box__title title-decorated-dark">Trabajo colaborativo</h2>
							<div class="box__excerpt">
								<p>En Empresas Socovesa estamos llamados a mirar los resultados globales más allá de los
									objetivos individuales y a integrar distintas visiones para alcanzar mejores resultados.</p>
							</div>
						</div>
					</article>
				</div>
				<div class="gr-7 prefix-1 prefix-0@book gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=center" alt="" class="cover-img" />
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 gr-12@tablet prefix-2 prefix-0@tablet no-gutter ">
					<div class="float__square bg-light-ice" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="row col-reverse@tablet">
				<div class="gr-7 gr-12@tablet flex-center">
					<figure class="box__figure">
						<img src="http://unsplash.it/700/400?gravity=south" alt="" class="cover-img" />
					</figure>
				</div>
				<div class="gr-4 gr-5@book gr-12@tablet prefix-1 prefix-0@book flex-center">
					<article class="box box--simple">
						<div class="box__body">
							<span class="box__counter">02</span>
							<h2 class="box__title title-decorated-dark medium">Poner al cliente en la conversación</h2>
							<div class="box__excerpt">
								<p>Nuestro negocio comienza y termina en el cliente. Y esto es más que un simple decir: como
									compañiahemos fortalecido la capacidad de poner a la persona y sus necesidades al centro de
									cada decisión organizacional.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
		data-horizon>
		<div class="container container--float" style="max-width:1200px">
			<div class="row">
				<div class="gr-8 prefix-4 gr-12@tablet prefix-0@tablet no-gutter">
					<figure class="horizon__bg" style="background-image: url(http://unsplash.it/1200/600?gravity=south)"
						data-eq></figure>
				</div>
			</div>
		</div>
		<div class="container full-height margin--top no-gutter@tablet">
			<div class="row relative" data-eq>
				<div class="gr-5 gr-7@book gr-12@tablet no-gutter@tablet flex-center">
					<article class="box box--float box--float--left bg-white">
						<div class="box__body">
							<h2 class="box__title title-decorated">COMUNICAR VALOR</h2>
							<div class="box__excerpt">
								<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde officiis esse ut nam possimus
									tenetur. Quibusdam iure iusto nobis qui quia quisquam, illo dolor, molestias magnam, animi
									tempora! Libero, eaque?.</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon--normal bg-light-ice">
		<div class="container">
			<div class="row">
				<div class="gr-6 gr-12@tablet">
					<article class="box box--square">
						<div class="box__body no-gutter">
							<h2 class="box__title title-regular">Análisis 3d</h2>
							<div class="box__excerpt">
								<p>Se entiende el proyecto en su contexto. Se evalúa la composición sociodemográfica y las
									dinámicas del entorno: cercanía al transporte público, comercio, servicios y todo lo que
									tenga relación con la calidad de vida de los usuarios.</p>
								<p>Se estudia el mercado en cuanto a superficie, mix, distribución, terminaciones, facilities,
									espacios comunes, fachada, precios y otras características relevantes.
									Por último, se lleva a cabo un acercamiento cualitativo y cuantitativo al consumidor
									potencial y final. El objetivo es conocer su estructura familiar, su etapa de vida, sus
									dinámicas, sus ideas y todos los anhelos que podamos interpretar al momento de diseñar. </p>
							</div>
						</div>
					</article>
				</div>
				<div class="gr-6 prefix-0@medium gr-12@tablet flex-middle flex-center">
					<figure class="gutter-double">
						<img src="<?php echo get_template_directory_uri(); ?>/dist/img/content/icono-diseno-3d.svg"
							alt="Imagen de " class="elastic-img">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section id="seventeen" class="horizon horizon--marcas no-gutter@tablet">
		<div class="container heels@tablet">
			<div class="row">
				<div class="gr-8 gr-10@medium gr-12@small gr-centered">
					<h2 class="horizon__title title-center">Te presentamos nuestro programa de apoyo educacional</h2>
					<div class="horizon__excerpt">
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque eius hic voluptates quas architecto
							qui? Cum adipisci impedit reiciendis, commodi eligendi dolor quaerat vel ea nulla saepe deleniti
							cupiditate molestias.</p>
					</div>
				</div>
			</div>
		</div>
		<section class="horizon horizon--marcas">
			<div class="container container--float-full no-gutter">
				<div class="row row--absolute">
					<div class="gr-10 no-gutter gr-12@medium">
						<div class="float__square bg-light-ice"></div>
					</div>
				</div>
			</div>
			<div class="container full-height">
				<div class="slider slider--diapo slider--blocks" data-module="slider" data-transition="false">
					<div class="slider__items" data-role="slider-list" data-equalize="target" data-mq="medium-down"
						data-eq-target=".row">
						<div class="slider__slide current" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">2018</h2>
											<div class="box__excerpt">
												<p>Empresas Socovesa demuestra una vez más su capacidad para adaptarse a los
													ciclos económicos del país y
													supera el nivel
													de promesas netas comprometidas en 2017, al mismo tiempo que conserva los
													equilibrios inmobiliarios dentro
													de los parámetros
													definidos.</p>
												<p>Este año la Compañía inicia un proceso de modernización de la compañía, a
													partir el cual se decide que,
													entre el 1 de enero
													de 2019 y los siguientes cinco años, la matriz y todas las filiales - con
													excepción de Socovesa Sur que
													seguirá operando en
													Temuco - instalarán sus oficinas en un mismo edificio corporativo en la comuna
													de providencia.</p>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=south">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">1979</h2>
											<div class="box__excerpt">
												<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque facilis
													delectus mollitia et, ipsam,
													nihil praesentium, temporibus saepe officiis asperiores aliquam? Iste, minus
													perspiciatis error maxime
													sunt at similique. Quis.</p>
												<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque facilis
													delectus mollitia et, ipsam,
													nihil praesentium, temporibus saepe officiis asperiores aliquam? Iste, minus
													perspiciatis error maxime
													sunt at similique. Quis.</p>
											</div>
											<div class="box__action hat-small">
												<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=center">
									</figure>
								</div>
							</div>
						</div>
						<div class="slider__slide" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title">1983</h2>
											<div class="box__excerpt">
												<p>Empresas Socovesa demuestra una vez más su capacidad para adaptarse a los
													ciclos económicos del país y
													supera el nivel
													de promesas netas comprometidas en 2017, al mismo tiempo que conserva los
													equilibrios inmobiliarios dentro
													de los parámetros
													definidos.</p>
												<p>Este año la Compañía inicia un proceso de modernización de la compañía, a
													partir el cual se decide que,
													entre el 1 de enero
													de 2019 y los siguientes cinco años, la matriz y todas las filiales - con
													excepción de Socovesa Sur que
													seguirá operando en
													Temuco - instalarán sus oficinas en un mismo edificio corporativo en la comuna
													de providencia.</p>
											</div>
											<div class="box__action hat-small">
												<a href="#" class="button button--main" title="Ir a ">Seguir leyendo</a>
											</div>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<figure>
										<img class="cover-img" src="https://unsplash.it/1200/600?gravity=north">
									</figure>
								</div>
							</div>
						</div>
					</div>
					<div class="container container--float dates-container gr-8 prefix-2 gr-12@book font-centered no-gutter">
						<div class="slider__dates bg-white">
							<a class="slider__date slider__date--prev" data-role="slider-arrow" data-direction="prev"></a>
							<a class="slider__date slider__bullet--current" data-role="slider-bullet" data-target="0">1965</a>
							<a class="slider__date" data-role="slider-bullet" data-target="1">1979</a>
							<a class="slider__date" data-role="slider-bullet" data-target="2">1983</a>
							<a class="slider__date slider__date--next" data-role="slider-arrow" data-direction="next"></a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

	<section id="eighteen" class="horizon horizon--marcas">
		<div class="container">
			<h2 class="horizon__title title-center">Ubicación de nuestros proyectos</h2>
			<div class="tab__wrapper hat-small" data-role="tab-parent">
				<div class="tab__controls" data-role="tab-controls">
					<button class="tab__deployer no-touch active" data-func="tabControl"
						data-target="tab-almagro"><span>Almagro</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-socovesa"><span>Socovesa</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-pilares"><span>Pilares</span></button>
					<button class="tab__deployer no-touch" data-func="tabControl"
						data-target="tab-desarrollo-comercial"><span>Desarrollo Comercial</span></button>
				</div>
				<div class="tab__contents" data-equalize="target" data-mq="small-down" data-eq-target=".row">
					<div class="tab__target active" data-tab-name="tab-almagro">

						<div class="row" data-role="ciudad-parent" data-color-active="#E75D5D" data-color-preview="#F19E9E">
							<div class="gr-3 gr-4@medium gr-12@small flex-center">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>

					</div>
					<div class="tab__target" data-tab-name="tab-socovesa">
						<div class="row" data-role="ciudad-parent" data-color-active="#98dff1" data-color-preview="#cceff9">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab__target" data-tab-name="tab-pilares">
						<div class="row" data-role="ciudad-parent" data-color-active="#ff9016" data-color-preview="#ffbc73">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab__target" data-tab-name="tab-desarrollo-comercial">
						<div class="row" data-role="ciudad-parent" data-color-active="#9695d3" data-color-preview="#c0bfe5">
							<div class="gr-3 gr-4@medium gr-12@small flex-center no-gutter@small">
								<div class="svg-marcas__selector show@small">
									<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span
											data-selector-show>Santiago</span><span class="hide" data-selector-message>Ciudad a
											mostrar</span></button>
									<div class="svg-selector__options" data-role="svg-selector-options">
										<button class="svg-selector__option" data-role="ciudad-control" data-target="iquique"
											data-status="preview">Iquique</button>
										<button class="svg-selector__option" data-role="ciudad-control" data-target="antofagasta"
											data-status="preview">Antofagasta</button>
										<button class="svg-selector__option active" data-role="ciudad-control"
											data-target="santiago" data-status="active">Santiago</button>
									</div>
								</div>
								<div class="svg-marcas__pais hide@small">
									<?php get_template_part('partials/maps/mapa', 'chile'); ?>
								</div>
							</div>
							<div class="gr-6 prefix-1 prefix-0@medium gr-12@small no-gutter@small">
								<div class="svg-marcas__ciudades">
									<?php get_template_part('partials/maps/ciudades'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>