<?php
/*
* Template Name: Página repositorio
* Template Post Type: en_page, page
*/
the_post();
$pid = get_the_ID();
global $post,$pid;
get_header();
?>
<main id="top">
	<?php
	$secciones = '';
	$secciones = get_field('secciones', $pid);
	global $data_section;
	if( isset($secciones) && !empty($secciones)):
		foreach( $secciones as $data_section ):
			if(!empty($data_section) && isset($data_section)):
				get_template_part('partials/repositorio/'. str_replace('_', '-', $data_section['acf_fc_layout']));
			endif;
		endforeach;
	endif;
	?>
</main>

<?php get_footer(); ?>