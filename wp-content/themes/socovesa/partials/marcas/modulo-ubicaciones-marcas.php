<?php global $data_section;
if(!empty($data_section['filiales'])):
?>
<section class="horizon horizon--marcas">
	<div class="container">
		<h2 class="horizon__title title-center">Ubicación de nuestros proyectos</h2>
		<div class="tab__wrapper hat-small" data-role="tab-parent">
			<div class="tab__controls" data-role="tab-controls">
			<?php
			$con_filial = 0;
			foreach($data_section['filiales'] as $filial):
				$con_filial == 0 ? $active_btn = 'active' : $active_btn = '';
				echo '<button class="tab__deployer no-touch '.$active_btn.'" data-func="tabControl" data-target="tab-'.clean_string($filial['titulo']).'"><span>'.$filial['titulo'].'</span></button>';
				$con_filial++;
			endforeach;
			?>
			</div>
			<div class="tab__contents" data-equalize="target" data-mq="small-down" data-eq-target=".row">
			<?php
			$con_filial = 0;
			global $filial;
			foreach($data_section['filiales'] as $filial):
				$selectores = $ciudades = '';
				$regiones = array();
				$con_filial == 0 ? $active_tab = 'active' : $active_tab = '';
			?>
				<div class="tab__target <?php echo $active_tab; ?>" data-tab-name="tab-<?php echo clean_string($filial['titulo']); ?>">
					<div class="row" data-role="ciudad-parent" data-color-active="<?php echo $filial['color_activo']; ?>" data-color-preview="<?php echo $filial['color_inactivo']; ?>">
					<?php
					if(!empty($filial['regiones'])):
						global $regiones;
						$regiones = $filial['regiones'];
						foreach($regiones as $region):
							$active = 'preview';
							if($region['activa']):
								$active = 'active';
								$active_region_name = $region['titulo'];
							endif;

							$selectores .= '<button class="svg-selector__option '. $active .'" data-role="ciudad-control" data-target="'.$region['region'].'" data-status="'.$active.'">'.$region['titulo'].'</button>';
							$ciudades .= '<section class="map__city '. $active .'" data-role="ciudad-detalle" data-ciudad="'.$region['region'].'">'.wp_get_attachment_image($region['imagen'], 'full', false, array('class' => 'elastic-img')).'</section>';
						endforeach;
					?>
						<div class="gr-5 gr-4@medium gr-12@tablet flex-center">
							<div class="svg-marcas__selector show@tablet">
								<button class="svg-selector__deployer" data-role="svg-selector-deployer"><span data-selector-show><?php echo $active_region_name; ?></span><span class="hide" data-selector-message>Región a mostrar</span></button>
								<div class="svg-selector__options" data-role="svg-selector-options">
								<?php echo $selectores; ?>
								</div>
							</div>						
							<div class="svg-marcas__pais hide@tablet">
							<?php get_template_part('partials/maps/mapa-chile'); ?>
							</div>
						</div>
						<div class="gr-6 prefix-1 gr-12@tablet no-gutter@tablet">
							<div class="svg-marcas__ciudades"><?php echo $ciudades; ?></div>
						</div>
					<?php endif; ?>
					</div>
				</div>
			<?php $con_filial++; endforeach; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>