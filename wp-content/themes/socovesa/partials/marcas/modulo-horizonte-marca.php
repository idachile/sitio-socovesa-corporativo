<?php
global $data_section;
$alineacion = $data_section['alineacion'];
?>
<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
	<div class="container container--float-full no-gutter" data-eq>
		<div class="row">
			<div class="gr-7 gr-6@medium gr-12@tablet <?php echo $alineacion == 'left' ? 'prefix-5 prefix-6@medium prefix-0@tablet' : ''; ?> no-gutter">
				<div class="float__square <?php echo $data_section['color']; ?>" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container full-height no-gutter@tablet">
		<?php if(!empty($data_section['imagen'])): ?>
		<div class="horizon__logo flex-center@small flex-middle@small <?php echo $alineacion == 'left' ? 'flex-right' : ''; ?>">
			<img src="<?php echo $data_section['imagen'] ?>" class="logo-img" alt="Imagen de marca <?php echo $data_section['titulo']; ?>">
		</div>
		<?php endif; ?>
		<div class="row <?php echo $alineacion == 'left' ? 'col@small' : 'row-reverse'; ?> " data-equalize="target" data-mq="small-down" data-eq-target="[data-equalize]">
			<div class="gr-6 gr-7@large gr-8@medium gr-12@small no-gutter@medium">
				<article class="box box--area bg-white" data-equalize>
					<div class="box__body">
						<h2 class="box__title title-decorated-dark"><?php echo $data_section['titulo']; ?></h2>
						<?php if(!empty($data_section['bajada'])) echo '<div class="box__excerpt">'.apply_filters('the_content', $data_section['bajada']).'</div>' ?>
					</div>
				</article>
			</div>
			<div class="gr-5 gr-4@large gr-12@small <?php echo $alineacion == 'left' ? 'prefix-1 prefix-0@medium' : 'suffix-1 suffix-0@medium'; ?> no-gutter@medium">
				<?php if(!empty($data_section['video'])): ?>
				<div class="box box--media">
					<video data-equalize data-role="video-player" src="<?php echo $data_section['video']; ?>"
						type="video/mp4"></video>
						<?php if(!empty($data_section['video_prev'])): ?>
						<figure class="video-prev" data-role="video-prev" style="background-image: url('<?php echo $data_section['video_prev'] ?>')"><div class="video-prev__veil"></div></figure>
					<?php endif; ?>
					<div class="video__control">
						<button class="video__control__deployer" data-role="video-player-deployer"></button>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="slider slider--single hat" data-module="slider" data-transition="false">
			<div class="slider__items" data-role="slider-list" data-equal-media>
				<?php $i=1; $images = $data_section['slider']; ?>
				<?php foreach($images as $image): ?>
				<div class="slider__slide <?php if($i==1): ?>current<?php endif; ?>" data-role="slider-slide">
					<figure class="slider__figure">
						<img src="<?php echo $image['sizes']['big_1024x576'] ?>" alt="<?php echo $image['alt']; ?>" class="cover-img" />
					</figure>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="slider__arrows">
				<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>
				<button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>
			</div>
			<div class="slider__bullets">
				<?php
            $i=0;
            foreach($images as $slide): ?>
				<button class="slider__bullet <?php if($i==0): ?>slider__bullet--current<?php endif; ?>" data-role="slider-bullet" data-target="<?php echo $i; ?>"></button>
				<?php $i++; endforeach; ?>
			</div>
		</div>
		<?php if(!empty($data_section['enlace']['url'])): ?>
		<div class="horizon__action flex-center hat">
			<a href="<?php echo ensure_url($data_section['enlace']['url']); ?>" class="button button--ghost-dark button--full" title="<?php echo $data_section['enlace']['title']; ?>"
			<?php echo !empty($data_section['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $data_section['enlace']['title']; ?></a>
		</div>
		<?php endif; ?>
	</div>
</section>
