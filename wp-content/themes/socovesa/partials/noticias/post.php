<?php global $post;?>

<article class="box box--news">
	<div class="box__body">
		<?php
			$categoryName = wp_get_post_terms($post->ID, 'category');
			if(!empty($categoryName)) echo '<p class="box__meta">'.$categoryName[0]->name.'</p>';
		?>
		<h3 class="box__title"><a href="<?php echo the_permalink(); ?>"><?php the_title() ?></a></h3>
		<?php $bajada = has_excerpt($post) ? cut_string_to(get_the_excerpt($post), 200) : cut_string_to($post->post_content, 200); ?>
		<?php if(!empty($bajada)): ?>
		<div class="box__excerpt">
			<?php echo apply_filters('the_content', $bajada); ?>
		</div>
		<?php endif; ?>
		<div class="box__action">
			<div class="box__date">
				<span class="box__date__day"><?php echo get_the_date('d',$post); ?> de
					<?php echo get_the_date('F',$post); ?></span>
			</div>
			<a href="<?php the_permalink(); ?>" class="link link--view" title="Ir a <?php the_title(); ?>">Seguir leyendo</a>
		</div>
	</div>
</article>