<?php global $data_section; ?>
<section id="three" class="horizon horizon--normal">
	<div class="container">
		<div class="row minibanner">
			<div class="gr-6 gr-12@tablet minibanner__info">
				<div class="minibanner__info__col">
					<?php if(isset($data_section['titulo']) && !empty($data_section['titulo'])): ?>
					<h2 class="minibanner__title"><?php  echo $data_section['titulo']; ?></h2>
					<?php endif; ?>
					<?php if(isset($data_section['tagline']) && !empty($data_section['tagline'])): ?>
					<p><?php  echo $data_section['tagline']; ?></p>
					<?php endif; ?>
				</div>
				<div class="minibanner__info__col">
					<?php if(isset($data_section['dato']) && !empty($data_section['dato'])): ?>
					<p><?php  echo $data_section['dato']; ?></p>
					<?php endif; ?>
					<?php if(isset($data_section['valor']) && !empty($data_section['valor'])): ?>
					<p class="minibanner__title"><?php  echo $data_section['valor']; ?></p>
					<?php endif; ?>
				</div>
			</div>
			<div class="gr-6 gr-12@tablet minibanner__mensaje">
				<?php if(isset($data_section['bajada']) && !empty($data_section['bajada'])): ?>
				<?php  echo $data_section['bajada']; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>