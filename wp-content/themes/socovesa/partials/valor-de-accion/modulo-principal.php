<?php global $data_section,$pid; ?>
<section class="main__top">
	<figure class="main__top__figure">
		<div class="main__top__bg"
			style="background-image: url('<?php echo isset($data_section['imagen']) ? $data_section['imagen'] : get_the_post_thumbnail_url($pid, 'full'); ?>')">
		</div>
	</figure>
	<div class="main__top__body">
		<div class="container">
			<div class="main__top__content">
				<h1 class="main__title">
					<?php echo isset($data_section['titulo']) ? $data_section['titulo'] : get_the_title($pid); ?></h1>
				<?php if(isset($data_section['bajada']) && !empty($data_section['bajada'])): ?>
				<div class="main__excerpt">
					<?php  echo $data_section['bajada']; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>