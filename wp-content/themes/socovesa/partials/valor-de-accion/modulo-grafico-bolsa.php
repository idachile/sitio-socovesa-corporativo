<?php global $data_section;
?>
<section class="horizon bg-light-grey">
	<div class="container">
		<div class="row">
			<div class="gr-10 gr-12@medium gr-centered">
				<?php if(!empty($data_section['titulo'])) echo '<h2 class="horizon__title title-center">'.$data_section['titulo'].'</h2>'; ?>
				<?php if(!empty($data_section['bajada'])): ?>
					<div class="row hat horizon__excerpt" data-role="divide-text" data-col-class="gutter-double gr-6 gr-12@tablet"><?php echo $data_section['bajada'] ?></div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="gr-10 gr-centered gr-12@tablet">
				<figure class="box box--graph no-gutter-horizontal box--graph--tall">
					<div id="ipsa-chart" data-role="spline-area-chart" data-bg="#ebebeb" data-color="#333333" data-points="<?php echo $data_section['datos_json'] ?>" data-title="<?php echo $data_section['titulo_grafico']; ?>" data-axisy-izq="<?php echo $data_section['titulo_axis_y_izq']; ?>" data-axisy-der="<?php echo $data_section['titulo_axis_y_der']; ?>" data-intervalo-x="<?php echo $data_section['intervalo_x']; ?>" data-intervalo-y="<?php echo $data_section['intervalo_y']; ?>" data-intervalo-y2="<?php echo $data_section['intervalo_y2']; ?>"></div>
				</figure>
			</div>
		</div>
	</div>
</section>
<?php
needs_script('canvasjs');
needs_script('chart_simpleline');
needs_script('chart_spline_area');
?>