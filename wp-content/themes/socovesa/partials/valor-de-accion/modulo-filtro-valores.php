<?php global $data_section;

$current_month = $current_year = '';
$meses = array();

$current_year = date('Y');
$current_month = date('m');
$meses = get_months_alt();
?>
<section class="horizon horizon--normal">
	<div class="container">
		<div class="row bg-light-grey">
			<form action="/wp-json/obtener/valores-bolsa" method="post" class="flex-wrap no-border" data-role="filtro-bolsa-valores">
				<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
					<div class="form-control__container" required>
						<select id="filtro-year" name="year" class="form-control__field--select form-control__field form-control__select w-semibold" data-role="filtro-bolsa-year"
						data-current="<?php if(isset($current_year) && !empty($current_year)) echo $current_year; ?>">
							<option value="" selected>Año</option>
							<?php 
							for($x = 0; $x <= 10; $x++):
								$year = $current_year - $x;
								echo '<option value="'.$year.'">'.$year.'</option>';
							endfor;
							?>
						</select>
					</div>
				</div>
				<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
					<div class="form-control__container">
						<select id="filtro-month" name="month" class="form-control__field--select form-control__field form-control__select w-semibold" data-role="filtro-bolsa-month"
						data-current="<?php if(isset($current_month) && !empty($current_month)) echo $current_month; ?>" required>
							<option value="" selected>Mes</option>
							<?php
							if(isset($meses) && !empty($meses)):
								foreach ($meses as $mes) {
									echo '<option value="'.$mes['num'].'">'.$mes['name'].'</option>';
								}
							endif;
							?>
						</select>
					</div>
				</div>
				<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
					<div class="form-control__container">
						<select id="filtro-day" name="day" class="form-control__field--select form-control__field form-control__select w-semibold loading" data-role="filtro-bolsa-day" disabled required>
							<option value="" selected>Día</option>
						</select>
					</div>
				</div>
				<div class="gr-3 gr-12@medium gutter-double gutter@medium font-centered">
					<div class="box__action no-margin">
						<button class="button button--main button--full-width waiting" data-role="submit-proyecto">Filtrar</button>
					</div>
				</div>
			</form>
		</div>

		<div class="row minibanners hide" data-role="valores-bolsa-container">
			<div class="gr-4 gr-12@tablet minibanner__info">
				<div class="minibanner__info__col">
					<p>Opening price</p>
					<p class="minibanner__title" data-role="opening-price"></p>
				</div>
			</div>
			<div class="gr-4 gr-12@tablet minibanner__info">
				<div class="minibanner__info__col">
					<p>Closing price</p>
					<p class="minibanner__title" data-role="closing-price"></p>
				</div>
			</div>
			<div class="gr-4 gr-12@tablet minibanner__info">
				<div class="minibanner__info__col">
					<p>Monto</p>
					<p class="minibanner__title" data-role="total"></p>
				</div>
			</div>
		</div>
		<div class="row minibanners hide" data-role="error-container">
			<div class="gr-12 minibanner__info">
				<div class="minibanner__info__col">
					<p>No se encontraron registros para la fecha indicada</p>
				</div>
			</div>
		</div>
	</div>
</section>