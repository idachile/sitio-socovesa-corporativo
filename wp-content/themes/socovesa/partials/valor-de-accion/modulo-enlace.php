<?php global $data_section; ?>

<?php if(isset($data_section['enlace']) && !empty($data_section['enlace'])): ?>
<section class="horizon">
	<div class="container">
		<div class="flex-center"><a href="<?php echo $data_section['enlace']['url']; ?>" class="button button--ghost-dark" title="<?php echo $data_section['enlace']['title']; ?>"<?php echo 'target="' . $data_section['enlace']['target'] .'"'; ?> ><?php echo $data_section['enlace']['title']; ?></a></div>
	</div>
</section>
<?php endif; ?>