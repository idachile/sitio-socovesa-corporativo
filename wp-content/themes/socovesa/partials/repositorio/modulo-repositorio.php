<?php global $pid;?>
<section class="horizon horizon--normal">
	<div class="container">
		<div class="row bg-light-grey">
			<form action="#" method="get" class="flex-wrap no-border">
				<div
					class="gr-6 prefix-1 gr-7@large prefix-0@large gr-12@medium gutter-double gutter@medium flex flex-center">
					<input type="text" id="term" name="term" class="form-control__field" placeholder="Buscar...">
				</div>
				<div class="gr-4 gr-12@medium suffix-1 gr-5@large suffix-0@large gutter-double gutter@medium font-centered">
					<div class="box__action no-margin">
						<button class="button button--main button--full-width">Filtrar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<?php
$documentos = get_documentos_completos_repositorio($pid);
?>

<section class="horizon horizon--normal">
	<div class="container">
		<div class="row">
			<div class="gr-3 suffix-1 gr-4@large suffix-0@large gr-12@tablet">
				<section class="repo-sidebar" data-role="sticky-role" data-offset-parent="60" data-stop="#footer">
					<button class="repo-sidebar__filtrar show@tablet" data-func="toggleTarget" data-target=".repo-sidebar__menu">Filtrar <span class="icon"></span></button>
					<ul class="repo-sidebar__menu" data-role="scroll-navigation-side">
						<?php
				$contador_pagina = 0;
				$horizontes = array();
				foreach($documentos as $pagina):  $child_class = ''; if(!empty($pagina['horizontes'])) $child_class = 'has-children'; ?>
						<li class="repo-sidebar__menu__item <?php echo $child_class; ?>" data-child>
							<a href="#<?php echo sanitize_slug($pagina['titulo']); ?>" data-role="collapseHorizonte" title="Mostrar" data-id="<?php echo $pagina['id']; ?>" data-offset="20"><?php echo $pagina['titulo']; ?></a>
							<?php if(!empty($pagina['horizontes'])): $contador_horizonte = 0; if($contador_pagina == 0) $horizontes = $pagina['horizontes']; ?>
							<ul class="repo-sidebar__submenu" data-role="scroll-navigation-child">
								<?php foreach($pagina['horizontes'] as $horizonte): ?>
								<li class="repo-sidebar__submenu__item" data-child>
									<a href="#<?php echo $contador_pagina.'__'.$contador_horizonte; ?>" data-func="scrollToTarget"
										data-id="<?php echo $contador_pagina.'__'.$contador_horizonte; ?>"
										title="Filtrar" data-offset="60"><?php echo $horizonte['titulo']; ?></a>
								</li>
								<?php $contador_horizonte++; endforeach; ?>
							</ul>
							<?php endif; ?>
						</li>
						<?php $contador_pagina++; endforeach; ?>
					</ul>
				</section>
			</div>
			<div class="gr-8 gr-12@tablet hat@tablet">
				<?php if(!empty($documentos)): $contador_pagina = 0; ?>
					<?php foreach($documentos as $pagina): ?>
					<section id="<?php echo sanitize_slug($pagina['titulo']); ?>" class="repo-pagina" data-role="pagina-documento" data-pagina-id="<?php echo $pagina['id']; ?>">
						<h2 class="horizon__title heels-small"><?php echo $pagina['titulo'] ?></h2>
						<?php if(!empty($pagina['horizontes'])): global $documento; $contador_horizonte = 0; ?>
							<?php foreach($pagina['horizontes'] as $horizon): $contador_documento = 0; ?>
							<section id="<?php echo $contador_pagina.'__'.$contador_horizonte; ?>" class="repo-horizonte <?php echo $contador_horizonte == 0 ? 'active ' : ''; ?>"
							data-role="horizonte-documento" data-horizonte-id="<?php echo $contador_pagina.'__'.$contador_horizonte; ?>">
								<h2 class="horizon__subtitle"><?php echo $horizon['titulo'] ?></h2>
								<div class="row" data-role="divide-news" data-col-class="gr-6 gr-12@medium no-gutter">
									<?php foreach($horizon['documentos'] as $documento): ?>
									<div class="gr-12 gr-12@tablet" data-role="news-child" data-num="<?php echo $contador_documento; ?>">
										<?php get_template_part('partials/repositorio/documento') ?></div>
									<?php $contador_documento++; endforeach; ?>
								</div>
							</section>
							<?php $contador_horizonte++; endforeach; ?>
						<?php endif; ?>
					</section>
					<?php $contador_pagina++; endforeach; ?>
				<?php endif; ?>

				<?php //printme($horizontes) ?>
			</div>
		</div>
	</div>
</section>
