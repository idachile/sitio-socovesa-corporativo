<?php global $subsection, $pid; ?>
<article class="box box--side">
	<div class="form-parent" data-parent>
		<form action="/wp-json/procesar/suscripcion/" class="form form--newsletter" method="POST" data-module="common-form" data-validation="generic_ajax" autocomplete="off">
			
			<?php if(isset($subsection['titulo']) && !empty($subsection['titulo'])): ?>
			<h3 class="box__title"><?php echo $subsection['titulo']; ?></h3>
			<?php endif; ?>

			<?php if(isset($subsection['bajada']) && !empty($subsection['bajada'])) echo '<div class="box__excerpt">'. apply_filters('the_content', $subsection['bajada']) .'</div>'; ?>
			<input type="hidden" name="pid" value="<?php echo $pid; ?>">
			<input type="hidden" name="st_verify" value="">
			<div class="form-control hat-small">
				<input class="form-control__field" type="text" id="nombre" name="nombre" placeholder="Ingrese su nombre" required>
			</div>
			<div class="form-control">
				<input class="form-control__field" type="email" id="email" name="email" placeholder="Ingrese su e-mail" required>
			</div>
			<div class="form-control">
				<?php wp_nonce_field('enviar_newsletter', 'st_nonce'); ?>
				<button class="button button--main button--full-width" type="submit" title="Enviar formulario">
					<?php echo isset($subsection['boton']) && !empty($subsection['boton']) ? $subsection['boton'] : 'Enviar'; ?>
				</button>
			</div>
		</form>
	</div>
</article>