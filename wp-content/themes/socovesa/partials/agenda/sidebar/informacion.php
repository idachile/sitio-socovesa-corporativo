<?php global $subsection; ?>

<article class="box box--side">
	<div class="box__body">
		<h3 class="box__title"><?php echo $subsection['titulo']; ?></h3>
		<?php if(!empty($subsection['bajada'])) echo '<div class="box__excerpt">'. apply_filters('the_content', $subsection['bajada']) .'</div>'; ?>
		<?php if(!empty($subsection['enlaces'])): ?>
		<ul class="box__enlaces">
			<?php foreach($subsection['enlaces'] as $link) echo '<li class="box__enlace"><a href="'.$link['enlace']['url'].'">'.$link['enlace']['title'].'</a></li>'; ?>
		</ul>
		<?php endif; ?>
	</div>
</article>