<?php global $fecha_sel;
$add_data = '';
if(isset($fecha_sel) && !empty($fecha_sel)):
	$add_data = 'data-filter="true" data-selected="'.$fecha_sel.'"';
endif;

$eventos_calendario = '';
$eventos_calendario = generate_events_calendar('data-role="datepicker-calendar" data-today="'.date('Ymd').'" data-active="true" ' . $add_data);
?>
<div class="box hide@tablet" data-area-name="desktop-calendar">
	<article class="box no-gutter" data-mutable="tablet-down" data-desktop-area="desktop-calendar" data-mobile-area="mobile-calendar">
	<?php if(isset($eventos_calendario) && !empty($eventos_calendario)) echo $eventos_calendario; ?>
	</article>
</div>