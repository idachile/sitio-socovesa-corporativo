<?php
global $pid, $paged, $data_section, $fecha_sel;


if(!empty($fecha_sel)):
	$strtime = strtotime(strval(str_replace('/', '-', $fecha_sel)));
	$search_time = date('Ymd',$strtime);
	$compare = '=';
else:
	$search_time = date('Ymd');
	$compare = '>=';
endif;

$meta_query = array(
	array(
		'key' => 'fecha',
		'value' => $search_time,
		'type' => 'DATE',
		'compare' => $compare
	)
);

$args = array(
	'post_type' => 'evento',
	'post_status' => 'publish',
	'posts_per_page' => 1,
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'meta_key' => 'fecha',
	'meta_query' => $meta_query
);

$query_future = new WP_Query($args);
?>
<form class="hide" action="<?php the_permalink() ?>#resultado" method="GET" id="calendar-events"></form>

<section class="horizon horizon--content" id="resultado" data-module="sidebar-agenda">
	<div class="container">
		<div class="row col-reverse@tablet">
			<div class="gr-4 gr-12@tablet hat@tablet">
				<?php
				if(!empty($data_section['secciones_sidebar'])):
					foreach($data_section['secciones_sidebar'] as $side_section):
						global $subsection;
						$subsection = $side_section;
						get_template_part('partials/agenda/sidebar/' . $side_section['acf_fc_layout']);
					endforeach;
				endif; ?>
			</div>
			<div class="gr-8 gutter-left-double gutter-left@tablet gr-12@tablet">
			<?php
			if ( $query_future->have_posts() && $paged <= 1 ) : $count = 0; ?>
				<div class="heels show@tablet" data-area-name="mobile-calendar"></div>
				<section class="single__content">
					<h2 class="single__subtitle"><?php echo empty($fecha_sel) ? 'Próximo evento' : 'Evento seleccionado'; ?></h2>
					<?php 
						while ( $query_future->have_posts() ) : $query_future->the_post();
							echo get_calendar_box($post, true);
						$count++; endwhile;
					?>
				</section>
				<?php if(!empty($fecha_sel)) echo '<div class="single__action hat flex-center"><a href="'.get_permalink($pid).'" class="button button--ghost-main">Ver todos los eventos</a></div>'; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>

			<?php 
			$args['meta_query'] = array( array( 'key' => 'fecha', 'value' => date('Ymd'), 'type' => 'DATE', 'compare' => '<=' ) );
			$args['posts_per_page'] = 5;
			$args['paged'] = $paged;
			$args['order'] = 'DESC';
			
			$query_past = new WP_Query($args);
			?>
			<?php if ( $query_past->have_posts() && empty($fecha_sel) ) : $count = 0; ?>
				<section class="single__content hat">
					<h2 class="single__subtitle">Eventos pasados</h2>
					<?php 
						while ( $query_past->have_posts() ) : $query_past->the_post();
							echo get_calendar_box($post);
						$count++; endwhile;
					?>
					<?php echo get_pagination($query_past); ?>
				</section>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>
</section>