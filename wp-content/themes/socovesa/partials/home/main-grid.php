<?php global $data_section; if(!empty($data_section['bloques'])): ?>
<section class="horizon horizon--main current" data-horizon data-offset="0">
	<div class="container container--main no-gutter">
		<div class="grid grid-cols-5 grid-cols-4@book grid-cols-2@tablet grid-cols-1@phablet" id="main-grid">
		<?php $i=1; foreach($data_section['bloques'] as $bloques ): ?>
			<?php
			$bg = $data_expand = $data_expand_tablet = '';
			if( !empty($bloques['data_expand']) && $bloques['box_grid'] == 'box--expand'):
				$data_expand = ' data-expand="'.$bloques['data_expand'].'" ';
				$bg = 'data-bg="background-image: url(\''.wp_get_attachment_image_url($bloques['imagen'], 'medium_560x590', false).'\')"';
			endif;
			if(!empty($bloques['data_expand_tablet']) && $bloques['box_grid'] == 'box--expand'):
				$data_expand_tablet = ' data-expand-tablet="'.$bloques['data_expand_tablet'].'" ';
				$bg = 'data-bg="background-image: url(\''.wp_get_attachment_image_url($bloques['imagen'], 'medium_560x590', false).'\')"';
			endif;
			if(!$bg && !empty($bloques['imagen'])):
				$bg = 'style="background-image:url(\''.wp_get_attachment_image_url($bloques['imagen'], 'medium_560x590', false).'\')" data-srcset="background-image:url(\''.wp_get_attachment_image_url($bloques['imagen'], 'avatar_3x4', false).'\') 768w""';
			endif;
			?>
			<div class="<?php echo $bloques['grid']; ?> <?php echo $bloques['box_grid'] !== 'box--block' ? '' : 'grid-blocked'; ?>" data-grid-num="<?php echo $i; ?>">
				<article class="box box--grid <?php echo $bloques['box_grid'] !== 'box--block' ? $bloques['box_grid'] : 'blocked'; ?>" <?php echo $bg; ?>
					<?php echo $data_expand; ?><?php echo $data_expand_tablet; ?>>
					<div class="box__body">
						<h3 class="box__title"><?php echo $bloques["titulo"] ?></h3>
						<div class="box__excerpt"><?php echo $bloques["bajada"]; ?></div>
						<?php if(!empty($bloques['enlace'])): ?>
						<div class="box__action">
							<a href="<?php echo $bloques['enlace']['url']; ?>" class="button button--main" title="<?php echo $bloques['enlace']['title']; ?>" <?php echo !empty($bloques['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloques['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
		<?php
			unset($data_expand);
			unset($data_expand_tablet);
			unset($bg);
			$i++;

		endforeach;?>
		</div>
	</div>
</section>
<?php endif;?>