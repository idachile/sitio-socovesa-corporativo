<?php global $data_section; ?>
<?php $i=1; foreach($data_section["bloques"] as $bloque ): ?>
<section class="horizon horizon--float bg-main bg-tall wait open" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-offset="300" data-name data-horizon="featured-call">
	<div class="container container--float no-gutter">
		<div class="row">
			<div class="gr-7 gr-12@tablet no-gutter-horizontal">
				<div class="float__square bg-main" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="gr-5 gr-6@medium gr-12@tablet">
				<article class="box box--square bg-main" data-eq>
					<div class="box__body gr-12@book no-gutter">
						<h2 class="box__title"><?php echo $bloque["titulo"] ?></h2>
						<div class="box__excerpt">
							<?php echo $bloque["bajada"] ?>
						</div>
						<?php if(!empty($bloque['enlace'])): ?>
						<div class="box__action">
							<a href="<?php echo $bloque['enlace']['url']; ?>" class="button button--ghost-white" title="Seguir leyendo" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<div class="gr-6 prefix-1 gr-6@large gr-6@medium prefix-0@medium gr-12@tablet" data-rellax
				data-rellax-speed="1" data-rellax-percentage="0.1">
				<figure class="float__image hat middle full" data-reveal="fade-up">
					<?php echo wp_get_attachment_image($bloque['imagen'], 'medium_640x360', false, array('class' => 'cover-img', 'data-srcset' => wp_get_attachment_image_url($bloque['imagen'], 'avatar_360x450', false) .' 768w' )) ?>
				</figure>
			</div>
		</div>
	</div>
</section>
<?php endforeach; ?>
