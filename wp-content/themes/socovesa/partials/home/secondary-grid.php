<?php global $data_section; ?>
<section class="horizon horizon--simple bg-tall wait" data-offset="300" data-horizon data-name="secondary-grid">
	<div class="container heels-small">
		<div class="row">
			<div class="gr-8 gr-10@book gr-12@tablet gr-centered">
				<h2 class="horizon__title title-center"><?php echo $data_section["titulo"]; ?></h2>
				<div class="horizon__excerpt text-center">
					<?php echo $data_section["bajada"]; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container main no-gutter">
		<div class="grid grid-cols-5 grid-cols-4@medium grid-cols-2@tablet grid-cols-1@small" data-reveal="fade-up">
			<?php $i=1; foreach($data_section["bloques"] as $bloques ): ?>
			<?php if($bloques['data_expand'] !== '' || !empty($bloques['data_expand'])): ?>
			<div class="grid-item" data-grid-num="<?php echo $i; ?>">
				<article class="box box--grid box--expand blocked" data-expand="<?php echo $bloques['data_expand']; ?>"
					<?php if($bloques['data_expand_tablet']): ?>data-expand-tablet="<?php echo $bloques['data_expand_tablet'] ?>"
					<?php endif; ?>
					<?php if($bloques['data_expand_medium']): ?>data-expand-medium="<?php echo $bloques['data_expand_medium'] ?>"
					<?php endif; ?> <?php if($bloques['imagen']): ?>
					data-bg="background-image:url('<?php echo wp_get_attachment_image_url($bloques['imagen'], 'square_360', false); ?>')" data-bgset="background-image:url('<?php echo wp_get_attachment_image_url($bloques['imagen'], 'square_300', false); ?>') 768w" <?php endif; ?>>
					<div class="box__head__veil hover <?php if($bloques['imagen']) echo 'darker' ?>"></div>
					<?php if($bloques['imagen']==""): ?>
					<div class="box__head <?php echo $bloques['box_color']; ?>">
						<h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
					</div>
					<?php endif; ?>
					<div class="box__body <?php echo $bloques['box_color']; ?>">
						<h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
						<?php if($bloques['bajada']): ?>
						<div class="box__excerpt">
							<?php echo $bloques['bajada']; ?>
						</div>
						<?php endif; ?>
						<?php if(!empty($bloques['enlace'])): ?>
						<div class="box__action">
							<a href="<?php echo $bloques['enlace']['url']; ?>" class="button button--ghost-white"
								title="<?php echo $bloques['enlace']['title']; ?>" <?php echo !empty($bloques['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloques['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<?php else: ?>
			<div class="grid-item" data-grid-num="<?php echo $i; ?>">
				<article class="box box--grid blocked"
					<?php if($bloques['imagen']) echo 'style="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_360', false) .')" data-srcset="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_300', false) .') 768w"'; ?>>
					<?php if(empty($bloques['imagen'])): ?>
					<div class="box__head <?php echo $bloques['box_color']; ?>">
						<h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
					</div>
					<?php endif; ?>
				</article>
			</div>
			<?php endif; ?>
			<?php $i++; endforeach; ?>
		</div>
	</div>
</section>