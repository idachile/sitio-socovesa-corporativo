<?php global $data_section; ?>
<section class="horizon horizon--float bg-tall wait" data-offset="300" data-horizon data-name="final-grid">
   <div class="container">
      <div class="row heels-small">
         <div class="gr-6 gr-8@large gr-10@book gr-12@tablet">
            <h2 class="horizon__title"><?php echo $data_section['titulo']; ?></h2>
            <div class="horizon__excerpt">
               <?php echo $data_section['bajada']; ?>
            </div>
         </div>
      </div>
		<?php if(!empty($data_section['bloques'])): $i=1; ?>
      <div class="row">
         <div class="gr-10 gr-centered gr-12@medium no-gutter@tablet">
            <section class="grid grid-cols-4 grid-cols-2@tablet grid-cols-1@phablet" data-reveal="fade-up">
				<?php foreach($data_section['bloques'] as $bloques ): ?>
					
					<?php if($bloques['data_expand'] !== '' && !empty($bloques['data_expand'])): ?>
					
					<div class="grid-item" data-grid-num="<?php echo $i; ?>">
                  <article <?php echo $bloques['data_expand'] !== '' ? 'class="box box--grid box--expand" data-expand="'. $bloques['data_expand'] . '"' : 'class="box box--grid"' ?>
                     <?php if($bloques['data_expand_tablet']) echo 'data-expand-tablet="'. $bloques['data_expand_tablet'] . '"'; ?>
                     <?php if($bloques['data_expand_medium']) echo 'data-expand-medium="'. $bloques['data_expand_medium'] . '"'; ?>
							<?php if($bloques['imagen']) echo 'data-bg="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_360', false) .')" data-bgset="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_300', false) .') 768w"'; ?>>
							<div class="box__head__veil hover <?php if($bloques['imagen']) echo 'darker' ?>"></div>
							<?php if($bloques['imagen'] == ''): ?>
                     <div class="box__head <?php echo $bloques['box_color']; ?>">
                        <h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
                     </div>
							<?php endif; ?>
							
                     <div class="box__body <?php echo $bloques['box_color']; ?>">
								<h3 class="box__title"> <?php echo $bloques['titulo']; ?></h3>
								<?php if($bloques['bajada']): ?>
                        <div class="box__excerpt"><?php echo $bloques['bajada']; ?></div>
								<?php endif; ?>
								<?php if(!empty($bloques['enlace'])): ?>
								<div class="box__action">
									<a href="<?php echo $bloques['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $bloques['enlace']['title']; ?>" <?php echo !empty($bloques['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloques['enlace']['title']; ?></a>
								</div>
								<?php endif; ?>	
							</div>
						</article>
						
					</div>
					
					<?php else: ?>
					
					<div class="grid-item <?php if(empty($bloques['imagen'])) echo 'grid-blocked' ?>" data-grid-num="<?php echo $i; ?>">
						<article class="box box--grid blocked" <?php if($bloques['imagen']) echo 'style="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_360', false) .')" data-srcset="background-image:url('. wp_get_attachment_image_url($bloques['imagen'], 'square_300', false) .') 768w"'; ?>>
						<?php if(empty($bloques['imagen'])): ?>
                     <div class="box__head <?php echo $bloques['box_color']; ?>">
                        <h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
							</div>
						<?php endif; ?>
                  </article>
					</div>
					
					<?php endif; ?>
					
				<?php $i++; endforeach; ?>
				</section>
         </div>
      </div>
		<?php endif;?>
   </div>
</section>