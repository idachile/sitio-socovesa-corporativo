<?php global $data_section; ?>
<section class="horizon" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-horizon>
	<div class="container container--float no-gutter hide@tablet">
		<div class="row <?php echo isset($data_section['alineacion']) && $data_section['alineacion'] == 'right' ? 'row-reverse col-reverse@tablet' : ''; ?>">
			<div class="gr-5 gr-6@medium gr-12@tablet no-gutter"><div class="float__square bg-dark" data-eq></div></div>
			<div class="gr-7 gr-6@medium gr-12@tablet no-gutter"><div class="float__square bg-light-main" data-eq></div></div>
		</div>
	</div>
	<div class="container full-height no-gutter@tablet">
		<div class="row <?php echo isset($data_section['alineacion']) && $data_section['alineacion'] == 'right' ? 'row-reverse col-reverse@tablet' : ''; ?>">
			<div class="gr-4 gr-5@large gr-6@medium gr-12@tablet no-gutter-vertical no-gutter@tablet <?php echo $data_section['alineacion'] == 'right' ? 'prefix-1 prefix-0@large' : ''; ?>">
				<article class="box box--square bg-dark" data-eq>
					<div class="box__body gr-12@book gutter-double@tablet">
						<?php if(isset($data_section['titulo']) && !empty($data_section['titulo'])): ?>
						<h2 class="box__title title-decorated"><?php  echo $data_section['titulo']; ?></h2>
						<?php endif; ?>
						<?php if(isset($data_section['bajada']) && !empty($data_section['bajada'])): ?>
						<div class="box__excerpt">
							<?php  echo $data_section['bajada']; ?>
						</div>
						<?php endif; ?>
						<?php if(isset($data_section['enlace']) && !empty($data_section['enlace'])): ?>
						<div class="box__action">
							<a href="<?php echo $data_section['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $data_section['enlace']['title']; ?>" <?php echo isset($data_section['enlace']['target']) && !empty($data_section['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $data_section['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<div class="gr-7 gr-6@medium gr-12@tablet no-gutter@tablet <?php echo isset($data_section['alineacion']) && $data_section['alineacion'] !== 'right' ? 'prefix-1 prefix-0@large' : ''; ?>">
				<figure class="box box--graph bg-light-main">
					<?php $data_grpah = isset($data_section['api_o_archivo']) && $data_section['api_o_archivo'] =="api" ? $data_section['api_rest']:$data_section['datos_json']; ?>
					<div id="chart-<?php  echo sanitize_title($data_section['titulo']); ?>" data-role="spline-area-chart" data-bg="#EAF9FD" data-colors="#4AC6E6,#8E8E8E" data-points="<?php echo $data_grpah ?>" data-title="<?php echo $data_section['titulo_grafico']; ?>" data-axisy-izq="<?php echo $data_section['titulo_axis_y_izq']; ?>" data-axisy-der="<?php echo $data_section['titulo_axis_y_der']; ?>" data-intervalo-x="<?php echo $data_section['intervalo_x']; ?>" data-intervalo-y="<?php echo $data_section['intervalo_y']; ?>" data-intervalo-y2="<?php echo $data_section['intervalo_y2']; ?>"></div>
				</figure>
			</div>
		</div>
	</div>
</section>
