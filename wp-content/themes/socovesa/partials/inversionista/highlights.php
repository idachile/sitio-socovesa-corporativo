<?php global $data_section; ?>
<section id="five" class="horizon horizon--float">
  <div class="container">
    <div class="row heels-small">
      <div class="gr-8 gr-10@book gr-12@tablet gr-centered">
        <h2 class="horizon__title title-center"><?php  echo $data_section['titulo']; ?></h2>
        <div class="horizon__excerpt text-center">
          <?php  echo $data_section['bajada']; ?>
        </div>
      </div>
    </div>

	 <div class="gr-centered">
		<div class="grid grid-cols-<?php echo count($data_section['bloques']); ?> grid-cols-4@medium grid-cols-2@tablet grid-cols-1@phablet grid--position__relative" data-role="parent-grid" data-maxgrow="<?php echo count($data_section['bloques']); ?>" data-maxgrow-medium="4" data-maxgrow-tablet="2" id="highlights-grid">
			<?php $i=1; foreach($data_section["bloques"] as $bloques ): ?>
			<div class="grid-item grid-item--position__unset">
				<article class="box box--block" data-grow="left-right">
					<div class="box__head">
					<h3 class="box__title"><?php echo $bloques['cifra']; ?></h3>
					<p><?php echo $bloques['tagline']; ?></p>
					</div>
					<div class="box__body box__body--adapter">
					<div class="row">
						<button class="box__body__close" data-role="close-block"></button>
						<div class="box__content flex flex-center gr-6 gr-12@medium gutter-triple-right">
							<h4 class="box__title"><?php  echo $bloques['modal']['titulo']; ?></h4>
							<div class="box__excerpt">
							<?php  echo $bloques['modal']['bajada']; ?>
							</div>
						</div>
						<div class="box__graphics flex flex-center gr-6 gr-12@medium gutter-double box__graphics--custom">
							<div class="row">
							<?php if($bloques['modal']['json_1']!="" && $bloques['modal']['json_2']!=""): ?>
								<div class="box box--graph gr-6 gr-12@tablet" data-maxwidth="230" data-maxwidth-medium="235" data-maxwidth-tablet="400">
									<div id="column-<?php echo $i; ?>-1" data-role="<?php echo $bloques['modal']['tipo_grafico_1'] ?>" data-bg="#F0F0F1" data-color="#54c9e8" data-title="<?php echo $bloques['modal']['titulo_grafico_1']; ?>" data-points="<?php echo $bloques['modal']['json_1']; ?>" data-axisy-izq="<?php echo $bloques['modal']['titulo_axis_y_1']; ?>" data-axisy-der="<?php echo $bloques['modal']['titulo_axis_y2_1']; ?>" data-intervalo-x="<?php echo $bloques['modal']['intervalo_axis_x_1']; ?>" data-intervalo-y="<?php echo $bloques['modal']['intervalo_axis_y_1']; ?>" data-intervalo-y2="<?php echo $bloques['modal']['intervalo_axis_y2_1']; ?>" style="min-height: 400px;"></div>
								</div>
									<div class="box box--graph gr-6 gr-12@tablet" data-maxwidth="230" data-maxwidth-medium="235" data-maxwidth-tablet="400">
									<div id="column-<?php echo $i; ?>-2" data-role="<?php echo $bloques['modal']['tipo_grafico_2'] ?>" data-bg="#F0F0F1" data-color="#54c9e8" data-title="<?php echo $bloques['modal']['titulo_grafico_2']; ?>" data-points="<?php echo $bloques['modal']['json_2']; ?>" data-axisy-izq="<?php echo $bloques['modal']['titulo_axis_y_2']; ?>" data-axisy-der="<?php echo $bloques['modal']['titulo_axis_y2_2']; ?>" data-intervalo-x="<?php echo $bloques['modal']['intervalo_axis_x_2']; ?>" data-intervalo-y="<?php echo $bloques['modal']['intervalo_axis_y_2']; ?>" style="min-height: 400px;"></div>
								</div>
							<?php else: ?>
                        <?php if(($bloques['modal']['json_1']!="" && $bloques['modal']['json_2']=="") ): ?>
                           <div class="box box--graph gr-12 gr-12@tablet" data-maxwidth="500" data-maxwidth-medium="500" data-maxwidth-tablet="400">
                              <div id="column-<?php echo $i; ?>-1" data-bg="#F0F0F1" data-color-column="#54c9e8" data-color-line="#333333"  data-role="<?php echo $bloques['modal']['tipo_grafico_1'] ?>" data-title="<?php echo $bloques['modal']['titulo_grafico_1']; ?>" data-axisy-izq="<?php echo $bloques['modal']['titulo_axis_y_1']; ?>" data-axisy-der="<?php echo $bloques['modal']['titulo_axis_y2_1']; ?>" data-points="<?php echo $bloques['modal']['json_1']; ?>" data-intervalo-x="<?php echo $bloques['modal']['intervalo_axis_x_1']; ?>" data-intervalo-y="<?php echo $bloques['modal']['intervalo_axis_y_1']; ?>" data-intervalo-y2="<?php echo $bloques['modal']['intervalo_axis_y2_1']; ?>" data-intervalo-y2="<?php echo $bloques['modal']['intervalo_axis_y2_1']; ?>" style="min-height: 400px;"></div>
                           </div>
                        <?php endif; ?>
                        <?php if(($bloques['modal']['json_1']=="" && $bloques['modal']['json_2']!="")): ?>
                           <div class="box box--graph gr-12 gr-12@tablet" data-maxwidth="500" data-maxwidth-medium="500" data-maxwidth-tablet="400">
                              <div id="column-<?php echo $i; ?>-1" data-bg="#F0F0F1" data-color-column="#54c9e8" data-color-line="#333333"  data-role="<?php echo $bloques['modal']['tipo_grafico_2'] ?>" data-title="<?php echo $bloques['modal']['titulo_grafico_2']; ?>" data-axisy-izq="<?php echo $bloques['modal']['titulo_axis_y_2']; ?>" data-axisy-der="<?php echo $bloques['modal']['titulo_axis_y2_2']; ?>" data-points="<?php echo $bloques['modal']['json_2']; ?>" data-intervalo-x="<?php echo $bloques['modal']['intervalo_axis_x_2']; ?>" data-intervalo-y="<?php echo $bloques['modal']['intervalo_axis_y_2']; ?>" data-intervalo-y2="<?php echo $bloques['modal']['intervalo_axis_y2_2']; ?>" style="min-height: 400px;"></div>
                           </div>
                        <?php endif; ?>
							<?php endif; ?>
							</div>
						</div>
					</div>
					</div>
				</article>
			</div>
			<?php $i++; endforeach; ?>
			</div>
		</div>
	</div>
  </section>
