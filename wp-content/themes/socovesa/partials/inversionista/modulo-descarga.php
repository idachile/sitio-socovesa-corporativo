<?php global $data_section; ?>
<section id="seven" class="horizon horizon--float">
  <div class="container no-gutter@small">
    <div class="row" data-equalize="target" data-mq="phablet-down" data-eq-target="[data-eq]">
      <?php foreach($data_section['grupo_de_documentos'] as $docs): ?>
        <div class="gr-6 gr-12@tablet no-gutter-horizontal@small">
          <article class="box box--bg" style="background-image: url(<?php echo $docs['imagen'] ?>)">
            <div class="box__body">
              <h3 class="box__title"><?php echo $docs['titulo'] ?></h3>
              <div class="box__excerpt">
                <?php echo $docs['bajada'] ?>
              </div>
              <ul class="box__downloads">
                <?php
                  foreach($docs['archivos'] as $download):
                    if(!empty($download['documento']['url'])):
                ?>
                  <li class="box__download"><a href="<?php echo $download['documento']['url'] ?>" title="Descargar documento" download><?php echo $download['documento']['title'] ?></a></li>
                <?php
                    endif;
                  endforeach;
                ?>
				     </ul>
				  <?php if(!empty($docs['enlace']['url'])): ?>
              <div class="box__action">
                <a href="<?php echo $docs['enlace']['url']; ?>" class="button button--main"><?php echo $docs['enlace']['title'] ?></a>
				  </div>
				  <?php endif; ?>
            </div>
          </article>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
