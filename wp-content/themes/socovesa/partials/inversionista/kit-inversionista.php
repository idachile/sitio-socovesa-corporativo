<?php $inversionistas = get_field("kit_inversionista", 12); ?>
<section class="stickit">
	<button class="stickit__deployer" data-func="toggleKit" data-target=".stickit">Kit inversionista</button>
	<button class="stickit__closer hide@small" data-func="toggleKit" data-target=".stickit"></button>
	<div class="stickit__body">
		<div class="grid grid-cols-5 grid-cols-3@book grid-cols-2@phablet" id="kit-grid">
      <?php foreach($inversionistas as $post_id): ?>
			<div class="grid-item">
				<article class="box box--kit" style="background-image: url(<?php echo get_the_post_thumbnail_url($post_id, 'grid_300'); ?>)">
					<div class="box__head">
						<a href="<?php echo get_permalink($post_id); ?>" class="ghost-link" title="Ir a <?php echo get_the_title($post_id); ?>"></a>
						<h3 class="box__title"><?php echo get_the_title($post_id); ?></h3>
					</div>
				</article>
			</div>
    <?php endforeach; ?>
		</div>
	</div>
</section>
