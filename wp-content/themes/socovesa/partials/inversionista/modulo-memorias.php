<?php global $data_section; ?>
<section class="horizon horizon--bg" data-equalize="target" data-mq="tablet-down" data-eq-target="[data-eq]"
	style="background-image: url(<?php echo $data_section['background_image'] ?>)">
	<div class="horizon__veil">
		<div class="container">
			<div class="row" data-eq>
				<div class="gr-6 gr-12@tablet gutter-double@tablet">
					<h2 class="horizon__title gutter-right-double"><?php echo $data_section['titulo'] ?></h2>
					<?php if(!empty($data_section['bajada'])): ?>
					<div class="horizon__excerpt gutter-right-double no-gutter@book">
						<?php echo $data_section['bajada']; ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="gr-6 gr-12@tablet no-gutter">
					<div class="row no-margin" data-eq>
						<div class="gr-6 gr-12@tablet suffix-6@large gutter@large flex-right">
							<?php if(!empty($data_section['portada'])): ?>
							<img class="cover-img" src="<?php echo $data_section['portada'] ?>">
							<?php endif; ?>
						</div>
						<div class="gr-6 gr-12@large flex flex-col-reverse gutter-left-double gutter@large">
							<?php if(!empty($data_section['pdf_file']) || !empty($data_section['archivo'])): ?>
							<ul class="box__downloads no-margin-bottom no-gutter text-small">
								<?php if(!empty($data_section['pdf_file'])): ?>
								<li class="w-bold no-gutter-important">
									<i class="font-color-grey-lightest icon icon-download"><a
											href="<?php echo $data_section['pdf_file']['url']; ?>"
											class="font-color-grey-lightest link-decorated gutter-left"></i><?php echo $data_section['pdf_file']['title']; ?></a>
								</li>
								<?php endif; ?>
								<?php if(!empty($data_section['archivo'])): ?>
								<li class="w-bold no-gutter-important">
									<i class="font-color-grey-lightest icon icon-arrow-right"><a
											href="<?php echo $data_section['archivo']['url'] ?>"
											class="font-color-grey-lightest link-decorated gutter-left"></i><?php echo $data_section['archivo']['title']; ?></a>
								</li>
								<?php endif; ?>
							</ul>
							<?php endif; ?>

							<?php if(!empty($data_section['minisitio'])): ?>
							<div class="box__action no-margin">
								<a href="<?php echo $data_section['minisitio']['url']; ?>"
									class="button button--main no-wspace gr-12@tablet"><?php echo $data_section['minisitio']['title']; ?></a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>