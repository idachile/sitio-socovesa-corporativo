<?php global $data_section; ?>
<section class="horizon horizon--float bg-tall bg-light-main" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-horizon>
	<div class="container container--float no-gutter">
		<div class="row">
			<div class="gr-7 gr-12@tablet no-gutter-horizontal">
				<div class="float__square bg-img" style="background-image: url('<?php  echo $data_section['imagen']; ?>')" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container full-height">
		<div class="row">
			<div class="gr-4 prefix-1 gr-6@medium prefix-0@medium gr-10@tablet gr-12@small">
				<article class="box box--square bg-img" data-eq>
					<div class="box__body gr-12@book no-gutter@small">
						<div class="box__excerpt">
							<?php  echo $data_section['bajada']; ?>
						</div>
						<?php if(!empty($data_section['enlace'])): ?>
						<div class="box__action">
							<a href="<?php echo $data_section['enlace']['url']; ?>" class="button button--ghost-white"
								title="<?php echo $data_section['enlace']['title']; ?>"><?php echo $data_section['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<div class="gr-5 prefix-1 gr-6@large gr-6@medium prefix-0@medium gr-12@tablet no-gutter">
				<div class="float__image hat-big hat-not@tablet full" data-rellax data-rellax-speed="1" data-rellax-percentage="0.1">
					<div class="grid grid-cols-2 grid-cols-1@phablet" id="modelo-grid">
						<?php $i=1; foreach($data_section["bloques"] as $bloques ): ?>

					<?php if(!empty($bloques['data_expand'])): ?>
						<div class="grid-item" data-grid-num="<?php echo $i; ?>">
							<article class="box box--grid box--expand " data-expand="<?php echo $bloques['data_expand']; ?>"
								<?php if($bloques['data_expand_tablet']): ?>data-expand-tablet="<?php echo $bloques['data_expand_tablet'] ?>"<?php endif; ?>
								<?php if($bloques['data_expand_medium']): ?>data-expand-medium="<?php echo $bloques['data_expand_medium'] ?>" <?php endif; ?>
								<?php if($bloques['imagen']): ?> data-bg="background-image: url('<?php echo $bloques['imagen']; ?>')" <?php endif; ?>
>
								<?php if($bloques['titulo'] !== ""): ?>
								<div class="box__head <?php echo $bloques['imagen'] . !empty($bloques['imagen']) ? 'bg-image' : ''; ?>">
									<h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
								</div>
								<?php endif; ?>
								<div class="box__body <?php echo $bloques['box_color']; ?>">
									<?php echo $bloques['titulo'] ? '<h3 class="box__title">'.$bloques['titulo'] .'</h3>' : '' ?>
									<?php if($bloques['bajada']): ?>
									<div class="box__excerpt">
										<?php echo $bloques['bajada']; ?>
									</div>
									<?php endif; ?>
									<?php if($bloques['enlace']!=''): ?>
									<div class="box__action">
										<a href="<?php echo $bloques['enlace'] ?>" class="button button--ghost-white"
											title="Seguir leyendo" <?php echo !empty($bloques['enlace']['target']) ? 'target="_blank"' : ''; ?>>Seguir leyendo</a>
									</div>
									<?php endif; ?>
								</div>
							</article>
						</div>
						<?php else: ?>
						<div class="grid-item grid-blocked" data-grid-num="<?php echo $i; ?>">
							<article class="box box--grid blocked">
								<div class="box__head <?php echo $bloques['box_color']; ?>">
									<h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
								</div>
							</article>
						</div>
						<?php endif; ?>
						<?php $i++; endforeach; ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>