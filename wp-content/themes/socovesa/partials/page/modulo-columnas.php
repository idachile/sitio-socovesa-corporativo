<?php global $data_section; ?>
<section class="horizon horizon--float">
	<div class="container">
		<div class="row">
			<div class="gr-10 gr-12@medium gr-centered">
				<?php if(!empty($data_section['titulo'])) echo '<h2 class="horizon__title title-center">'.$data_section['titulo'].'</h2>'; ?>

				<?php if(!empty($data_section['bajada'])): ?>
					<div class="row hat hat-tiny@tablet horizon__excerpt" data-role="divide-text" data-col-class="gutter-double gr-6 gr-12@tablet no-gutter@tablet"><?php echo $data_section['bajada'] ?></div>
				<?php endif; ?>
				<?php if( ( !empty($data_section['video']) || !empty($data_section['slider']) || !empty($data_section['imagen']) ) && $data_section['media_tipo'] !== 'no-media'): ?>
					<div class="row hat">
						<div class="gr-12">
							<?php if($data_section['media_tipo'] == "video" && !empty($data_section['video'])): ?>
								<div class="box box--media">
									<video data-role="video-player" src="<?php echo $data_section['video']; ?>" type="video/mp4"></video>
									<?php if(!empty($data_section['video_prev'])): ?>
									<figure class="video-prev" data-role="video-prev" style="background-image: url('<?php echo $data_section['video_prev'] ?>')"><div class="video-prev__veil"></div></figure>
									<?php endif; ?>
									<div class="video__control">
										<button class="video__control__deployer" data-role="video-player-deployer"></button>
									</div>
								</div>
							<?php endif; ?>
							<?php if($data_section['media_tipo'] == "slider"  && !empty($data_section['slider'])): ?>
								<div class="slider slider--single hat" data-module="slider" data-transition="false">
									<div class="slider__items" data-role="slider-list" data-equal-media>
										<?php $i=1; $images = $data_section['slider']; ?>
										<?php foreach($images as $image): ?>
											<div class="slider__slide <?php if($i==1): ?>current<?php endif; ?>" data-role="slider-slide">
												<figure class="slider__figure">
													<img src="<?php echo $image['sizes']['big_1024x576'] ?>" alt="<?php echo $image['alt']; ?>" class="cover-img" />
												</figure>
											</div>
										<?php endforeach; ?>
									</div>
									<?php if(count($data_section['slider']) > 1): ?>
									<div class="slider__arrows">
										<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
										data-direction="prev"></button>
										<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
										data-direction="next"></button>
									</div>
									<div class="slider__bullets">
										<?php
										$i=0;
										foreach($images as $slide): ?>
										<button class="slider__bullet <?php if($i==0): ?>slider__bullet--current<?php endif; ?>" data-role="slider-bullet" data-target="<?php echo $i; ?>"></button>
											<?php $i++; endforeach; ?>
										</div>
									</div>
									<?php endif; ?>
								<?php endif; ?>
								<?php if($data_section['media_tipo'] == 'single-image' &&  !empty($data_section['imagen'])): ?>
									<h2 class="horizon__title title-center"><?php echo $data_section['titulo_imagen'];?></h2>
									<div class="slider slider--single hat" data-module="slider" data-transition="false">
										<div class="slider__items" data-role="slider-list" data-equal-media>
											<div class="slider__slide" data-role="slider-slide">
												<figure class="slider__figure">
													<img src="<?php echo $data_section['imagen']['url'] ?>" alt="<?php echo $image['alt']; ?>" class="cover-img" />
												</figure>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
