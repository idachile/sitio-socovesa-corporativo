<?php
global $data_section;
$persona_id = $data_section['persona'][0];
if(!empty($persona_id)):
	$persona = get_post($persona_id);
?>
<section class="horizon horizon--float">
	<div class="container">
		<div class="row">
			<div class="gr-4 gr-3@book gr-5@tablet suffix-7@tablet">
			<?php if(has_post_thumbnail($persona_id)) echo '<figure class="persona__thumbnail">'. get_the_post_thumbnail($persona_id, 'avatar_3x4', array('class' => 'cover-img')) .'</figure>'; ?>
			</div>
			<div class="gr-8 gr-9@book gr-12@tablet hat@tablet">
				<div class="single__header">
					<?php if(get_field('cargo', $persona_id)) echo '<p class="single__meta">'.get_field('cargo', $persona_id).'</p>'; ?>
					<?php echo '<h1 class="single__title">'.get_the_title($persona_id).'</h1>'; ?>
				</div>
				<?php if(!empty($persona->post_content)) echo '<div class="row single__content" data-role="divide-text" data-col-class="gutter-double gr-6 gr-12@tablet">'. apply_filters('the_content', $persona->post_content) .'</div>';?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>