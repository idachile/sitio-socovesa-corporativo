<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ):
   if($bloque["push_box"]!=""){
      $bloque["push_box"] = $bloque["push_box"]. ' prefix-0@tablet ';
   }
   if($bloque["alineacion"]=="right"){
      $alingClass =' row-reverse';
   }
   if($bloque["alineacion"]=="left"){
      $alingClass ='';
   }  
   if($bloque["color"]=="bg-light-ice" || $bloque["color"]=="bg-grey-lightest"){
		$text_color="";
		$deco_color = '-dark';
   }else{
		$text_color=" text-light";
		$deco_color = '';
   }
?>
<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
	data-type="modulo-simple">
	<div class="container container--float-full no-gutter" data-eq>
		<div class="row">
			<div class="<?php echo $bloque["grid_box"];  ?> gr-12@tablet no-gutter <?php echo $bloque["push_box"];  ?>">
				<div class="<?php if($bloque["color"]!="bg-grey-lightest"): ?>float__square<?php endif; ?> <?php echo $bloque["color"]; ?>" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container full-height">
		<div class="row col-reverse@tablet <?php echo $alingClass; ?>">
			<?php //addclass prefix-1 prefix-0@book ?>
			<div class="gr-4 gr-5@book gr-12@tablet flex-center <?php echo $bloque['alineacion'] == 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<article class="box box--simple<?php echo $text_color; ?>">
					<div class="box__body">
						<?php if($bloque["numeral"]): ?><span
							class="box__counter"><?php echo $i <= 9 ? "0".$i: $i;  ?></span><?php endif; ?>
						<h2 class="box__title title-decorated<?php echo $deco_color; ?> medium"><?php echo $bloque["titulo"] ?></h2>
						<div class="box__excerpt">
							<?php echo $bloque["bajada"]; ?>
						</div>
						<?php if($bloque['enlace']!=''): ?>
						<div class="box__action">
							<a href="<?php echo $bloque['enlace']['url']; ?>" class="button <?php echo $bloque['color_link'] ?>" title="<?php echo $bloque['enlace']['title']; ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<?php //addclass prefix-1 prefix-0@book ?>
			<div class="gr-7 gr-12@tablet flex-center <?php echo $bloque['alineacion'] !== 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<?php if(!empty($bloque['imagen']) && $bloque['media_tipo'] == 'imagen'): ?>
				<figure class="box__figure">
					<img src="<?php  echo $bloque['imagen']; ?>" alt="<?php echo $bloque["titulo"] ?>" class="cover-img" />
				</figure>
				<?php endif; ?>
				<?php if(!empty($bloque['video']) && $bloque['media_tipo'] == 'video'): ?>
				<div class="box box--media">
					<video data-role="video-player" src="<?php echo $bloque['video']; ?>" type="video/mp4"></video>
					<?php if(!empty($bloque['video_prev'])): ?>
						<figure class="video-prev" data-role="video-prev" style="background-image: url('<?php echo $bloque['video_prev'] ?>')"><div class="video-prev__veil"></div></figure>
					<?php endif; ?>
					<div class="video__control">
						<button class="video__control__deployer" data-role="video-player-deployer"></button>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php $i++; endforeach; ?>