<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ): ?>
<section class="horizon horizon--marcas no-gutter@tablet" data-type="modulo-img-rse">
	<div class="container heels@tablet">
		<div class="row">
			<div class="gr-8 gr-10@medium gr-12@small gr-centered">
				<h2 class="horizon__title title-center"><?php  echo $bloque['titulo']; ?></h2>
				<div class="horizon__excerpt font-center">
					<?php  echo $bloque['bajada']; ?>
				</div>
			</div>
		</div>
	</div>
	<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
		<div class="container container--float-full no-gutter" data-eq>
			<div class="row">
				<div class="gr-10 no-gutter gr-12@tablet <?php echo $bloque['alineacion'] == 'right' ? 'prefix-2 prefix-0@tablet' : ''; ?>">
					<div class="float__square <?php echo $bloque['color'] !== '' ? $bloque['color'] : 'bg-light-ice'; ?>" data-eq></div>
				</div>
			</div>
		</div>
		<div class="container full-height" data-rellax data-rellax-speed="3">
			<div class="row heels heels-not@small <?php echo $bloque['alineacion'] == 'right' ? 'row-reverse col-reverse@tablet' : ''; ?>" data-equalize="target" data-mq="small-down" data-eq-target="[data-equalize]">
				<div class="gr-4 gr-5@book gr-12@tablet <?php echo $bloque['alineacion'] == 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
					<article class="box box--simple" data-equalize>
						<div class="box__body <?php if($bloque['color'] == 'bg-dark') echo 'text-light'; ?>">
							<h2 class="box__title title-decorated<?php if($bloque['color'] !== 'bg-dark') echo '-dark'; ?> medium"><?php  echo $bloque['titulo']; ?></h2>
							<div class="box__excerpt">
								<?php  echo $bloque['bajada']; ?>
							</div>
							<?php if($bloque['enlace']!=''): ?>
							<div class="box__action">
								<a href="<?php echo $bloque['enlace']['url']; ?>" class="button <?php echo $bloque['color_link'] ?>"
									title="<?php echo $bloque['enlace']['title']; ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
							</div>
							<?php endif; ?>
						</div>
					</article>
				</div>
				<div class="gr-7 gr-12@tablet flex-center <?php echo $bloque['alineacion'] !== 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
					<figure class="box__figure">
						<img src="<?php  echo $bloque['imagen']; ?>" alt="<?php  echo $bloque['titulo']; ?>" class="cover-img" />
					</figure>
				</div>
			</div>
			<div class="grid grid-cols-5 grid-cols-2@tablet grid-cols-1@phablet heels" id="people-grid">
				<?php $images = $bloque['slider']; ?>
				<?php if(!empty($images)): ?>
				<?php foreach($images as $image): ?>
				<div class="grid-item">
					<img src="<?php echo $image['sizes']['square_300'] ?>" alt="<?php echo $image['alt']; ?>" class="cover-img" />
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</section>
<?php $i++; endforeach; ?>