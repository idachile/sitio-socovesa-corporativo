<?php global $data_section; ?>
<section id="eight" class="horizon horizon--bg margin-vertical-triple"
   style="background-image: url(<?php echo $data_section['background_image'] ?>); margin: 3rem 0!important;">
   <div class="horizon__veil">
      <div class="container">
         <div class="row">
            <div class="gr-6 gr-12@book gutter-double@book">
               <h2 class="horizon__title gutter-right-double"><?php echo $data_section['titulo'] ?></h2>
               <div class="horizon__excerpt font-justified gutter-right-double">
                  <?php echo $data_section['bajada']; ?>
               </div>
            </div>
            <div class="gr-6 gr-12@book gutter-double@book margin-top-big" data-rellax data-rellax-speed="2"
               data-rellax-percentage="0.5">
               <ul class="box__downloads no-margin-bottom no-gutter text-small ">
                  <?php $documentos = $data_section['documentos']; ?>
                  <?php if(!empty($documentos)): ?>
                  <?php foreach ($documentos as $documento): ?>
                  <?php $archivo = $documento['archivo']; ?>
                  <li class="w-bold no-gutter-important">
                     <i class="font-color-grey-lightest icon icon-download"><a href="<?php echo $archivo['url'] ?>" class="font-color-grey-lightest link-decorated gutter-left"></i><?php echo $documento['nombre'] ?></a>
                  </li>
                  <?php endforeach;?>
                  <?php endif;?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>