<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ):
   if($bloque["alineacion"]=="left"){
      $reverse = 'row-reverse';
      $prefix ='';
   }
   if($bloque["alineacion"]=="right"){
      $reverse = '';
      $prefix ='prefix-2 prefix-1@book prefix-0@tablet';
   }
?>
<section class="horizon horizon--normal" data-equalize="target" data-mq="tablet-down" data-eq-target="[data-eq]">
   <div class="container container--float full-absolute-fam@tablet">
      <div class="container container--float-full no-gutter">
         <div class="row">
            <div class="gr-12 gutter-triple-top no-gutter@medium">
               <div class="<?php if($bloque["color"]!="bg-grey-lightest"): ?>float__square<?php endif; ?> <?php echo $bloque["color"]; ?>" data-eq></div>
            </div>
         </div>
      </div>
   </div>
   <div class="container takeoff">
      <div class="row takeoff__head <?php echo $reverse; ?>" data-eq>
         <div class="gr-8 gr-7@book gr-6@tablet gr-12@small no-gutter">
            <figure class="takeoff__figure">
               <img src="<?php  echo $bloque['imagen']; ?>" alt="<?php echo $bloque["titulo"] ?>" class="cover-img" />
            </figure>
         </div>
         <div class="gr-4 gr-5@book gr-6@tablet gr-12@small gutter-double flex-center">
            <h2 class="takeoff__title"><?php echo $bloque["titulo"] ?></h2>
         </div>
      </div>
      <div class="row takeoff__body">
         <div class="gr-10 gr-11@book gr-12@tablet no-gutter <?php echo $prefix; ?>">
            <div class="takeoff__excerpt row" data-role="divide-text" data-col-class="gutter-double gr-6 gr-12@tablet no-gutter@tablet">
               <?php echo $bloque["bajada"]; ?>
            </div>
            <?php if($bloque['enlace']!=''): ?>
            <div class="box__action flex-center" style="padding: 0 1.5rem">
               <a href="<?php echo ensure_url($bloque['enlace']['url']) ?>" class="button <?php echo $bloque['color_link'] ?>" title="<?php echo $bloque['enlace']['title']; ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
            </div>
            <?php endif; ?>
         </div>
      </div>
   </div>
</section>
<?php $i++; endforeach; ?>