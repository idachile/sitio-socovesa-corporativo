
<?php global $data_section; ?>
<section id="six" class="horizon horizon--float">
	<?php if($data_section['head']==true):?>
	<div class="container">
		<div class="row heels-small">
			<div class="gr-8 gr-10@book gr-12@tablet gr-centered">
				<h2 class="horizon__title title-center"><?php  echo $data_section['titulo']; ?></h2>
				<div class="horizon__excerpt text-center">
					<?php  echo $data_section['bajada']; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<div data-equalize="target" data-mq="tablet-down" data-eq-target="[data-eq]">
	<div class="container container--float no-gutter hide@tablet">
		<div class="row">
			<div class="gr-5 gr-6@medium gr-12@tablet no-gutter">
				<div class="float__square bg-dark" data-eq></div>
			</div>
			<div class="gr-7 gr-6@medium gr-12@tablet no-gutter">
				<div class="float__square" data-eq data-role="dinamic-image" style="background-image: url(<?php echo wp_get_attachment_image_url($data_section['slides'][0]['imagen'], 'full', false); ?>)"></div>
			</div>
		</div>
	</div>
	<div class="container no-gutter@tablet">
		<div class="slider slider--diapo slider--blocks" data-module="slider" data-transition="false">
			<div class="slider__items" data-role="slider-list">
				<?php
				$i=1;
				foreach($data_section['slides'] as $slide):
					?>
					<div class="slider__slide <?php if($i==1): ?>current<?php endif; ?>" data-role="slider-slide" data-bg-img="<?php echo wp_get_attachment_image_url($slide["imagen"], 'full', false); ?>">
						<div class="row">
							<div class="gr-5 gr-6@medium gr-12@tablet no-gutter-vertical">
								<article class="box box--square bg-dark" data-eq>
									<div class="box__body gr-12@book gutter-double@tablet">
										<h2 class="box__title title-decorated"><?php echo $slide["titulo"]; ?></h2>
										<div class="box__excerpt">
											<?php  echo $slide["bajada"]; ?>
										</div>
										<?php if(!empty($slide['enlace'])): ?>
										<div class="box__action">
											<a href="<?php  echo  $slide["enlace"]['url']; ?>" class="button button--ghost-white" title="<?php echo $slide['enlace']['title']; ?>" <?php echo !empty($slide['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $slide['enlace']['title']; ?></a>
										</div>
										<?php endif; ?>
									</div>
								</article>
							</div>
							<div class="gr-7 gr-6@medium gr-12@tablet no-gutter@tablet">
								<figure class="box__figure show@tablet">
									<?php echo wp_get_attachment_image($slide['imagen'], 'thumb-landing-ebook', false, array('class' => 'cover-img')); ?>
								</figure>
							</div>
						</div>
					</div>
					<?php $i++; endforeach; ?>
				</div>
				<?php if(count($data_section['slides']) > 1): ?>
				<div class="arrows-container gr-5 gr-6@medium no-gutter@medium gr-12@tablet">
					<div class="slider__arrows">
						<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>
						<button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>
					</div>
				</div>
				<div class="bullets-container prefix-5 prefix-6@medium gr-12@tablet prefix-0@tablet">
					<div class="slider__bullets">
						<?php
						$i=0;
						foreach($data_section['slides'] as $slide):
							?>
							<button class="slider__bullet <?php if($i==0): ?>slider__bullet--current<?php endif; ?>" data-role="slider-bullet" data-target="<?php echo $i; ?>"></button>
							<?php $i++; endforeach; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
