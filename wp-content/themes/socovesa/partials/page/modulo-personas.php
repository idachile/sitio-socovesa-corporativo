<?php global $data_section; ?>
<section class="horizon horizon--float">
	<div class="container">
		<div class="row heels">
			<div class="gr-10 gr-centered gr-12@medium">
			<?php
			if(isset($data_section['titulo']) && !empty($data_section['titulo'])):
				echo '<h2 class="horizon__title title-center">'.$data_section['titulo'].'</h2>';
			endif;

			if(isset($data_section['bajada']) && !empty($data_section['bajada'])):
				echo '<div class="horizon__excerpt font-centered">'.apply_filters('the_content', $data_section['bajada']).'</div>';
			endif;
			?>
			</div>
		</div>
	<?php
	$ids                    = array();
	if(isset($data_section['tipo']) && !empty($data_section['tipo'])):
		$tipo_sel               = $data_section['tipo'];
	else:
		$tipo_sel = '';
	endif;
   $args['post_type']      = 'persona';
	$args['post_status']    = 'publish';
	$setting['random_imagenes'] 	= $data_section['imagenes_random'];

	if(isset($tipo_sel) && !empty($tipo_sel) && $tipo_sel !== 'manual'):
		$taxes = $data_section[$data_section['tipo']];
		if(!empty($taxes)):
			foreach($taxes as $tax):
				$ids[] = $tax->term_id;
			endforeach;
			$args['posts_per_page'] = -1;
			$args['tax_query'] = array(
				array(
					'taxonomy' => $data_section['tipo'],
					'field' => 'term_id',
					'terms' => $ids
				)
			);		
		endif;
	else:
		$args['orderby'] = 'post__in';
		$args['post__in'] = $data_section['personas'];
	endif;
	?>
		<div class="heels">
			<div class="grid grid-cols-5 grid-cols-2@tablet grid-cols-1@phablet" id="equipo-grid">
			<?php
			$persona = new persona();
			$persona_print = '';
			$persona_print = $persona->get_grilla_personas($args, $setting);

			if(isset($persona_print) && !empty($persona_print)):
				echo $persona_print;
			endif;
			?>
			</div>
		</div>
	</div>
</section>