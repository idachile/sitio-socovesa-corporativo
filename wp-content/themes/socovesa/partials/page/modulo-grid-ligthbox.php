<?php global $data_section; ?>

<section class="horizon horizon--float bg-tall">
	<?php if(!empty($data_section['head'])): ?>
	<div class="container heels-small">
		<div class="row">
			<div class="gr-8 gr-10@book gr-12@tablet gr-centered">
				<?php if(!empty($data_section['titulo'])) echo '<h2 class="horizon__title title-center">'.$data_section['titulo'].'</h2>'; ?>
				<?php if(!empty($data_section['bajada'])) echo '<div class="horizon__excerpt text-center">'.apply_filters('the_content', $data_section['bajada']).'</div>'; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if(!empty($data_section['bloques'])): $contador = 0; ?>
	<div class="container main no-gutter">
		<div class="grid grid-cols-5 grid-cols-4@medium grid-cols-2@tablet grid-cols-1@phablet grid--position__relative" data-role="parent-grid" data-maxgrow="5" data-maxgrow-medium="4" data-maxgrow-tablet="1">
		<?php foreach($data_section['bloques'] as $block): $contador++;?>
			<?php if($block['habilitar_ligthbox']): ?>
			<div class="grid-item grid-item--position__unset" data-grid-num="<?php echo $contador; ?>">
				<article class="box box--block" data-grow="left-right">
					<div class="box__head" style="background-image: url(<?php echo wp_get_attachment_image_url($block['imagen'], 'thumb-landing-ebook', false); ?>);">
						<div class="box__head__veil click"></div>
					</div>
					<div class="box__body box__body--adapter" style="background-image: url(<?php echo wp_get_attachment_image_url($block['imagen'], 'portada_1024x360', false); ?>)">
						<div class="body__veil"></div>
						<div class="row">
							<button class="box__body__close" data-role="close-block"></button>
							<div class="gr-4 gr-12@medium flex flex-center">
								<div class="box__content">
									<h4 class="box__title title-decorated"><?php echo $block['titulo']; ?></h4>
									<?php if(!empty($block['bajada'])) echo '<div class="box__excerpt">'.apply_filters('the_content', $block['bajada']).'</div>'; ?>
									<?php if(!empty($block['enlace'])): ?>
									<div class="box__action">
										<a href="<?php echo $block['enlace']['url']; ?>" class="button button--main" title="<?php echo $block['enlace']['title']; ?>" <?php echo !empty($block['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $block['enlace']['title']; ?></a>
									</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="gr-7 gr-10@medium gr-12@small prefix-0@small prefix-1 hat@medium">
							<?php if(!empty($block['video'])): ?>
								<div class="box box--media">
									<video data-equal-media data-role="video-player" src="<?php echo $block['video']; ?>" type="video/mp4"></video>
									<?php if(!empty($block['video_prev'])): ?>
										<figure class="video-prev" data-role="video-prev" style="background-image: url('<?php echo $block['video_prev'] ?>')"><div class="video-prev__veil"></div></figure>
									<?php endif; ?>
									<div class="video__control">
										<button class="video__control__deployer" data-role="video-player-deployer"></button>
									</div>
								</div>
							<?php endif; ?>
							</div>
						</div>
					</div>
				</article>
			</div>
			<?php else: ?>
			<div class="grid-item grid-item--position__unset" data-grid-num="<?php echo $contador; ?>">
				<article class="box box--grid">
					<div class="box__head <?php echo $block['box_color']; ?>">
						<h3 class="box__title"><?php echo $block['titulo'] ?></h3>
					</div>
				</article>
			</div>
			<?php endif; ?>
		<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>
</section>