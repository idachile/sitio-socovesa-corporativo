<?php global $data_section; ?>
<?php $i=1; foreach($data_section["bloques"] as $bloque ): ?>
<?php if($bloque["alineacion"]=="left"): ?>
<section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-horizon>
   <div class="container full-height no-gutter@tablet">
      <div class="row floater">
         <div class="gr-12 no-gutter@tablet">
            <figure class="horizon__bg" style="background-image: url(<?php  echo $bloque['imagen']; ?>)" data-eq></figure>
         </div>
      </div>
      <div class="row relative" data-eq>
         <div class="gr-5 prefix-7 gr-8@book prefix-4@book gr-12@tablet prefix-0@tablet no-gutter@tablet">
            <article class="box box--float box--float--top bg-white" data-rellax data-rellax-speed="1">
               <div class="box__body">
                  <h2 class="box__title title-decorated"><?php echo $bloque["titulo"] ?></h2>
                  <div class="box__excerpt">
                     <?php echo $bloque["bajada"] ?>
                  </div>
                  <?php if($bloque['enlace']!=''): ?>
                  <div class="box__action">
                     <a href="<?php echo $bloque['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $bloque['enlace']['title']; ?>"><?php echo $bloque['enlace']['title']; ?></a>
                  </div>
                  <?php endif; ?>
               </div>
            </article>
         </div>
      </div>
   </div>
</section>
<?php elseif($bloque["alineacion"]=="right"): ?>
<section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-horizon>
   <div class="container container--float no-gutter">
      <div class="row">
         <div class="gr-8 prefix-4 gr-12@tablet prefix-0@tablet no-gutter">
            <figure class="horizon__bg" style="background-image: url(<?php  echo $bloque['imagen']; ?>)" data-eq>
            </figure>
         </div>
      </div>
   </div>
   <div class="container full-height margin--top no-gutter@tablet">
      <div class="row relative" data-eq>
         <div class="gr-6 gr-7@book gr-12@tablet no-gutter@tablet">
            <article class="box box--float box--float--left bg-white" data-rellax data-rellax-speed="-1">
               <div class="box__body">
                  <h2 class="box__title title-decorated"><?php echo $bloque["titulo"] ?></h2>
                  <div class="box__excerpt"><?php echo $bloque["bajada"] ?></div>
                  <?php if($bloque['enlace']!=''): ?>
                  <div class="box__action">
                     <a href="<?php echo $bloque['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $bloque['enlace']['title']; ?>"><?php echo $bloque['enlace']['title']; ?></a>
                  </div>
                  <?php endif; ?>
               </div>
            </article>
         </div>
      </div>
   </div>
</section>
<?php elseif($bloque["alineacion"]=="home"): ?>
<section class="horizon horizon--float bg-tall" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
   data-horizon>
   <div class="container container--float no-gutter">
      <div class="row">
         <div class="gr-7 gr-12@tablet no-gutter-horizontal">
            <div class="float__square bg-dark" data-eq></div>
         </div>
      </div>
   </div>
   <div class="container full-height">
      <div class="row">
         <div class="gr-5 gr-6@medium gr-12@tablet">
            <article class="box box--square bg-dark" data-eq>
               <div class="box__body gr-12@book no-gutter">
                  <h2 class="box__title"><?php echo $bloque["titulo"] ?></h2>
                  <div class="box__excerpt">
                     <?php echo $bloque["bajada"] ?>
                  </div>
                  <?php if($bloque['enlace']!=''): ?>
                  <div class="box__action">
                     <a href="<?php echo $bloque['enlace']['url']; ?>" class="button button--main" title="<?php echo $bloque['enlace']['title']; ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
                  </div>
                  <?php endif; ?>
               </div>
            </article>
         </div>
         <div class="gr-5 prefix-1 gr-6@large gr-6@medium prefix-0@medium gr-12@tablet">
            <figure class="float__image hat hat-not@tablet" data-rellax data-rellax-speed="1" data-rellax-percentage="0.1">
               <img src="<?php  echo $bloque["imagen"]; ?>" alt="Imagen de " class="cover-img" />
            </figure>
         </div>
      </div>
   </div>
</section>

<?php endif; ?>
<?php endforeach; ?>
