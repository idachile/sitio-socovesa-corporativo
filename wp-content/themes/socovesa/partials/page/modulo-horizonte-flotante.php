<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ):

	if(isset($bloque['color']) && !empty($bloque["color"])):
		if($bloque["color"]=="bg-light-ice" || $bloque["color"]=="bg-grey-lightest"){
			$text_color="";
		}else{
			$text_color=" text-light";
		}
	endif;
?>
<section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-module="horizonte-flotante">
		<div class="container container--float no-gutter">
			<div class="row floater">
				<div class="gr-12 no-gutter"><figure class="horizon__bg" style="background-image: url(<?php  echo $bloque['imagen']; ?>)" data-eq></figure></div>
			</div>
		</div>
		<div class="container full-height margin--top no-gutter@tablet">
			<div class="row relative ajuste-prefix" data-eq>
				<div class="gr-6 gr-8@book gr-12@tablet no-gutter <?php echo $bloque['alineacion'] == 'right' ? 'prefix-6 prefix-4@book prefix-0@tablet' :  ''; ?>">
					<article class="box box--float box--float--tall bg-white">
						<div class="box__body">
							<h2 class="box__title title-decorated-dark <?php echo $title; ?>"><?php echo $bloque["titulo"]; ?></h2>
							<?php if(!empty($bloque['bajada'])) echo '<div class="box__excerpt">' . apply_filters('the_content', $bloque['bajada']) . '</div>'; ?>

							<?php if(!empty($bloque['enlace']) || !empty($bloque['enlace_secundario'])): ?>
							<div class="box__action <?php echo !empty($bloque['enlace']) && !empty($bloque['enlace_secundario']) ? 'flex-space-btwn' : '' ?>">
								<?php if(!empty($bloque['enlace'])): ?>
								<a href="<?php echo ensure_url($bloque['enlace']['url']) ?>" class="button button--main"
									title="<?php echo $bloque['enlace']['title'] ?>"><?php echo $bloque['enlace']['title'] ?></a>
								<?php endif; ?>
								<?php if(!empty($bloque['enlace_secundario'])): ?>
								<a href="<?php echo ensure_url($bloque['enlace_secundario']['url']) ?>" class="button button--main"
									title="<?php echo $bloque['enlace_secundario']['title'] ?>"><?php echo $bloque['enlace_secundario']['title'] ?></a>
								<?php endif; ?>
							</div>
							<?php endif; ?>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $i++; endforeach; ?>