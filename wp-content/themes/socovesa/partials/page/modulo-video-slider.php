<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ):
  ?>

<section id="nineteen" class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
	data-type="modulo-video-slider">
	<div class="container container--float-full no-gutter" data-eq>
		<div class="row">
			<div
				class="gr-10 gr-12@tablet <?php echo $bloque['alineacion'] == 'right' ? 'prefix-2 prefix-0@tablet' : ''; ?> no-gutter">
				<div class="float__square <?php echo $bloque['color']; ?>" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container full-height" data-rellax data-rellax-speed="3">
		<div class="row heels heels-not@small <?php echo $bloque['alineacion'] == 'right' ? 'row-reverse' : ''; ?>" data-equalize="target" data-mq="tablet-down"
			data-eq-target="[data-equalize]">
			<div class="gr-4 gr-5@book gr-12@tablet <?php echo $bloque['alineacion'] == 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<article class="box box--simple" data-equalize>
					<div class="box__body">
						<h2 class="box__title title-decorated-dark medium"><?php echo $bloque['titulo'] ?></h2>
						<div class="box__excerpt">
							<?php echo $bloque['bajada'] ?>
						</div>
					</div>
				</article>
			</div>

			<div class="gr-7 gr-12@tablet flex-center <?php echo $bloque['alineacion'] !== 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<figure class="box__figure">
					<img src="<?php echo $bloque['imagen'] ?>" alt="<?php echo $bloque['titulo'] ?>" class="cover-img" />
				</figure>
			</div>
		</div>

		<div class="row <?php echo $bloque['alineacion'] == 'right' ? 'row-reverse' : ''; ?>" data-equalize="target" data-mq="small-down" data-eq-target="[data-equal-media]">
			<div class="gr-7 gr-12@tablet <?php echo $bloque['alineacion'] == 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<div class="slider slider--single" data-module="slider" data-transition="false">
					<div class="slider__items" data-role="slider-list" data-equal-media>
						<?php $i=1; $images = $bloque['slider']; ?>
						<?php foreach($images as $image): ?>
						<div class="slider__slide <?php if($i==1): ?>current<?php endif; ?>" data-role="slider-slide">
							<figure class="slider__figure">
								<img src="<?php echo $image['sizes']['big_1024x576'] ?>" alt="<?php echo $image['alt']; ?>"
									class="cover-img" />
							</figure>
						</div>
						<?php endforeach; ?>
					</div>
					<div class="slider__arrows" data-equalize>
						<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow"
							data-direction="prev"></button>
						<button class="slider__arrow slider__arrow--next" data-role="slider-arrow"
							data-direction="next"></button>
					</div>
					<div class="slider__bullets">
						<?php
            $i=0;
            foreach($images as $slide):
              ?>
						<button class="slider__bullet <?php if($i==0): ?>slider__bullet--current<?php endif; ?>"
							data-role="slider-bullet" data-target="<?php echo $i; ?>"></button>
						<?php $i++; endforeach; ?>
					</div>
				</div>
			</div>

			<div class="gr-4 gr-5@book gr-12@tablet <?php echo $bloque['alineacion'] !== 'right' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<?php if(!empty($bloque['video'])): ?>
				<article class="box box--media">
					<video data-equal-media data-role="video-player" src="<?php echo $bloque['video'] ?>"
						type="video/mp4"></video>
					<?php if(!empty($bloque['video_prev'])): ?>
						<figure class="video-prev" data-role="video-prev" style="background-image: url('<?php echo $bloque['video_prev'] ?>')"><div class="video-prev__veil"></div></figure>
					<?php endif; ?>
					<div class="video__control">
						<button class="video__control__deployer" data-role="video-player-deployer"></button>
					</div>
				</article>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php $i++; endforeach; ?>