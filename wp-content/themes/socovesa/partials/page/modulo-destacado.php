<?php global $data_section; ?>
<?php $i=1; foreach($data_section["bloques"] as $bloque ): ?>
  <?php
  if($bloque["color"]=="") {
    $bloque["color"]="bg-main";
    $bloque["button"] = "button--ghost-white";
  }else{
    $bloque["button"] = "button--main";
  }

  if($bloque["color"]!="bg-main"){
    $middle = 'flex-middle';
    $title = 'title-regular';
  }else{
    $middle = '';
    $title = 'title-decorated';
  }

  if($bloque["alineacion"]=="left"){
    $alingClass ='row-reverse';
  }
  if($bloque["alineacion"]=="right"){
    $alingClass ='';
  }

  ?>
  <section class="horizon wait <?php echo $bloque["color"] ?>" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-offset="300" data-horizon>
    <div class="container">
      <div class="row <?php echo $alingClass; ?>  ">
        <div class="gr-6 gr-12@tablet">
          <article class="box box--square <?php echo $bloque["color"] ?>" data-eq>
            <div class="box__body gr-12@book no-gutter">
              <h2 class="box__title <?php echo $title; ?>"><?php echo $bloque["titulo"] ?></h2>
              <div class="box__excerpt">
                <?php echo $bloque["bajada"] ?>
              </div>
              <?php if($bloque['enlace']!=''): ?>
                <div class="box__action">
                  <a href="<?php echo $bloque['enlace']['url']; ?>" class="button <?php echo $bloque["button"]; ?>" title="<?php echo $bloque['enlace']['title'] ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title'] ?></a>
                </div>
              <?php endif; ?>
            </div>
          </article>
        </div>
        <div class="gr-6 prefix-0@medium gr-12@tablet flex-center <?php echo $middle; ?>">
          <figure class="gutter-double" data-rellax data-rellax-speed="1" data-rellax-percentage="0.5">
            <img src="<?php  echo $bloque["imagen"]; ?>" alt="Imagen de <?php echo $bloque["titulo"] ?>" data-reveal="fade-up" class="cover-img" />
          </figure>
        </div>
      </div>
    </div>
  </section>
<?php endforeach; ?>
