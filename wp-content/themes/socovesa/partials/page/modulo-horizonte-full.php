<?php global $data_section;
$i=1;
foreach($data_section["bloques"] as $bloque ):
  if($bloque["alineacion"]=="left"){
    $alingClass =' row-reverse';
    $prefix ='gr-5 gr-8@book gr-12@tablet no-gutter';
    $title ='';
  }
  if($bloque["alineacion"]=="right"){
    $alingClass ='';
    $prefix ='gr-5 gr-8@book gr-12@tablet prefix-7 prefix-4@book prefix-0@tablet no-gutter';
    $title ='title-decorated-black';

  }

  ?>
  <section class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]" data-horizon>
    <div class="container full-height no-gutter@tablet">
      <div class="row floater no-left@tablet gutter-vertical-double">
        <div class="gr-12 no-gutter@tablet">
          <figure class="horizon__bg" style="background-image: url(<?php  echo $bloque['imagen']; ?>)" data-eq></figure>
        </div>
      </div>
      <div class="row relative ajuste-prefix" >
        <div class="<?php echo $prefix; ?>">
          <article class="box box--float box--float--top box--float--big bg-white">
            <div class="box__body">
              <h2 class="box__title title-decorated <?php echo $title; ?>"><?php echo $bloque["titulo"]; ?></h2>
              <div class="box__excerpt">
                <?php echo $bloque["bajada"]; ?>
              </div>
              <?php if(!empty($bloque['enlace']) || !empty($bloque['enlace_secundario'])): ?>
                <div class="box__action <?php echo !empty($bloque['enlace']) && !empty($bloque['enlace_secundario']) ? 'flex-space-btwn' : 'font-righted' ?>">
  					<?php if(!empty($bloque['enlace'])): ?>
						<a href="<?php echo ensure_url($bloque['enlace']['url']) ?>" class="button button--main" title="<?php echo $bloque['enlace']['title'] ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title'] ?></a>
					<?php endif; ?>
					<?php if(!empty($bloque['enlace_secundario'])): ?>
						<a href="<?php echo ensure_url($bloque['enlace_secundario']['url']) ?>" class="button button--main" title="<?php echo $bloque['enlace_secundario']['title'] ?>" <?php echo !empty($bloque['enlace_secundario']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace_secundario']['title'] ?></a>
					<?php endif; ?>
                </div>
              <?php endif; ?>
              </div>
          </article>
        </div>
      </div>
    </div>
  </section>
  <?php $i++; endforeach; ?>
