<?php global $data_section; ?>

<section class="horizon horizon--float heels-big">
   <div class="container">
      <h1 class="horizon__title heels gutter-left@medium"><?php the_title(); ?></h1>
      <div class="row bg-main-lightest">
         <form action="#" method="get" class="flex-wrap no-border" data-role="filtro-documentos">
            <?php if($data_section['ano']): ?>
            <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
               <div class="form-control__container">
                  <!-- <i class="form-control__addon icon-chevron_down"></i> -->
                  <select id="year" name="year" class="form-control__field--select form-control__field form-control__select w-semibold" data-selector="year">
                     <option value="" selected>Todos los años</option>
                  </select>
               </div>
            </div>
            <?php endif;  ?>
            <?php if($data_section['mes']): ?>
            <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
               <div class="form-control__container">
                  <!-- <i class="form-control__addon icon-chevron_down"></i> -->
                  <select id="month" name="month" class="form-control__field--select form-control__field form-control__select w-semibold" data-selector="month">
							<option value="" selected>Todos los meses</option>
                  </select>
               </div>
            </div>
            <?php endif;  ?>
            <?php if($data_section['tipo_documento']): ?>
            <div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
               <div class="form-control__container">
                  <!-- <i class="form-control__addon icon-chevron_down"></i> -->
                  <select id="tipo" name="tipo" class="form-control__field--select form-control__field form-control__select w-semibold" data-selector="tipo">
						<option value="" selected>Todos los tipos de documento</option>
                  </select>
               </div>
            </div>
            <?php endif;  ?>
            <div class="gr-3 gr-12@medium gutter-double gutter@medium font-centered">
               <div class="box__action no-margin">
						<button class="button button--main button--rectangle">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</section>
