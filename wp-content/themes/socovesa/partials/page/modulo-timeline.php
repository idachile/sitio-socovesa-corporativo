
<?php global $data_section; ?>
<section class="horizon horizon--marcas no-gutter@tablet">
	<?php if($data_section['head']==true):?>
		<div class="container heels@tablet">
			<div class="row">
				<div class="gr-8 gr-10@medium gr-12@small gr-centered">
					<h2 class="horizon__title title-center"><?php  echo $data_section['titulo']; ?></h2>
					<div class="horizon__excerpt">
						<?php  echo $data_section['bajada']; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="horizon horizon--marcas">
		<div class="container container--float-full no-gutter">
			<div class="row row--absolute">
				<div class="gr-10 no-gutter gr-12@medium">
					<div class="float__square bg-light-ice"></div>
				</div>
			</div>
		</div>
		<div class="container full-height">
			<div class="slider slider--diapo slider--blocks slider--timeline" data-module="slider" data-transition="false">
				<div class="slider__items" data-role="slider-list" data-equalize="target" data-mq="medium-down" data-eq-target=".row">
					<?php
					$i=1;
					foreach($data_section['slides'] as $slide):
						?>
						<div class="slider__slide <?php if($i==1): ?>current<?php endif; ?>" data-role="slider-slide">
							<div class="row">
								<div class="gr-5 gr-6@book gr-12@medium">
									<article class="box box--square heels-tiny@medium">
										<div class="box__body gutter-big">
											<h2 class="box__title"><?php echo $slide["titulo"]; ?></h2>
											<div class="box__excerpt">
												<?php  echo $slide["bajada"]; ?>
											</div>
											<?php if(!empty($slide["enlace"]["url"])): ?>
												<div class="box__action">
													<a href="<?php  echo  $slide["enlace"]["url"]; ?>" class="button button--ghost-white" title="Seguir leyendo" <?php echo !empty($slide['enlace']['target']) ? 'target="_blank"' : ''; ?>>Seguir leyendo</a>
												</div>
											<?php endif; ?>
										</div>
									</article>
								</div>
								<div class="gr-7 gr-6@book gr-12@medium flex-center flex-start@medium heels@medium">
									<?php if(!empty($slide['imagen'])): ?>
									<figure>
										<img src="<?php echo $slide["imagen"]; ?>" alt="Imagen de <?php echo $slide["titulo"]; ?>" class="cover-img" />
									</figure>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php $i++; endforeach; ?>
					</div>
					<div class="container container--float slider__dates gr-8 prefix-2 gr-12@book prefix-0@book font-centered no-gutter">
						<div class="slider__dates bg-white">
							<a class="slider__date slider__date--prev" data-role="slider-arrow" data-direction="prev"></a>
							<?php
							$i=0;
							foreach($data_section['slides'] as $slide):
								?>
								<a class="slider__date <?php if($i==0): ?>slider__bullet--current<?php endif; ?>" data-role="slider-bullet" data-target="<?php echo $i; ?>"><?php echo $slide["titulo"]; ?></a>
								<?php $i++; endforeach; ?>
								<a class="slider__date slider__date--next" data-role="slider-arrow" data-direction="next"></a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>
