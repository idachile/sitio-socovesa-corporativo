<?php
function get_persona_box($pid, $is_main = false){ $persona = get_post($pid);?>
<div class="gr-6 gr-12@small <?php echo $is_main ? 'prefix-3 suffix-3 prefix-0@small suffix-0@small' : '';?>">
	<article class="box box--people medium-height">
		<?php if(has_post_thumbnail($pid)): ?>
		<figure class="box__figure"><?php echo get_the_post_thumbnail($pid, 'avatar_3x4', array('class' => 'cover-img')); ?></figure>
		<?php endif; ?>
		<div class="box__body">
			<?php if(get_field('cargo', $pid)) echo '<p class="box__meta">'.get_field('cargo', $pid).'</p>'; ?>
			<?php echo '<h4 class="box__title">'.get_the_title($pid).'</h4>'; ?>
			<?php if(get_field('rut', $pid)) echo '<p class="box__meta" style="margin: .5rem 0 .75rem; font-size: 1rem;">'.get_field('rut', $pid).'</p>'; ?>
			<?php if(has_excerpt($pid)):?>
			<div class="box__excerpt"><?php echo apply_filters('the_content', get_the_excerpt($pid)); ?></div>
			<?php else: ?>
			<div class="box__excerpt"><?php echo apply_filters('the_content', cut_string_to($persona->post_content, 240)); ?></div>
			<?php endif; ?>
			<div class="box__action">
				<a href="<?php echo get_permalink($pid); ?>" class="button button--main" title="Seguir leyendo">Seguir leyendo</a>
			</div>
		</div>
	</article>
</div>
<?php } ?>

<?php
global $data_section;
if(!empty($data_section['principal']) || !empty($data_section['personas'])):
?>
<section class="horizon horizon--float " data-equalize="target" data-mq="tablet-down" data-eq-target=".box__body">
	<div class="container no-gutter@book">
		<div class="row">
			<?php if(!empty($data_section['principal'])) echo get_persona_box($data_section['principal'][0], true); ?>
			<?php if(!empty($data_section['personas'])):
				foreach($data_section['personas'] as $persona_id):
					echo get_persona_box($persona_id);
				endforeach;
			endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>