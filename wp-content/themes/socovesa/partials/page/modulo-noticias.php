<?php
global $data_section;
$post__in = $data_section['noticias_relacionadas']?$data_section['noticias_relacionadas']:false;
$args = array(
   'post_type' =>  'post',
   'post_status' =>  'publish',
   'paged' =>  false,
   'posts_per_page' => 5,
   'tax_query' => false,
   'meta_query' => false,
   'orderby' => 'date',
   'order' => 'DES',
   'post__in' => $post__in,
   'post__not_in'=>false,
   'offset' => false,
   'author'=>false
);
$q = new WP_Query($args);
$i=0;
?>
<section id="nine" class="horizon horizon--float">
  <div class="container">
    <div class="row heels-small">
      <div class="gr-8 gr-10@book gr-12@tablet gr-centered">
        <h2 class="horizon__title title-center">Noticias</h2>
      </div>
    </div>
    <div class="row">
    <?php  if( $q->have_posts() ): while( $q->have_posts() ): $q->the_post(); ?>
    <?php  if( $i==0 ): ?>
      <div class="gr-12 brc-bottom">
        <article class="box box--post box--post--main">
          <div class="box__head">
            <div class="box__date">
              <span class="box__date__day"><?php echo get_the_date('d')  ?></span>
              <span class="box__date__month"><?php echo get_the_date('M')  ?></span>
            </div>
          </div>
          <div class="box__body">
            <h3 class="box__title"><a href="<?php echo get_permalink($q->post->ID) ?>"><?php echo get_the_title($q->post->ID) ?></a></h3>
            <div class="box__excerpt">
              <?php echo get_field('bajada'); ?>
            </div>
          </div>
          <div class="box__action flex-right flex-middle hide@tablet">
            <a href="<?php echo get_permalink($q->post->ID) ?>" class="button button--main">Seguir leyendo</a>
          </div>
        </article>
      </div>
    <?php else: ?>
      <div class="gr-6 gr-12@tablet no-gutter-bottom@tablet">
        <article class="box box--post">
          <div class="box__body">
            <p class="box__meta"><?php echo get_the_date('d \d\e F');  ?></p>
            <h4 class="box__title"><a href="<?php echo get_permalink($q->post->ID) ?>"><?php echo get_the_title($q->post->ID) ?></a></h4>
            <div class="box__action">
              <a href="<?php echo get_permalink($q->post->ID) ?>" class="link">Ver más</a>
            </div>
          </div>
        </article>
      </div>
    <?php endif; ?>
    <?php $i++; endwhile; endif; ?>
    </div>
    <div class="flex-center hat">
      <a href="#" class="button button--ghost-dark">Ver más noticias</a>
    </div>
  </div>
</section>
