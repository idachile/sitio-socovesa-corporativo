<?php global $data_section; ?>
<section class="horizon horizon--normal" data-role="filter-horizon" <?php echo $data_section['habilitar_filtro'] ? ' data-group-year="'.$data_section['ano_agrupador'].'"' : 'data-group-year="'.str_replace('año ', '', strtolower($data_section['ano']).'"'); ?>>
	<div class="container">
		<div class="row">
			<h2 class="title gutter-left-double@desktop"><?php echo $data_section['ano'] ?></h2>
			<table class="box__table">
				<thead class="box__table--head bg-main-lightest">
					<tr>
						<th class="font-color-main-regular text-upper w-bold" width="25%">
							<?php echo $data_section['mes_o_ano']?></th>
						<th class="font-color-main-regular text-upper w-bold" width="50%">Documento</th>
						<th class="font-color-main-regular text-upper w-bold" width="25%">Descargar</th>
					</tr>
				</thead>
				<tbody class="box__table--body">
					<?php $filas = $data_section['filas']; ?>
					<?php if(!empty($filas)): ?>
					<?php foreach($filas as $fila): ?>
					<tr class="box__table box__table__row" data-role="filter-row" data-group-month="<?php echo strtolower($fila['fecha']);  ?>"
					data-group-monthnum="<?php echo get_data_monthnum( strtolower($fila['fecha']) );  ?>" <?php echo !empty($fila['tipo']) ? 'data-group-tipoid="'.$fila['tipo']->term_id.'" data-group-tipo="'.$fila['tipo']->name.'"' : ''; ?>>
						<td class="box__head text-upper w-bold">
							<?php echo $fila['fecha'] ?>
						</td>
						<td>
							<h4 class="box__title no-margin-bottom"><?php echo $fila['titulo'] ?></h4>
							<div class="box__excerpt">
								<?php echo $fila['documento'] ?>
							</div>
						</td>
						<?php
						$archivo = $fila['archivo'];
					
					if(!empty($archivo)):
                  $archivosb = 'doc';
                  if( $archivo['subtype'] != 'doc' ) {
                  // if( $archivo['subtype'] != 'doc' && $archivo['subtype'] != 'docx' && strlen($archivo['subtype']) < 10 ) {
                    $archivosb = $archivo['subtype'];
                  }
                ?>
						<td class="box__meta">
							<a href="<?php echo $archivo['url'] ?>" class="box__meta__title show box__meta__content box__meta__content__<?php echo $archivosb ?>" title="Descargar documento" download target="_blank"><strong class="link-black">Descargar
									<?php echo $archivosb ?></strong></a>
							<span class="box__meta__details"><?php echo convert_size( $archivo['filesize']  )?></span>
						</td>
					<?php endif; ?>
					</tr>
					<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>