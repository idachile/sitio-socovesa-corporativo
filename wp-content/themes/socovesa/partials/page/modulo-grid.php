<?php global $data_section; ?>
<section class="horizon horizon--simple bg-tall">
   <div class="container heels-small">
      <div class="row">
         <div class="gr-8 gr-10@book gr-12@tablet gr-centered">
            <h2 class="horizon__title title-center"><?php echo $data_section["titulo"]; ?></h2>
            <div class="horizon__excerpt text-center">
               <?php echo $data_section["bajada"]; ?>
            </div>
         </div>
      </div>
   </div>
   <?php if(!empty($data_section['bloques'])): $i=1; ?>
   <div class="container main no-gutter" data-rellax data-rellax-speed="-1" data-rellax-percentage="0.4">
      <div class="grid grid-cols-5 grid-cols-4@medium grid-cols-2@tablet grid-cols-1@small">
      <?php foreach($data_section["bloques"] as $bloques ): ?>
         <div class="grid-item" data-grid-num="<?php echo $i; ?>">
            <article class="box box--grid box--expand" data-expand="<?php echo $bloques['data_expand']; ?>"
               <?php if($bloques['data_expand_tablet']): ?>data-expand-tablet="<?php echo $bloques['data_expand_tablet'] ?>"
               <?php endif; ?>
               <?php if($bloques['data_expand_medium']): ?>data-expand-medium="<?php echo $bloques['data_expand_medium'] ?>"
               <?php endif; ?> <?php if($bloques['imagen']): ?>
               data-bg="background-image: url('<?php echo $bloques['imagen']; ?>')" <?php endif; ?>>
               <div class="box__head__veil hover <?php if($bloques['imagen']) echo 'darker' ?>"></div>
               <?php if($bloques['imagen']==""): ?>
               <div class="box__head <?php echo $bloques['box_color']; ?>">
                  <h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
               </div>
               <?php endif; ?>
               <div class="box__body <?php echo $bloques['box_color']; ?>">
                  <h3 class="box__title"><?php echo $bloques['titulo']; ?></h3>
                  <?php if($bloques['bajada']): ?>
                  <div class="box__excerpt">
                     <?php echo $bloques['bajada']; ?>
                  </div>
                  <?php endif; ?>
                  <?php if(!empty($bloques['enlace'])): ?>
                  <div class="box__action">
                     <a href="<?php echo $bloques['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $bloques['enlace']['title']; ?>" <?php echo !empty($bloques['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloques['enlace']['title']; ?></a>
                  </div>
                  <?php endif; ?>
               </div>
            </article>
         </div>
         <?php $i++; endforeach; ?>
      </div>
   </div>
   <?php endif; ?>
</section>
