<?php global $data_section;
if(!empty($data_section['bloques'])):
   $i=1;
   foreach($data_section["bloques"] as $bloque ):
      if($bloque["alineacion"]=="left"):
         $alingL ='gr-6 gr-12@tablet prefix-6 prefix-0@tablet no-gutter';
         $alingR ='gr-7 gr-12@tablet no-gutter@tablet';
      elseif($bloque["alineacion"]=="right"):
         $alingL ='gr-6 gr-12@tablet prefix-0@tablet no-gutter';
         $alingR ='gr-7 gr-12@tablet prefix-5 no-gutter@tablet prefix-0@tablet';
      endif;
?>

<section id="seven" class="horizon horizon--float" data-equalize="target" data-mq="false" data-eq-target="[data-eq]"
   data-horizon>
   <div class="container container--float no-gutter">
      <div class="row">
         <div class="<?php echo $alingL; ?>">
            <figure class="horizon__bg" style="background-image: url(<?php  echo $bloque['imagen']; ?>)" data-eq>
            </figure>
         </div>
      </div>
   </div>
   <div class="container full-height margin--top no-gutter@tablet">
      <div class="row relative" data-eq>
         <div class="<?php echo $alingR; ?>">
            <article class="box box--float box--float--left bg-white">
               <div class="box__body">
                  <h2 class="box__title title-decorated"><?php echo $bloque["titulo"] ?></h2>
                  <div class="box__excerpt">
                     <?php echo $bloque["bajada"]; ?>
                  </div>
                  <?php if(!empty($bloque['enlace'])): ?>
                  <div class="box__action hat-small">
                     <a href="<?php echo $bloque['enlace']['url']; ?>" class="button <?php echo $bloque['color_link'] ?>" title="<?php echo $bloque['enlace']['title']; ?>" <?php echo !empty($bloque['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $bloque['enlace']['title']; ?></a>
                  </div>
                  <?php endif; ?>
						<?php if(!empty($bloque['archivo'])): ?>
						<div class="box__action">
							<div class="minidownload">
								<a href="<?php echo $bloque['archivo']['url'] ?>" class="box__meta__download" download>
									<h4 class="box__meta__title show"><?php echo $bloque['archivo']['title'] ?></h4><span class="box__meta__details" style="padding-left:0;"><?php echo convert_size($bloque['archivo']['filesize']); ?></span>
								</a>
							</div>
						</div>
                  <?php endif; ?>
               </div>
            </article>
         </div>
      </div>
   </div>
</section>
<?php $i++;
   endforeach;
endif; ?>