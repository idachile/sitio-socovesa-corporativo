<?php global $imagenes, $post; ?>
<section class="horizon" data-horizon>
   <div class="container no-gutter@small">
      <h2 class="horizon__title gutter-horizontal@small">Galería de imágenes</h2>
      <div class="slider slider--single hat-small" data-module="slider" data-transition="false">
         <div class="slider__items" data-role="slider-list">
            <?php $i=0; foreach($imagenes as $imagen): ?>
            <div class="slider__slide <?php if($i==0): ?>current<?php endif;?>" data-role="slider-slide">
               <figure class="slider__figure">
                  <img src="<?php echo $imagen['sizes']['dossier_1280x700'] ?>" class="cover-img hide@tablet" alt="<?php echo $imagen['alt'] ?>" />
                  <img src="<?php echo $imagen['sizes']['medium_640x360']  ?>" class="cover-img show@tablet hide@phablet" alt="<?php echo $imagen['alt'] ?>" />
                  <img src="<?php echo $imagen['sizes']['medium_640x360']  ?>" class="cover-img show@phablet" alt="<?php echo $imagen['alt'] ?>" />
               </figure>
            </div>
            <?php $i++;endforeach; ?>
         </div>
         <div class="slider__arrows">
            <button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>
            <button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>
         </div>
         <div class="slider__bullets">
            <?php $i=0; foreach($imagenes as $imagen): ?>
            <button class="slider__bullet <?php if($i==0): ?>slider__bullet--current<?php endif;?>" data-role="slider-bullet" data-target="<?php echo $i; ?>"></button>
            <?php $i++;endforeach; ?>
         </div>
      </div>
   </div>
</section>
