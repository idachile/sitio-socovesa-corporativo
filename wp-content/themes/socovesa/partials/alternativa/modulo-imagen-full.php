<?php global $data_section; ?>
<section class="horizon horizon--normal">
	<div class="container">
		<?php if(!empty($data_section['titulo'])): ?>
		<h2 class="horizon__title heels"><?php echo $data_section['titulo']; ?></h2>
		<?php endif; ?>
		<figure class="horizon__figure">
			<?php echo !empty($data_section['enlace']) ? '<a href="'.$data_section['enlace']['url'].'" class="ghost-link" title="'.$data_section['enlace']['title'].'" target="'.$data_section['enlace']['target'].'"></a>' : '' ?>
			<?php if(!empty($data_section['imagen'])): ?>
			<img src="<?php echo $data_section['imagen'] ?>" alt="Imagen de <?php echo $data_section['titulo']; ?>" class="cover-img hide@tablet" />
			<img src="<?php echo $data_section['imagen_mobile'] ?>" alt="Imagen de <?php echo $data_section['titulo']; ?>" class="cover-img show@tablet" />
			<?php else: ?>
			<img src="<?php echo $data_section['imagen'] ?>" alt="Imagen de <?php echo $data_section['titulo']; ?>" class="cover-img" />
			<?php endif; ?>
		</figure>
	</div>
</section>