<?php
global $data_section;
?>

<?php if(!empty($data_section['titulo']) || !empty($data_section['bajada'])): ?>
<section class="horizon horizon--float">
	<div class="container">
		<div class="row">
			<div class="gr-6 gr-12@tablet">
				<?php if(!empty($data_section['titulo'])): ?>
				<h2 class="horizon__title"><?php echo $data_section['titulo']; ?></h2>
				<?php endif; ?>
				<?php if(!empty($data_section['bajada'])): ?>
				<div class="horizon__excerpt"><?php echo apply_filters('the_content', $data_section['bajada']); ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<?php if(!empty($data_section['contenido_tabla'])): $tabla_acf = $data_section['contenido_tabla']; $last_row = array(); ?>
<section class="horizon horizon--normal" data-role="filter-horizon" <?php echo $data_section['habilitar_filtro'] ? ' data-group-year="'.$data_section['ano_agrupador'].'"' : 'data-group-year="'.str_replace('año ', '', strtolower($data_section['ano']).'"'); ?>>
	<div class="container">
		<div class="heels-big">
			<?php if(!empty($data_section['ano'])): ?>
			<h2 class="title gutter-left-double@desktop"><?php echo $data_section['ano']; ?></h2>
			<?php endif; ?>
			<table class="tablepress tablepress--fluid tablepress--contraste">
			<?php
			if(!empty($tabla_acf['header'])):
				echo '<thead><tr>';
				foreach($tabla_acf['header'] as $th):
					echo '<th>'.$th['c'].'</th>';
				endforeach;
				echo '</tr></thead>';
			endif;

			if($data_section['ultima_fila_footer']):
				$last_row = end($tabla_acf['body']);
			endif;

				if(!empty($tabla_acf['body'])):
					echo '<tbody>';
					foreach($tabla_acf['body'] as $tr):
						if($tr !== $last_row):
							echo '<tr>';
							foreach($tr as $td):
								echo '<td>'.$td['c'].'</td>';
							endforeach;
							echo '</tr>';
						endif;
					endforeach;
					echo '</tbody>';

					if(!empty($last_row)):
						echo '<tfoot>';
								echo '<tr>';
								foreach($last_row as $td):
									echo '<td>'.$td['c'].'</td>';
								endforeach;
								echo '</tr>';
						echo '</tfoot>';
					endif;
				endif;
			?>
			</table>
		</div>
	</div>
</section>
<?php endif; ?>
