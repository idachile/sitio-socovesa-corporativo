<?php global $data_section;
$color_graph_1 = '#fefefe';
$color_graph_2 = '#8E8E8E,#9C9C9C';
$color_graph_3 = '#8E8E8E';
// if($data_section['color_box'] == 'bg-main' || $data_section['color_box'] == 'bg-light-main'):
if(strpos($data_section['color_box'], 'main') !== false):
	$color_graph_1 = '#FBFEFF';
	$color_graph_2 = '#4AC6E6,#90DAED';
	$color_graph_3 = '#4AC6E6';
endif;

$data_section['color_box'] !== '' ? $data_section['color_box'] = $data_section['color_box'] : $data_section['color_box'] = 'bg-dark';

if(strpos($data_section['color_box'], 'light') == false || strpos($data_section['color_box'], 'clear')):
	$t_light = '';
	$t_dark = '-dark';
else:
	$t_light = 'text-light';
	$t_dark = '';
endif;
?>

<section class="horizon horizon--marcas" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
	<div class="container container--float-full no-gutter" data-eq>
		<div class="row">
			<div class="gr-6 gr-12@tablet no-gutter <?php echo $data_section['alineacion'] !== 'left' ? 'prefix-6 prefix-0@tablet' : ''; ?>">
				<div class="float__square bg-transparent" data-eq></div>
			</div>
		</div>
	</div>
	<div class="container full-height">
		<div class="row <?php echo $data_section['alineacion'] !== 'left' ? 'row-reverse col-reverse@tablet' : ''; ?>">
			<div class="gr-5 gr-5@book gr-12@tablet flex-center">

				<article class="box box--simple ">
					<div class="box__body">
						<h2 class="box__title title-decorated-dark"><?php echo $data_section['titulo']; ?></h2>
						<div class="box__excerpt">
							<?php  echo $data_section['bajada']; ?>
						</div>
						<?php if(!empty($data_section['enlace']['url'])): ?>
						<div class="box__action">
							<a href="<?php echo $data_section['enlace']['url']; ?>" class="button button--ghost-white" title="<?php echo $data_section['enlace']['title']; ?>" <?php echo !empty($data_section['enlace']['target']) ? 'target="_blank"' : ''; ?>><?php echo $data_section['enlace']['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</article>
			</div>
			<div class="gr-7 gr-12@tablet flex-center <?php echo $data_section['alineacion'] == 'left' ? 'prefix-1 prefix-0@book' : ''; ?>">
				<div class="box box--graph box--graph--medium <?php echo strpos($data_section['color_box'], 'main') !== false ? 'bg-clear-main' : 'bg-clear-grey'; ?>" data-maxwidth="500">
				<?php if($data_section['grafico_tipo'] == 'columna'): ?>
					<div id="chart-<?php  echo sanitize_title($data_section['titulo']); ?>" data-role="column-chart" data-bg="<?php echo $color_graph_1; ?>" data-color="<?php echo $color_graph_2; ?>" data-points="<?php  echo $data_section['datos_json']; ?>" data-title="<?php echo $data_section['titulo_grafico']; ?>" data-axisy-izq="<?php echo $data_section['titulo_axis_y_izq']; ?>" data-axisy-der="<?php echo $data_section['titulo_axis_y_der']; ?>" data-intervalo-x="<?php echo $data_section['intervalo_x']; ?>" data-intervalo-y="<?php echo $data_section['intervalo_y']; ?>" data-intervalo-y2="<?php echo $data_section['intervalo_y2']; ?>"></div>
				<?php elseif($data_section['grafico_tipo'] == 'circular'): ?>
					<div id="chart-<?php echo sanitize_title($data_section['titulo']); ?>"
						data-role="doughnout-chart" data-bg="<?php echo $color_graph_1; ?>" data-percentage="<?php echo $data_section['porcentajes']; ?>" data-colors="<?php  echo $data_section["colores_graficos"]; ?>"
						data-points="<?php  echo $data_section['datos_json']; ?>" data-title="<?php echo $data_section['titulo_grafico']; ?>"></div>
				<?php else: ?>
					<div id="chart-<?php  echo sanitize_title($data_section['titulo']); ?>"
						data-role="spline-area-chart" data-bg="<?php echo $color_graph_1; ?>" data-colors="<?php echo $color_graph_2; ?>"
						data-points="<?php  echo $data_section['datos_json']; ?>" data-title="<?php echo $data_section['titulo_grafico']; ?>" data-axisy-izq="<?php echo $data_section['titulo_axis_y_izq']; ?>" data-axisy-der="<?php echo $data_section['titulo_axis_y_der']; ?>" data-intervalo-x="<?php echo $data_section['intervalo_x']; ?>" data-intervalo-y="<?php echo $data_section['intervalo_y']; ?>" data-intervalo-y2="<?php echo $data_section['intervalo_y2']; ?>"></div>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>