<?php require_once('_meta-header.php'); global $post; ?>
<header id="nav" class="header-bar">
	<section class="nav-bar" data-module="nav-bar" data-offtop="120" data-stopper="[data-role='stopper']">
		<div class="container">
			<div class="nav-bar__holder">
				<div class="nav-bar__mobile-head">
					<button class="nav-bar__mobile-menu show@medium" aria-label="Ver menú" data-role="nav-deployer">
						<span></span>
					</button>
					<div class="nav-bar__brand">
						<a class="app-brand" title="Ir a " href="/">
							<img src="<?php the_field('logo_principal', 'options'); ?>" class="app-brand__logo" alt="Logo Socovesa blanco" />
						</a>
					</div>
					<button class="nav-bar__mobile-search show@medium" aria-label="Búsqueda" data-role="search-deployer">
						<span></span>
					</button>
				</div>
				<div class="nav-bar__body" data-role="nav-body">
					<div class="nav-bar__menu-holder">
						<?php
						$main_nav  = '<ul id="%1$s" class="%2$s">';
						$main_nav .= 	'%3$s';
						$main_nav .= '<li class="menu-item search hide@medium">';
						$main_nav .= '<button class="icon icon-search no-touch" data-touch="false" title="Búsqueda" data-role="search-deployer"></button>';
						$main_nav .= '</li>';
						$main_nav .= '</ul>';
						if(get_post_type($post) == 'en_page'):
							$menu_id = 'main-menu';
						else:
							$menu_id = 'menu-principal';
						endif;
						wp_nav_menu(array(
							'menu'              => $menu_id,
							'theme_location'    => $menu_id,
							'menu_class'        => "nav-bar__menu",
							'menu_id'           => "principal",
							'container'         => false,
							'container_class'   => false,
							'container_id'      => false,
							'walker'            => new MainWalker(),
							'items_wrap' =>     $main_nav,
							'fallback_cb'       => false
						));
						?>
						<div class="nav-bar__internal show@medium" data-area-name="mobile-aux"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="nav-bar__search" data-role="search-body">
		<div class="container no-gutter@medium">
			<form action="<?php echo home_url(); ?>" method="get" class="search-module">
				<input type="text" class="search-input" id="s" name="s" placeholder="Buscar" tabindex="1">
				<button class="search-button no-touch" title="Ver resultados de búsqueda" data-touch="false" tabindex="2"></button>
			</form>
		</div>
	</div>
	<?php
	//Páginas de contacto y exito/error
	//361, 1155, 1157, 75, 78, 79
	if (isset($post) && !empty($post)) :

		$parent_id = $post->post_parent;
		$parent_post = get_post($post->post_parent);
		if ($post->post_parent && $post->post_parent !== 361 && $post->post_parent !== 75 && !is_search() && !is_404()) :
			if (!$parent_post->post_parent) :
				echo get_siblings_navigation($post->ID, $post->post_parent);
			else :
				echo get_siblings_navigation($parent_id, $parent_post->post_parent);
			endif;
		elseif (has_children($post->ID) && $post->ID !== 361 && $post->ID !== 75 && !is_search() && !is_404()) :
			echo get_siblings_navigation($post->ID, $post->ID);
		elseif (is_singular('persona')) :
			$pertenencia = wp_get_post_terms($post->ID, 'pertenencia');
			$filial = wp_get_post_terms($post->ID, 'filial');
			if (!empty($pertenencia)) :
				$pertenencia_slug = $pertenencia[0]->slug;
				$pertenencia_page = get_page_by_path('/equipo/' . $pertenencia_slug);
				echo get_siblings_navigation($pertenencia_page->ID, $pertenencia_page->post_parent);
			elseif (!empty($filial)) :
				$filial_slug = $filial[0]->slug;
				$filial_page = get_page_by_path('/equipo/equipo-' . $filial_slug);
				echo get_siblings_navigation($filial_page->ID, $filial_page->post_parent);
			endif;
		endif;

	endif;
	?>
</header>