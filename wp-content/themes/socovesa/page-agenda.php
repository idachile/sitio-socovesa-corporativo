<?php
/*
*Template name: Página Agenda de eventos
* Template Post Type: page, en_page
*/
global $post, $pid, $fecha_sel;
$fecha_sel = '';
if(isset($_GET['fecha'])):
	$fecha_sel = $_GET['fecha'];
endif;
get_header();
the_post();

$pid = get_the_ID();
?>
<main id="top">
<?php
$secciones = '';
$secciones = get_field('secciones', $pid);
global $data_section;
if(isset($secciones) && !empty($secciones)):
	foreach( $secciones as $data_section ):
		get_template_part('partials/agenda/'. str_replace('_', '-', $data_section['acf_fc_layout']));
	endforeach;
endif;
?>
</main>
<?php
$post = '';
$post = get_post($pid);
if(isset($post) && $post->post_parent !== 0):
	if($post->post_parent == 12 || get_post($post->post_parent)->post_parent == 12):
		echo load_template_part('partials/inversionista/kit','inversionista');
	endif;
endif;
?>
<?php get_footer(); ?>