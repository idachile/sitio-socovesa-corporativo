<?php
/*
* Template Name: Página marcas
* Template Post Type: en_page, page
*/
get_header();
the_post();
global $post,$pid;
$pid = get_the_ID();
?>
<main id="top">
<?php
	$secciones = '';
	$secciones = get_field('secciones', $pid);
	global $data_section;
	if( isset($secciones) && !empty($secciones)):
		foreach( $secciones as $data_section ){
			$title = str_replace('_', '-', $data_section['acf_fc_layout']);
			get_template_part('partials/marcas/modulo-'. $title);
		}
	endif;
?>
</main>
<?php get_footer(); ?>