$(document).ready(function () {
	console.log('uncollapse');
	jQuery.each(jQuery('[data-role="collapseHorizonte"]:not(.active)'), function () {
		var $selector = $(this),
			$parent = $selector.parent(),
			$selector_id = $selector.data('id');

		$selector.on('click', function(event){
			// $selector.siblings().removeClass('active');
			$parent.siblings().removeClass('active');
			// $parent.siblings().find('.repo-sidebar__submenu__item').removeClass('active');
			// $parent.siblings().find('[data-role="collapseDocumento"]').removeClass('active');
			$parent.toggleClass('active');
			$selector.toggleClass('active');
			
			// jQuery.each(jQuery('[data-role="pagina-documento"]'), function (e) {
			// 	var $pagina = $(this);

			// 	if($pagina.data('pagina-id') == $selector_id){
			// 		$pagina.addClass('active').removeClass('hide');
			// 		$pagina.children().addClass('active').removeClass('hide');
			// 	}else{
			// 		$pagina.addClass('hide').removeClass('active');
			// 		$pagina.children().addClass('hide').removeClass('active');
			// 	}
			// });

			event.preventDefault();
		});
	});

	jQuery.each(jQuery('[data-role="collapseDocumento"]'), function () {
		var $selector = $(this),
			$parent = $selector.parent(),
			$selector_id = $selector.data('id');

		$selector.on('click', function(event){

			$parent.siblings().removeClass('active');
			$parent.siblings().find('[data-role="collapseDocumento"]').removeClass('active');
			$selector.toggleClass('active');
			$parent.toggleClass('active');
			
			jQuery.each(jQuery('[data-role="horizonte-documento"]'), function (e) {
				var $horizonte = $(this);
				
				if($horizonte.data('horizonte-id') == $selector_id){
					$horizonte.addClass('active').removeClass('hide');
					$horizonte.parent().addClass('active').removeClass('hide');
					$horizonte.parent().siblings().removeClass('active').addClass('hide');
				}else{
					$horizonte.addClass('hide');
				}
			});

			event.preventDefault();
		});
	});
});