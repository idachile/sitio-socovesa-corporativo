$(document).ready(function () {
	var splinerea_options = '';

	jQuery.each(jQuery('[data-role="spline-area-chart"]'), function (e) {
		var $this = $(this),
			chart_id = $this.attr('id'),
			chart_colors = $this.data('colors') || '',
			chart_bg_color = $this.data('bg') || '',
			chart_datapoints_content = $this.data('points'),
			chart_title = $this.data('title') || '',
			chart_axisy_title_izq = $this.data('axisy-izq') || '',
			chart_axisy_title_der = $this.data('axisy-der') || '',
			chart_intervalo_x = $this.data('intervalo-x') || 0,
			chart_intervalo_y = $this.data('intervalo-y') || 0,
			chart_intervalo_y_2 = $this.data('intervalo-y2') || 0,
			colors_array = {},
			colors_id = '';

			$.getJSON( chart_datapoints_content, function() {
				// console.log('success: spline area chart');
			}).done(function(jsonData){
				const data_object = {};
				var json_length = Object.keys(jsonData).length;
				var cont = 0;
				var axis_type = 'primary';
				
				if (chart_colors !== '') {
					colors_array = chart_colors.split(',');
					colors_id = chart_id + '_colors';
					CanvasJS.addColorSet(colors_id, colors_array);
				}

		
				if (jsonData) {
					jQuery.each(jsonData, function (name, data) {
						if(json_length > 1 && ((json_length - 1) == cont)){
							axis_type = 'secondary';
						}

						data_object[cont] = {
							type: "line",
							name: name,
							markerSize: 0,
							markerType: "circle",
							showInLegend: true,
							axisYType: axis_type,
							dataPoints: data
						};
						cont++;
					});

					var data_array = Object.keys(data_object).map(function (key) {
						return data_object[key];
					});
			
					splinerea_options = {
						backgroundColor: chart_bg_color,
						colorSet: colors_id,
						animationEnabled: true,
						theme: "light2",
						axisX: {
							interval: chart_intervalo_x,
							labelAngle: -45
						},
						axisY: {
							valueFormatString: "#0",
							gridThickness: 1,
							titleFontColor: "#808080",
							title: chart_axisy_title_izq,
							titleFontSize: 12,
							interval: chart_intervalo_y,
						},
						axisY2: {
							valueFormatString: "#0",
							gridThickness: 1,
							titleFontColor: "#808080",
							title: chart_axisy_title_der,
							titleFontSize: 12,
							interval: chart_intervalo_y_2,
						},
						title: {
							text: chart_title
						},
						legend: {
							cursor: "pointer",
							horizontalAlign: "left",
							verticalAlign: "top",
							itemclick: toogleDataSeries
						},
						data: data_array
					};
					$('#' + chart_id).CanvasJSChart(splinerea_options);
				}
			}).fail(function (error) {
				console.log(error);
				$this.remove();
				$this.parents('.box__graphics').remove();
			});

		//toggle chart on title click
		function toogleDataSeries(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			} else {
				e.dataSeries.visible = true;
			}
			e.chart.render();
		}
	});
});