$(document).ready(function () {
	var doughnout_options = '';

	jQuery.each(jQuery('[data-role="doughnout-chart"]'), function (e) {
		var $this = $(this),
			chart_id = $this.attr('id'),
			chart_colors = $this.data('colors') || '',
			chart_bg_color = $this.data('bg') || '',
			chart_datapoints_content = $this.data('points'),
			chart_percentage = $this.data('percentage') || '<b>{name}:</b> {y}',
			chart_title = $this.data('title') || '',
			colors_array = {},
			colors_id = '',
			data_array = [];

		$.getJSON(chart_datapoints_content, function () {
			// console.log('success: doughnout chart');
		}).done(function (jsonData) {
			if (chart_colors !== '') {
				colors_array = chart_colors.split(',');
				colors_id = chart_id + '_colors';
				CanvasJS.addColorSet(colors_id, colors_array);
			}

			if(jsonData){
				jQuery.each(jsonData, function (key, value) {
					data_array = value
				});

				doughnout_options = {
					backgroundColor: chart_bg_color,
					colorSet: colors_id,
					animationEnabled: true,
					theme: "light2",
					legend: {
						horizontalAlign: "right",
						verticalAlign: "center"
					},
					title: {
						text: chart_title
					},
					data: [{
						type: "doughnut",
						startAngle: 270,
						innerRadius: 60,
						showInLegend: true,
						markerSize: 20,
						toolTipContent: chart_percentage,
						indexLabel: "",
						cursor: "pointer",
						dataPoints: data_array
					}]
				};

				$('#' + chart_id).CanvasJSChart(doughnout_options);
			}
		});
	});
});