$(document).ready(function () {
	var column_options = '';

	jQuery.each(jQuery('[data-role="column-multitype"]'), function (e) {
		var $this = $(this),
			chart_id = $this.attr('id'),
			chart_title = $this.data('title') || '',
			chart_bg_color = $this.data('bg') || '',
			chart_axisy_title_izq = $this.data('axisy-izq') || '',
			chart_axisy_title_der = $this.data('axisy-der') || '',
			chart_intervalo_x = $this.data('intervalo-x') || 0,
			chart_intervalo_y = $this.data('intervalo-y') || 0,
			chart_intervalo_y_2 = $this.data('intervalo-y2') || 0,
			chart_datapoints_content = $this.data('points'),
			$parent = $($this.parent('div')),
			$parent_width = $($this.parent('div')).data('maxwidth'),
			$window_width = $(window).width(),
			maxwidth = $parent_width || 600;

		$(window).on('resize', function () {
			$window_width = $(window).width();
		});

		if ($window_width <= 850) {
			maxwidth = $parent.data('maxwidth-medium') || 600;
		}

		if ($window_width <= 768) {
			maxwidth = $parent.data('maxwidth-tablet') || maxwidth;
		}

		if ($window_width <= 500) {
			maxwidth = 300;
		}

		$.ajax({
			url: chart_datapoints_content,
			dataType: 'json'
		}).done(function (jsonData) {
			column_options = {
				width: maxwidth,
				culture: "es",
				animationEnabled: true,
				backgroundColor: chart_bg_color,
				theme: "light2",
				title: {
					text: chart_title,
					fontSize: 16,
					margin: 16
				},
				axisX: {
					interval: chart_intervalo_x,
					labelAngle: -45
				},
				axisY: {
					title: chart_axisy_title_izq,
					titleFontColor: "#808080",
					titleFontSize: 12,
					interval: chart_intervalo_y,
					gridThickness: 1
				},
				axisY2: {
					title: chart_axisy_title_der,
					titleFontColor: "#808080",
					titleFontSize: 12,
					interval: chart_intervalo_y_2,
					gridThickness: 1
				},
				data: jsonData
			};
			$('#' + chart_id).CanvasJSChart(column_options);
		}).fail(function (error) {
			$this.remove();
			$this.parents('.box__graphics').remove()
			console.log(error);
		});
	});
});