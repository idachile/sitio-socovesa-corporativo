$(document).ready(function () {

	jQuery.each(jQuery('[data-expand]'), function (e) {
		var $this = $(this),
			bg = $this.data('bg'),
			aplicar = true,
			$window_width = $(window).width(),
			height = $this.height(),
			width = $this.width(),
			expand_height = height * 2,
			expand_width = width * 2;

		//! remove on phablet
		if($window_width <= 500){
			var bg = $this.data('bg');
			$this.attr('style', bg).removeClass('box--expand');
			$this.removeAttr('data-expand data-expand-width data-expand-height data-bg');
			aplicar = false;
		}

		$(window).on('resize', function(){
			//! remove on phablet
			$window_width = $(window).width(),
			height = $this.height(),
			width = $this.width(),
			expand_height = height * 2,
			expand_width = width * 2;

			if($window_width <= 500){
				var bg = $this.data('bg');
				$this.attr('style', bg).removeClass('box--expand');
				$this.removeAttr('data-expand data-expand-width data-expand-height data-bg');
				aplicar = false;
			}else{
				aplicar = true;
			}
		});

		if(aplicar == true){
			$this.attr('style', bg);
			
			$this.mouseover( function(e){

				$this.attr('data-expand-width', expand_width + 'px');
				$this.attr('data-expand-height', expand_height + 'px');

				$($this.parent('div')).attr('data-maxwidth',  + width + 'px');
				$($this.parent('div')).attr('data-maxheight',  + height + 'px');

				$this.addClass('expanding');
				$this.attr('style', 'min-height:'+ expand_height +'px; min-width: '+ expand_width +'px;' + bg);
				$this.children('.box__body').animate({
					opacity: 1,
				}, 3);
			}).mouseleave(function(e){
				var maxwidth = $($this.parent('div')).attr('data-maxwidth'),
					maxheight = $($this.parent('div')).attr('data-maxheight');

				$this.removeClass('expanding');
				$this.attr('style', 'min-height:'+ maxheight +'; min-width: '+ maxwidth +';' + bg);
				$this.children('.box__body').animate({
					opacity: 0,
				}, 0);
			});
		}
	});

	jQuery.each(jQuery('[data-grow]'), function (e) {

		var $this = $(this),
			$all_grows = $('[data-grow]'),
			$grid_parent = $($this.parents('[data-role="parent-grid"]')),
			$close_btn = $($this.find('[data-role="close-block"]')),
			$this_head = $($this.find('.box__head'));
			
			$this_head.on('click', function(e){
				var width = $this.width(),
				// height = $this.height(),
				// gap = $grid_parent.data('gap') || 0,
				expand_width = width,
				$window_width = $(window).width();
				// common = width * $grid_parent.data('maxgrow');

				// expand_width = (width * $grid_parent.data('maxgrow')) + (gap * 2);
				expand_width = width * $grid_parent.data('maxgrow');

				$this.parents('.horizon').addClass('open');
				
				//@medium
				// if($window_width <= '850'){ expand_width = width * $grid_parent.data('maxgrow-medium') + (gap * 2); }
				if($window_width <= '850'){ expand_width = width * $grid_parent.data('maxgrow-medium'); }
				//@tablet
				// if($window_width <= '768'){ expand_width = width * $grid_parent.data('maxgrow-tablet') + (gap * 1); }
				if($window_width <= '768'){ expand_width = width * $grid_parent.data('maxgrow-tablet'); }

				if($window_width <= '500'){ expand_width = width; }

				// $($this.parent('div')).attr('style', 'min-width:' + width + 'px; height: ' + height + 'px; ');
				$($this.parent('div')).attr('style', 'min-width:' + width + 'px;');
				$($this.parent('div')).attr('data-maxwidth',  + width + 'px');
				$($this.parent('div')).addClass('open-grid');

				$(window).on('resize', function(){
					width = $(window).width();
					// expand_width = (expand_width * $grid_parent.data('maxgrow')) + (gap * 2);
					expand_width = width * $grid_parent.data('maxgrow');
					//@medium
					if($window_width <= '850'){
						// expand_width = width * $grid_parent.data('maxgrow-medium') + (gap * 2);
						expand_width = width * $grid_parent.data('maxgrow-medium');
					}
					//@tablet
					if($window_width <= '768'){
						expand_width = width * $grid_parent.data('maxgrow-tablet');
					}

					if($window_width <= '500'){
						expand_width = width;
					}

					// $($this.parent('div')).attr('style', 'min-width:' + width + 'px; height: ' + height + 'px;');
					$($this.parent('div')).attr('style', 'min-width:' + width + 'px;');
					$($this.parent('div')).attr('data-maxwidth',  + width + 'px');
					$($this.parent('div')).addClass('open-grid');
				});


				$all_grows.removeClass('growing').css('min-width', width);
				$this.addClass('growing');
				$this.animate({
					minWidth: expand_width,
					// height: 'auto',
				}, 0);
				
			});

			$close_btn.on('click', function(e){
				var normal_width = $($this.parent('div')).attr('data-maxwidth');
				var $video = $($this.find('[data-role="video-player"]')) || false;

				$($this.parent('div')).removeClass('open-grid').addClass('closed-grid');
				$this.removeClass('growing');
				$this.css('width', normal_width);
				$this.css('min-width', 0);

				$this.parents('.horizon').removeClass('open');
				
				if($video !== false){
					$video[0].pause();
				}
				
			})
	});
});