$(document).ready(function () {

	jQuery.each(jQuery('[data-touch]'), function (e) {
		var $this = $(this);

		$(window).on('resize', function () {
			var $window = $(this);

			if ($window.width() < 768) {
				$this.removeClass('no-touch').addClass('touch');
				$this.attr('data-touch', true);
			} else {
				$this.addClass('no-touch').removeClass('touch');
				$this.attr('data-touch', false);
			}
		});
	});

	jQuery.each(jQuery('.grid-item:not(.grid-item--position__unset)'), function (e) {
		var $this = $(this);

		$this.mouseover(function (e) {
			$this.addClass('opening');
		}).mouseleave(function (e) {
			$this.removeClass('opening');
		});
	});

	jQuery.each(jQuery('[data-area-name="mobile-aux"] [data-child] a'), function (e) {
		var $this = $(this);

		$this.on('click', function () {
			$('body').removeClass('nav-open');
			$('[data-role="nav-body"]').removeClass('deployed');
			$('[data-role="nav-deployer"]').removeClass('deployed');
		});
	});

	jQuery.each(jQuery('[data-role="video-player"]'), function () {
		var $this_video = $(this);
		var video = this;
		var $deployer = $($this_video.parent()).find('[data-role="video-player-deployer"]');

		$deployer.on("click", function () {
			if (video.paused == true) {
				video.play();
				$deployer.addClass('playing');
				$this_video.attr('controls', 'true');
				$('[data-role="video-prev"]').addClass('hide');
			} else {
				video.pause();
				$deployer.removeClass('playing');
			}
		});

		if (video.paused == true) {
			$deployer.addClass('check-playing');
		} else {
			$deployer.removeClass('check-playing');
		}
	});

	jQuery.each(jQuery('[data-role="ciudad-control"]:not([data-status="inactive"])'), function () {
		var $filler = $(this);
		var $parent = $filler.parents('[data-role="ciudad-parent"]');
		var $active = $($parent.find('[data-role="ciudad-control"][data-status="active"]'));
		var $siblings = $($parent.find('[data-role="ciudad-control"][data-status="preview"]'));
		var $comunas = $($parent.find('[data-role="ciudad-comuna"][data-status="preview"]'));
		var default_color = $parent.data('color-preview');
		var paint_color = $parent.data('color-active');

		$active.attr('fill', paint_color);
		$comunas.attr('fill', paint_color);
		$siblings.attr('fill', default_color);

		$filler.on("click", function () {
			$active.attr('fill', default_color).attr('data-status', 'preview').removeClass('active');
			$siblings.attr('fill', default_color).attr('data-status', 'preview').removeClass('active');
			$(this).attr('fill', paint_color).attr('data-status', 'active').addClass('active');

			var target_city = $(this).data('target');
			var $target = $($($(this).parents('[data-role="ciudad-parent"]')).find('[data-role="ciudad-detalle"][data-ciudad="' + target_city + '"]'));
			var $other = $($($(this).parents('[data-role="ciudad-parent"]')).find('[data-role="ciudad-detalle"]'));
			var $title = $($($(this).parents('[data-role="ciudad-parent"]')).find('[data-role="ciudad-title"][data-ciudad="' + target_city + '"]'));
			var $other_titles = $($($(this).parents('[data-role="ciudad-parent"]')).find('[data-role="ciudad-title"]'));

			$other.removeClass('active');
			$target.addClass('active');
			$($other_titles.parent('text')).attr('font-weight', 'normal');
			$($title.parent()).attr('font-weight', 'bold');

			var $selector = $($parent.find('[data-role="svg-selector-deployer"]'));
			var $options = $($parent.find('[data-role="svg-selector-options"]'));
			var $selector_name = $($selector.find('[data-selector-show]'));
			var $selector_message = $($selector.find('[data-selector-message]'));

			$selector_name.html(target_city).removeClass('hide');
			$selector_message.addClass('hide');

			$selector.removeClass('active');
			$options.removeClass('active');
		});
	});

	jQuery.each(jQuery('[data-role="svg-selector-deployer"]'), function () {
		var $selector = $(this);
		var $selector_show = $selector.find('[data-selector-show]');
		var $selector_message = $selector.find('[data-selector-message]');

		$selector.on("click", function () {
			var options = $(this).next('[data-role="svg-selector-options"]');

			$selector_show.toggleClass('hide');
			$selector_message.toggleClass('hide');
			$(this).toggleClass('active');
			$(options).toggleClass('active');
		});
	});

	jQuery.each(jQuery('[data-role="slider-selector-deployer"]'), function () {
		var $selector = $(this);
		var $selector_show = $selector.find('[data-selector-show]');
		var $selector_message = $selector.find('[data-selector-message]');

		$selector.on("click", function () {
			var options = $(this).next('[data-role="slider-selector-options"]');

			$selector_show.toggleClass('hide');
			$selector_message.toggleClass('hide');
			$(this).toggleClass('active');
			$(options).toggleClass('active');

			$('[data-role="slider-bullet"]').on('click', function(e){
				e.preventDefault();
				var $bullet = $(this);

				$selector_show.html($bullet.html());
				$selector_show.removeClass('hide');
				$selector_message.addClass('hide');
				$selector.removeClass('active');
				$(options).removeClass('active');
			});
		});
	});

	jQuery.each(jQuery('[data-role="divide-text"]'), function () {
		var $this = $(this),
			col_class = $this.data('col-class') || 'gr-6 gr-12@tablet',
			countext = $this.children().length,
			leftcol_qant = Math.round(countext / 2),
			contador = 0,
			left_col = '',
			col_html = '',
			right_col = '';

		if(countext > 1){
			jQuery.each(jQuery($this.children()), function () {
				var lilkid = $(this).get(0).outerHTML;
				contador++;
				if (contador <= leftcol_qant) {
					left_col += lilkid;
				} else {
					right_col += lilkid;
				}
			});

			col_html = '<div class="'+col_class+'">' + left_col + '</div>' + '<div class="'+col_class+'">' + right_col + '</div>';
			$this.html(col_html)
		};
	});

	jQuery.each(jQuery('[data-role="divide-news"]'), function () {
		var $this = $(this),
			col_class = $this.data('col-class') || 'gr-6 gr-12@tablet no-gutter',
			countext = $this.children().length,
			leftcol_qant = Math.floor(countext / 2),
			contador = 0,
			left_col = [],
			col_html = '',
			total_html = '',
			right_col = [];

		// if(countext > 1){
			jQuery.each(jQuery($this.find('[data-role="news-child"]')), function () {
				var $child = $(this);
				var lilkid = $(this).get(0).outerHTML;
				var lilkid_numb = $child.data('num');
				// contador++

				if (countext == 1 || (lilkid_numb % 2) == 0) {
					left_col += lilkid;
					// left_col.push(lilkid_numb);
				} else {
					right_col += lilkid;
					// right_col.push(lilkid_numb);
				}
				total_html += lilkid;
			});

			var window_width = $(window).width();

			console.log(window_width);

			if(window_width > 768){
				col_html = '<div class="'+col_class+'">' + left_col + '</div>' + '<div class="'+col_class+'" style="margin-top: 1rem;">' + right_col + '</div>';
				$this.html(col_html)
			}else{
				$this.html(total_html);
			}

			$(window).on('resize', function(){
				window_width = $(window).width();

				if(window_width > 768){
					col_html = '<div class="'+col_class+'">' + left_col + '</div>' + '<div class="'+col_class+'" style="margin-top: 1rem;">' + right_col + '</div>';
					$this.html(col_html)
				}else{
					$this.html(total_html);
				}
			});
		// }
	});

	jQuery.each(jQuery('[data-module="common-form"]'), function () {
		var $form = $(this),
			$children = $form.find('[name]'),
			tabindex = 0;

		jQuery.each(jQuery($children), function () {
			var $child = $(this);
			tabindex++;
			$child.attr('tabindex', tabindex);
		});

	});


	jQuery.each(jQuery('[data-selector="month"]'), function () {
		var $this = $(this),
			this_html = $this.html(),
			meses_keys = new Array();

		jQuery.each(jQuery('[data-group-month]'), function (index) {
			var $child = $(this),
				child_key = $child.data('group-monthnum');

			meses_keys[index] = child_key;
		});

		var meses_reordenados = [];

		$.each(meses_keys, function (key, value) {
			if ($.inArray(value, meses_reordenados) == -1) meses_reordenados.push(value);
		});
		meses_reordenados.sort(function (a, b) {
			return a - b
		});

		$.each(meses_reordenados, function (key, value) {
			var month_name = $('[data-group-monthnum="' + value + '"]').data('group-month');
			this_html = this_html + '<option value="' + month_name + '">' + month_name.charAt(0).toUpperCase() + month_name.slice(1) + '</option>';
		});

		$this.html(this_html);
	});

	jQuery.each(jQuery('[data-selector="year"]'), function () {
		var $this = $(this),
			this_html = $this.html(),
			years = new Array();

		jQuery.each(jQuery('[data-group-year]'), function (index) {
			var $child = $(this),
				child_value = $child.data('group-year');

			years[index] = child_value;
		});

		var anhos = [];

		$.each(years, function (key, value) {
			if ($.inArray(value, anhos) == -1) anhos.push(value);
		});

		$.each(anhos, function (key, value) {
			this_html = this_html + '<option value="' + value + '">' + value + '</option>';
		});

		$this.html(this_html);
	});

	jQuery.each(jQuery('[data-selector="tipo"]'), function () {
		var $this = $(this),
			this_html = $this.html(),
			types = new Array();


		jQuery.each(jQuery('[data-group-tipo]'), function (index) {
			var $child = $(this),
				child_value = $child.data('group-tipo');

			types[index] = child_value;
		});

		var tipos = [];

		$.each(types, function (key, value) {
			if ($.inArray(value, tipos) == -1) tipos.push(value);
		});
		tipos.reverse();

		$.each(tipos, function (key, value) {
			this_html = this_html + '<option value="' + value + '">' + value + '</option>';
		});

		$this.html(this_html);
	});


	$('[data-role="filtro-documentos"]').submit(function () {
		var $form = $(this);
		var formData = $form.serializeArray();
		var tr = '';

		$('[data-role="filter-horizon"], [data-role="filter-row"]').attr('style', '');
		$('[data-role="filter-row"]').attr('data-show', 'yes');
		$.each(formData, function (key, value) {
			if (value['value'] !== '') {
				if (value['name'] == 'year') {
					$('[data-role="filter-horizon"]:not([data-group-year="' + value['value'] + '"])').attr('style', 'display: none;');
				} else if (value['name'] == 'tipo') {
					tr = tr + '[data-group-tipo="' + value['value'] + '"]';
				} else if (value['name'] == 'month') {
					tr = tr + '[data-group-month="' + value['value'] + '"]';
				}
			}
		});

		if(tr !== ''){
			$('[data-role="filter-row"]:not(' + tr + ')').attr('style', 'display: none;');
			$('[data-role="filter-row"]:not(' + tr + ')').attr('data-show', 'no');

			$.each($('[data-role="filter-horizon"]'), function (key, value) {
				if ($(this).find('[data-show="yes"]').length <= 0) {
					$(this).attr('style', 'display: none;')
				}
			});
		}

		return false;
	});



	$('[data-role="filtro-bolsa-month"]').change(function () {
		// event.preventDefault();

		var $selector_mes = $(this),
			mes = $selector_mes.val(),
			$selector_anho = $('[data-role="filtro-bolsa-year"]'),
			anho = $selector_anho.val(),
			$selector_dias = $('[data-role="filtro-bolsa-day"]'),
			$button = $('[data-role="submit-proyecto"]'),
			current_mes = $selector_mes.data('current'),
			current_anho = $selector_anho.data('current'),
			home_url = $('body').data('home');

		if (mes !== '' && anho !== '') {
			$selector_dias.html('<option>Buscando...</option>');

			$.ajax({
				url: home_url + '/wp-json/obtener/dias-mes',
				type: 'GET',
				dataType: 'json',
				data: 'month=' + mes + '&year=' + anho + '&cmonth=' + current_mes + '&cyear=' + current_anho,
				success: function (response) {
					console.log(response);
					$selector_dias.html('<option value="" disabled selected>Seleccione día</option>' + response.data_html);
					$selector_dias.removeClass('loading').removeAttr('disabled', 'false');
					$button.removeClass('waiting');
				},
				error: function (response) {
					console.log('error');
					console.log(response);
				}
			});
		}
		if($selector_dias.val() !== ''){
			$selector_dias.removeClass('waiting');
		}else{
			$selector_dias.addClass('waiting');
		}
	});

	$('[data-role="filtro-bolsa-year"]').change(function () {
		// event.preventDefault();

		var $selector_anho = $(this),
			anho = $selector_anho.val(),
			$selector_mes = $('[data-role="filtro-bolsa-month"]'),
			mes = $selector_mes.val(),
			$selector_dias = $('[data-role="filtro-bolsa-day"]'),
			$button = $('[data-role="submit-filter"]'),
			current_mes = $selector_mes.data('current'),
			current_anho = $selector_anho.data('current'),
			home_url = $('body').data('home');

		$button.addClass('waiting');

		if (mes !== '' && anho !== '') {
			$selector_dias.html('<option>Buscando...</option>');

			$.ajax({
				url: home_url + '/wp-json/obtener/dias-mes',
				type: 'GET',
				dataType: 'json',
				data: 'month=' + mes + '&year=' + anho + '&cmonth=' + current_mes + '&cyear=' + current_anho,
				success: function (response) {
					console.log(response);
					$selector_dias.html('<option value="" disabled selected>Seleccione día</option>' + response.data_html);
					$selector_dias.removeClass('loading').removeAttr('disabled', 'false');
					$button.removeClass('waiting');
				},
				error: function (response) {
					console.log(response);
				}
			});
		}

		if(anho !== '' && anho == $selector_anho.data('current')){
			jQuery.each(jQuery('[data-role="filtro-bolsa-month"] option'), function () {
				var $this = $(this);
				if($this.val() > $('[data-role="filtro-bolsa-month"]').data('current')){
					$this.attr('style', 'display:none;');
				}
			});
		}else{
			$('[data-role="filtro-bolsa-month"] option').attr('style', '');
		}

		if($selector_dias.val() !== ''){
			$selector_dias.removeClass('waiting');
		}else{
			$selector_dias.addClass('waiting');
		}
	});

	$('[data-role="filtro-bolsa-day"]').change(function () {
		// event.preventDefault();

		var $selector_dia = $(this),
			dia = $selector_dia.val(),
			$button = $('[data-role="submit-filter"]');

		if(dia !== ''){
			$selector_dia.removeClass('waiting');
		}else{
			$selector_dia.addClass('waiting');
		}
	});



	$('[data-role="filtro-bolsa-valores"]').submit(function () {
		var $form = $(this),
		formData = $form.serializeArray(),
		home_url = $('body').data('home'),
		$opening_price = $('[data-role="opening-price"]'),
		$closing_price = $('[data-role="closing-price"]'),
		$total = $('[data-role="total"]'),
		$container = $('[data-role="valores-bolsa-container"]'),
		$error_container = $('[data-role="error-container"]');

		$container.addClass('loading');
		$error_container.addClass('loading');
		$form.addClass('sending');

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			dataType: 'json',
			data: formData,
			success: function (response) {
				console.log('success');
				console.log(response);

				if(response.status == 200){
					$opening_price.html(response.opening_price);
					$closing_price.html(response.closing_price);
					$total.html(response.total);
					$error_container.addClass('hide');
					$container.removeClass('hide');
					$container.removeClass('loading');
				}else{
					$container.addClass('hide');
					$error_container.removeClass('hide');
					$error_container.removeClass('loading');
				}

				$form.removeClass('sending');
			},
			error: function (response) {
				console.log('error');
				console.log(response);
			}
		});

		return false;
	});

	jQuery.each(jQuery('[data-role="touch-submenu-toggler"]'), function () {
		var $this = $(this);

		$this.on('click', function (e) {
			event.preventDefault();
			// console.log('clicked');
			var $current = $(e.currentTarget),
				 $currentParent = $current.parents('[data-role="touch-submenu"]');
			$current.toggleClass('deployed');
			$currentParent.toggleClass('deployed');
		});
	});



	jQuery.each(jQuery('[data-role="nav-body"] .nav-bar__menu>li.menu-item:not(.current-menu-item):not(current-menu-parent):not(current-page-parent)'), function () {
		var $this = $(this);
		$this
		.mouseenter(function() {
			if(!$this.hasClass('current-menu-item') && !$this.hasClass('current-menu-parent') && !$this.hasClass('current-menu-ancestor') && !$this.hasClass('current-page-ancestor') && !$this.hasClass('current-page-parent')){
				$('[data-role="nav-body"] .nav-bar__menu>li.menu-item').addClass('hold-on');
				$('.nav-bar__internal').addClass('hide');
			}
		})
		.mouseleave(function() {
			$('[data-role="nav-body"] .nav-bar__menu>li.menu-item').removeClass('hold-on');
			$('.nav-bar__internal').removeClass('hide');
		 });
	});

	jQuery.each(jQuery('#equipo-grid [data-expand]'), function (e) {
		$(this).mouseover( function(e){
			$('body main').addClass('free');
		}).mouseleave(function(e){
			$('body main').removeClass('free');
		});
	});

	jQuery.each(jQuery('[data-role="deploy-select"]'), function () {
		var select = this,
			$select = $(select),
			value_deployer = $select.data('deployon');

		$select.on('change', function(){
			var selected_value = $(this).val();
			var $target = $('[data-role="deploy-target"]');

			if(selected_value == value_deployer){
				$target.removeClass('hide');
				$($target.find('input')).attr('required', true);
			}else{
				$target.addClass('hide');
				$($target.find('input')).attr('required', false);
			}
		});
	});

	jQuery.each(jQuery('img[data-srcset]'), function () {
		var $this_img = $(this);
		var src_set = $this_img.data('srcset') || '';

		if(src_set !== ''){
			var sizes = src_set.split(',');
			var resize = [];
			var default_src = $this_img.attr('src');

			if(sizes.length > 0){
				$this_img.attr('data-srcfull', default_src);
				sizes.forEach(function (value, key) {
					value = value.slice(0,-1);
					var size = value.split(' ');

					resize.push({ size: size[1], src: size[0] })
				});
				resize.reverse();
				resize.forEach(function (fuente, key) {
					var window_width = $(window).width();
					if(window_width <= fuente['size']){ $this_img.attr('src', fuente['src']) }
					if(window_width > resize[0]['size']){ $this_img.attr('src', default_src) }
				});
				$(window).on('resize', function () {
					resize.forEach(function (fuente, key) {
						var window_width = $(window).width();
						if(window_width <= fuente['size']){ $this_img.attr('src', fuente['src']) }
						if(window_width > resize[0]['size']){ $this_img.attr('src', default_src) }
					});
				});
			}
		}
	});

	jQuery.each(jQuery('[data-srcset]:not(img)'), function () {
		var $this_figure = $(this);
		var src_set = $this_figure.data('srcset') || '';

		if(src_set !== ''){
			var sizes = src_set.split(',');
			var resize = [];
			var default_src = $this_figure.attr('style');

			// console.log(sizes);
			
			if(sizes.length > 0){
				$this_figure.attr('data-srcfull', default_src);
				sizes.forEach(function (value, key) {
					value = value.slice(0,-1);
					var size = value.split(' ');

					resize.push({ size: size[1], bg: size[0] })
				});
				resize.reverse();
				resize.forEach(function (fuente, key) {
					var window_width = $(window).width();
					if(window_width <= fuente['size']){ $this_figure.attr('style', fuente['bg']) }
					if(window_width > resize[0]['size']){ $this_figure.attr('style', default_src) }
				});
				$(window).on('resize', function () {
					resize.forEach(function (fuente, key) {
						var window_width = $(window).width();
						if(window_width <= fuente['size']){ $this_figure.attr('style', fuente['bg']) }
						if(window_width > resize[0]['size']){ $this_figure.attr('style', default_src) }
					});
				});
			}
		}
	});

	jQuery.each(jQuery('[data-bgset]:not(img)'), function () {
		var $this_figure = $(this);
		var src_set = $this_figure.data('bgset') || '';

		if(src_set !== ''){
			var sizes = src_set.split(',');
			var resize = [];
			var default_src = $this_figure.attr('data-bg');

			// console.log(sizes);
			
			if(sizes.length > 0){
				$this_figure.attr('data-bgfull', default_src);
				sizes.forEach(function (value, key) {
					value = value.slice(0,-1);
					var size = value.split(' ');

					resize.push({ size: size[1], bg: size[0] })
				});
				resize.reverse();
				resize.forEach(function (fuente, key) {
					var window_width = $(window).width();
					if(window_width <= fuente['size']){
						$this_figure.attr('data-bg', fuente['bg']);
						$this_figure.attr('style', fuente['bg']);
					}
					if(window_width > resize[0]['size']){
						$this_figure.attr('data-bg', default_src);
						$this_figure.attr('style', default_src);
					}
				});
				$(window).on('resize', function () {
					resize.forEach(function (fuente, key) {
						var window_width = $(window).width();
						if(window_width <= fuente['size']){
							$this_figure.attr('data-bg', fuente['bg']);
							$this_figure.attr('style', fuente['bg']);
						}
						if(window_width > resize[0]['size']){
							$this_figure.attr('data-bg', default_src);
							$this_figure.attr('style', default_src);
						}
					});
				});
			}
		}
	});

	jQuery.each(jQuery('[data-role="sticky-role"]'), function () {
		var $this = $(this),
			$limit_top = $this.offset().top || 100,
			$limit_bottom = $($this.data('stop')).offset().top || 100,
			max_width = $this.width();
		
		$(window).scroll( function () {
			var scroll = $(window).scrollTop();

			// if(scroll  >= start){
			if(scroll  >= $limit_top && scroll < $limit_bottom){
				$this.addClass('fixed').css('width', max_width);

				$('[data-func="scrollToTarget]').on('click', function(){
					$this.removeClass('deployed');
					$(this).find('[data-func="toggleTarget"]').removeClass('deployed');
					$(this).find('[data-role="scroll-navigation-side"]').removeClass('deployed');
				});
			}else{					
				$this.removeClass('fixed');
			}
		});
	});
});

function sortByKeyDesc(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x > y) ? -1 : ((x < y) ? 1 : 0));
	});
}

function sortByKeyAsc(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}
