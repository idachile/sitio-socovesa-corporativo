<?php
/*
* Template name: Página Contacto
* Template Post Type: page, en_page
*/
get_header();
the_post();
$pid = get_the_ID();
$tipo_contacto = 27;
?>
<main>
	<section class="horizon">
		<div class="container">
			<div class="row">
				<div class="gr-6 gr-12@tablet flex flex-center">
				<?php the_title('<h1 class="single__title">', '</h1>');
						if(has_excerpt($pid)) echo '<div class="single__excerpt">' . apply_filters('the_content', get_the_excerpt($pid)) . '</div>';
					?>
				</div>
				<?php
					if(!empty(get_post_ancestors( $pid ))):
						$tipo_contacto = 28;
				?>
				<div class="gr-6 gr-12@tablet">
					<?php if(get_field('habilitar_contacto', $pid)): ?>
					<article class="box box--people" style="margin-bottom: -10rem;">
						<figure class="box__figure">
							<?php echo wp_get_attachment_image(get_field('imagen_contacto', $pid), 'square_300', false, array('class' => 'cover-img')); ?>
						</figure>
						<div class="box__body">
							<span class="box__meta"><?php the_field('subtitulo_contacto', $pid); ?></span>
							<h2 class="box__title"><?php the_field('titulo_contacto', $pid); ?></h2>
							<div class="box__excerpt">
								<p><strong>Telefono:</strong> <a href="tel:<?php echo clean_telefono(get_field('telefono_contacto', $pid)); ?>" title="Contactar a <?php the_field('titulo_contacto', $pid) ?>"><?php the_field('telefono_contacto', $pid); ?></a></p>
								<p><strong>Email:</strong> <a href="tel:<?php the_field('email_contacto', $pid); ?>" title="Llamar a <?php the_field('titulo_contacto', $pid) ?>"><?php the_field('email_contacto', $pid); ?></a></p>
							</div>
						</div>
					</article>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="horizon no-gutter-top">
		<div class="container">
			<div class="form-parent" data-parent>
				<form action="/wp-json/procesar/contacto/" class="row form" method="POST" data-module="common-form" data-validation="generic_submit" enctype="multipart/form-data">
					<input type="hidden" name="pid" value="<?php echo $pid; ?>">
					<input type="hidden" name="tipo_envio" value="submit">
					<input type="hidden" name="tipo_contacto" value="<?php echo $tipo_contacto; ?>">
					<input type="hidden" name="grupo" value="field_5d287e9c2dcd6">
					<input type="hidden" name="st_verify" id="st_verify">
					<?php
						global $form;
						$datos_de_contacto = get_field_object('datos_de_contacto', $pid);
					?>
					<div class="gr-6 gr-12@tablet">
					<?php
						$value = '';
						for($i = 0; $i < 4; $i++){
							$form->handler($datos_de_contacto['sub_fields'][$i], false, $pid);
						}
					?>
					</div>
					<div class="gr-6 gr-12@tablet">
						<?php
							$value = '';
							
							for($i = 4; $i < count($datos_de_contacto['sub_fields']); $i++){
								$form->handler($datos_de_contacto['sub_fields'][$i], false, $pid);
							}
						?>
						<div class="form-control flex-right flex-center@tablet">
							<?php wp_nonce_field('enviar_contacto', 'st_nonce'); ?>
							<button class="button button--main button--wide" type="submit" title="Enviar formulario">Enviar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</main>
<?php
$post = get_post($pid);
if($post->post_parent == 12 || get_post($post->post_parent)->post_parent == 12):
	echo load_template_part('partials/inversionista/kit','inversionista');
endif;
?>
<?php get_footer(); ?>