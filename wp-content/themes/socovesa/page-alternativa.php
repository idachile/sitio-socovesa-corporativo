<?php
/*
* Template Name: Página modular alternativa
* Template Post Type: en_page, page
*/
the_post();
$pid = get_the_ID();
global $post,$pid;
get_header();

?>
<main id="top" <?php echo is_page(394) ? 'data-role="horizon-navigation" class="animain"' : ''; ?>>
<?php
$secciones = '';
$secciones = get_field('secciones', $pid);

global $data_section;
if( isset($secciones) && !empty($secciones)):
	foreach( $secciones as $data_section ):
		if(!empty($data_section) && isset($data_section)):
			get_template_part('partials/alternativa/'. str_replace('_', '-', $data_section['acf_fc_layout']));
			if($data_section['acf_fc_layout']=="modulo_grafico"):
				needs_script('canvasjs');
				needs_script('chart_spline_area');
				needs_script('chart_doughnout');
				needs_script('chart_column');
			endif;
		endif;
	endforeach;
endif;
if(is_page(394)):
	needs_script('rellax');
endif;
?>
</main>
<?php
$post = '';
$post = get_post($pid);
if( isset($post)  && $post->post_parent !== 0):
	if($post->post_parent == 12 || get_post($post->post_parent)->post_parent == 12):
		echo load_template_part('partials/inversionista/kit','inversionista');
	endif;
endif;
?>
<?php get_footer(); ?>