<?php
get_header();

$enlace     = get_field('enlace_404', 'options');
$bajada     = get_field('bajada_404', 'options');
$imagen_url = get_field('imagen_404', 'options');
?>
<main>
	<section class="horizon horizon--overlay" style="background-image: url(<?php echo $imagen_url ?>)">
		<div class="horizon__veil">
			<div class="container">
				<div class="row">
					<div class="gr-4 prefix-8 gr-6@large prefix-6@large gr-10@medium prefix-1@medium gr-12@small prefix-0@small">
						<h1 class="horizon__title title-huge"><?php the_field('titulo_404', 'options'); ?></h1>
						<?php if (!empty($bajada)) : ?>
							<div class="horizon__excerpt"><?php echo apply_filters('the_content', $bajada); ?></div>
						<?php endif; ?>
						<?php if (!empty($enlace)) : ?>
							<div class="horizon__action">
								<a href="<?php echo $enlace['enlace_url']; ?>" class="button button--main" title="<?php echo $enlace['enlace_titulo']; ?>"><?php echo $enlace['enlace_titulo']; ?></a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>