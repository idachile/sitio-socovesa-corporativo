<?php global $post; ?>
<footer id="footer" class="footer-bar" data-role="stopper" data-offset="300">
	<section class="footer-bar__main">
		<div class="container">
			<div class="row" data-equalize="target" data-mq="book-down" data-eq-target="[data-eq]">
				<div class="gr-2 suffix-1 gr-12@book suffix-0@large flex-center flex-middle@book">
					<section class="footer-bar__brand" data-eq>
						<a href="/" title="Volver al home"><img src="<?php the_field('logo_principal', 'options'); ?>" class="elastic-img" alt="Logo Socovesa blanco"></a>
					</section>
				</div>
				<div class="gr-9 gr-10@large gr-12@book flex-end flex-middle@book">
					<?php
					$main_nav  = '<ul id="%1$s" class="%2$s" data-eq>';
					$main_nav .= 	'%3$s';
					$main_nav .= '</ul>';
					if(get_post_type($post) == 'en_page'):
						$menu_id = 'footer-menu';
					else:
						$menu_id = 'footer-principal';
					endif;
					wp_nav_menu(array(
						'menu'              => $menu_id,
						'theme_location'    => $menu_id,
						'menu_class'        => "footer-bar__nav",
						'menu_id'           => "footer",
						'container'         => false,
						'container_class'   => false,
						'container_id'      => false,
						'walker'            => new custom_sub_walker(),
						'depth'				  => 1,
						'items_wrap' =>     $main_nav,
						'fallback_cb'       => false
					));
					?>
				</div>
			</div>

			<div class="row hat-tiny" data-equalize="target" data-mq="false" data-eq-target="[data-eq]">
				<div class="gr-8 gr-10@large gr-12@book prefix-2 prefix-1@large suffix-2 suffix-1@large prefix-0@book suffix-0@book flex-end no-gutter-vertical no-gutter@book">
					<div class="row no-margin@book">
						<div class="gr-4 gr-12@tablet">
							<h2 class="footer-logos__title" data-eq><?php echo !empty(get_field('titulo_filiales', 'options')) ? get_field('titulo_filiales', 'options') : 'Filiales'; ?></h2>
						</div>
						<?php
						$filiales = get_field('filiales', 'options');
						if (!empty($filiales)) :
							foreach ($filiales as $filial) :
								echo 	'<div class="gr-2 gr-3@tablet gr-6@small"><figure class="footer-logos__figure" data-eq>';
								echo 		'<a href="' . ensure_url($filial['url']) . '" title="Ir a sitio de ' . $filial['nombre'] . '"><img src="' . $filial['logo'] . '" alt="Imagen de ' . $filial['nombre'] . '" class="elastic-img"></a>';
								echo 	'</figure></div>';
							endforeach;
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="footer-bar__copyright">
		<div class="container">
			<p class="flex-center">
				<span class="copyright__year"><?php echo get_field('footer_copyright', 'options'); ?></span>
				<a href="<?php echo gmap_url(get_field('footer_direccion', 'options')); ?>" class="copyright__address" title="Ver ubicación en google maps" target="_blank"><?php echo get_field('footer_direccion', 'options'); ?></a>
				<a href="tel:<?php echo clean_telefono(get_field('footer_telefono', 'options')); ?>" class="copyright__tel" title="Ver ubicación en google maps" target="_blank"><?php echo get_field('footer_telefono', 'options'); ?></a>
			</p>
		</div>
	</section>
</footer>
<?php wp_footer(); ?>
<?php the_field('codigo_linkedin', 'options'); ?>
<!--Por https://ida.cl-->
</body>

</html>