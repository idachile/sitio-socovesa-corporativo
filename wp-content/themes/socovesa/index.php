<?php
get_header();
// the_post();
global $wp_query, $post, $paged;
?>
<main id="top">
	<?php
		$secciones = get_field('secciones', 360); //pagina noticias
		global $data_section;
		if(!empty($secciones)):
			foreach( $secciones as $data_section ):
				get_template_part('partials/page/'. str_replace('_', '-', $data_section['acf_fc_layout']));
			endforeach;
		endif;
	?>

	<section class="horizon horizon--normal">
		<div class="container">
			<div class="row bg-light-grey">
				<form action="/noticias/" method="get" class="flex-wrap no-border">
					<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
						<div class="form-control__container">
							<select id="year" name="year" class="form-control__field--select form-control__field form-control__select w-semibold">
								<option value="">Cualquier año</option>
								<?php
								for($x = 0; $x<=10; $x++):
									$year = date('Y') - $x;
									isset($_GET['year']) && $_GET['year'] == $year ? $sel_year = 'selected' : $sel_year = '';
									echo '<option value="'.$year.'" '.$sel_year.'>'.$year.'</option>';
								endfor;
								?>
							</select>
						</div>
					</div>
					<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
						<div class="form-control__container">
							<select id="month" name="month" class="form-control__field--select form-control__field form-control__select w-semibold">
								<option value="">Cualquier mes</option>
								<?php
									$months = get_months();
									foreach ($months as $key => $month) {
										isset($_GET['month']) && $_GET['month'] == $key ? $sel_month = 'selected' : $sel_month = '';
										echo '<option value="'.$key.'" '.$sel_month.'>'.$month.'</option>';
									}
								?>
							</select>
						</div>
					</div>
					<div class="gr-3 gr-12@medium gutter-double gutter@medium flex flex-center">
						<div class="form-control__container">
							<select id="postype" name="tipo" class="form-control__field--select form-control__field form-control__select w-semibold">
								<option value="">Cualquier tipo</option>
								<?php
									$categories = get_terms('category', 'hide_empty=1&orderby=ID&order=ASC');
									foreach ( $categories as $category ) {
										$selected = "";
										if( strtolower($_GET['tipo']) == $category->slug ) {
											$selected = "selected";
										}
										echo '<option value="' . $category->slug . '" ' . $selected . '>' . $category->name . '</option>';
									}
								?>
							</select>
						</div>
					</div>
					<div class="gr-3 gr-12@medium gutter-double gutter@medium font-centered">
						<div class="box__action no-margin">
							<button class="button button--main button--full-width">Filtrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php
	$noticias_page = 360;

	$destacado_id = get_field('destacado', $noticias_page);
	?>
	<section class="horizon horizon--normal">
		<div class="container">
			<?php if(!empty($destacado_id) && $paged <= 1): $post = get_post($destacado_id[0]); ?>
			<div class="row">
				<div class="gr-12 gr-12@tablet"><?php get_template_part('partials/noticias/destacado') ?></div>
			</div>
			<?php endif; ?>
			<?php if(have_posts()): $contador = 1; ?>
			<div class="row" data-role="divide-news">
			<?php while(have_posts()): the_post(); ?>
				<div class="gr-12 gr-12@tablet" data-role="news-child" data-num="<?php echo $contador; ?>"><?php get_template_part('partials/noticias/post') ?></div>
			<?php $contador++; endwhile; ?>
			</div>
			<?php if($wp_query->found_posts > 10) echo get_pagination($query); ?>
			<?php else: ?>
			<h2 class="horizon__title">Lo sentimos</h2>
			<div class="horizon__excerpt heels">
				<p>No se encontraron noticias para el filtro seleccionado.</p>
			</div>
		<?php endif; wp_reset_query()?>
		</div>
	</section>
</main>
<?php get_footer(); ?>
