<?php
//*************************************************************                 Funciones utilitarias de menu para su uso en el theme
require_once 'framework/menu.php';
//*************************************************************                 Funciones utilitarias genéricas para su uso en el theme
require_once 'framework/utilities.php';
//*************************************************************                 Funciones utilitarias genéricas para su uso en el theme
require_once 'framework/email.php';
//*************************************************************                 Configuraciones sobre escritas de funcionalidades default de WP
require_once 'framework/overrides.php';
//*************************************************************                 Funcionalidades de caracter tranversal al theme de wordpress, footer, sidebar, header y widget en general
require_once 'framework/forms.php';
//*************************************************************                 Funcionalidades que afectan los post y post-type no geráricos del sitio
require_once 'framework/single.php';
//*************************************************************                 Definción de api para porceso transaccionales, formularios y consumo de datos
require_once 'framework/api-rest.php';
//*************************************************************                 Funcionalidades que afectan los post y post-type no geráricos del sitio
require_once 'framework/personas.php';
?>