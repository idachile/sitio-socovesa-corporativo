<?php
global $pid;
the_post();
$pid = get_the_ID();
$post = get_post($pid);
get_header();
?>
<main>
	<?php get_template_part('partials/sections/header', 'basic'); ?>

	<section class="horizon--inner">
		<div class="container">
			<div class="row">
				<div class="gr-8 gr-12@tablet gr-12@small">
					<section class="reset-contenido"><?php get_template_part('partials/sections/flex', 'content'); ?></section>
				</div>
				<div class="gr-4 gr-12@tablet gr-12@small"><?php get_sidebar(); ?></div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
