<?php
/*
* Template Name: Front page
* Template Post Type: en_page
*/
get_header();
the_post();
global $data_section;
?>
<main id="top" data-role="horizon-navigation" class="animain">
<?php
	$secciones = get_field('secciones');
	if (!empty($secciones)) :
		foreach ($secciones as $data_section) :
			get_template_part('partials/home/' . str_replace('_', '-', $data_section['acf_fc_layout']));
		endforeach;
		needs_script('rellax');
	endif;
?>
</main>
<?php get_footer(); ?>