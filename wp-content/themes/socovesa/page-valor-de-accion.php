<?php
/*
* Template name: Página Valor de acción
* Template Post Type: page, en_page
*/
global $post,$pid;
the_post();
$pid = get_the_ID();
$post = get_post($pid);
get_header();
?>
<main id="top">
<?php
$secciones = '';	
$secciones = get_field('secciones');
global $data_section;
if(!empty($secciones)):
	foreach( $secciones as $data_section ):
		get_template_part('partials/valor-de-accion/'. str_replace('_', '-', $data_section['acf_fc_layout']));
	endforeach;
endif;
?>
</main>
<?php
$post = '';
$post = get_post($pid);
if( isset($post)  && $post->post_parent !== 0):
	if($post->post_parent == 12 || get_post($post->post_parent)->post_parent == 12):
		echo load_template_part('partials/inversionista/kit','inversionista');
	endif;
endif;
?>
<?php get_footer(); ?>