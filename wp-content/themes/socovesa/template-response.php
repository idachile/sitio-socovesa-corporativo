<?php
/*
* Template name: Página Contacto - Respuesta
* Template Post Type: page, en_page
*/
get_header();
the_post();
?>
<main>
	<section class="horizon">
		<div class="container">
			<div class="row">
				<div class="gr-6 gr-12@tablet">
					<h1 class="single__title">Contacto</h1>
				</div>
			</div>
		</div>
	</section>
	<section class="horizon no-gutter-top">
		<div class="container">
			<div class="form-parent" data-parent>
				<div class="response">
					<figure class="response__icon"><span class="icon icon-times"></span></figure>
					<div class="response__body">
						<h3 class="response__title"><?php the_title() ?></h3>
						<div class="response__excerpt"> <?php the_content(); ?> </div>
						<div class="response__action hat-tiny">
							<a href="/">Regresar al home</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
