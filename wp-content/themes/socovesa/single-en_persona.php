<?php
the_post();
global $post;
get_header();
$pid = get_the_ID();
$post = get_post($pid);
$pertenencia = wp_get_post_terms($pid, 'pertenencia');
$filial = wp_get_post_terms($pid, 'filial');
$grupo_name = $button = '';
$agrupador = $grupos_id = $tax_query = array();

if(!empty($pertenencia)):
	if(count($pertenencia) > 1 && count($pertenencia) <= 2):
		foreach($pertenencia as $per):
			$grupo_name .= $per == end($pertenencia) ? ' y ' : '';
			$grupo_name .= $per->name;
			$grupage = get_page_by_path('/equipo/' . $per->slug);
			$button .= '<a href="'.get_permalink($grupage).'" class="button button--ghost-dark" title="Ir a '. $per->name .'">Ver '.$per->name.'</a>';
		endforeach;
	elseif(count($pertenencia) > 2):
		foreach($pertenencia as $per):
			$grupo_name .= $per == end($pertenencia) ? ' y ' : '';
			$grupo_name .= $per->name;
			$grupo_name .= $per !== end($pertenencia) ? ', ' : '';
			$grupage = get_page_by_path('/equipo/' . $per->slug);
			$button .= '<a href="'.get_permalink($grupage).'" class="button button--ghost-dark" title="Ir a '. $per->name .'">Ver '.$per->name.'</a>';
		endforeach;
	elseif(count($pertenencia) <= 1):
		$button = '<a href="'.get_permalink(get_page_by_path('/equipo/' . $pertenencia[0]->slug)).'" class="button button--ghost-dark" title="Ir a '. $pertenencia[0]->name .'">Ver '.$pertenencia[0]->name.'</a>';
		$grupo_name = $pertenencia[0]->name;
	endif;

	$agrupador = $pertenencia;
	$tax_name = 'pertenencia';
elseif(!empty($filial)):
	$grupo_name = $filial[0]->name;
	$button_name = $grupo_name;
	$grupage = get_page_by_path('/equipo/equipo-' . $filial[0]->slug);
	$agrupador = $filial;
	$tax_name = 'filial';

	$button = '<a href="'.get_permalink($grupage).'" class="button button--ghost-dark" title="Ir a '. $button_name .'">Ver '.$button_name.'</a>';
endif;

if(!empty($agrupador)):
	foreach($agrupador as $term) $grupos_id[] = $term->term_id;
	$tax_query = array(
		array(
			'taxonomy' => $tax_name,
			'field' => 'term_id',
			'terms' => $grupos_id
		)
	);
endif;
?>
<main id="top">

	<section class="horizon horizon--content" data-horizon>
		<div class="container">
			<div class="row" data-equalize="target" data-mq="tablet-down" data-eq-target="[data-eq]">
				<div class="gr-6 gr-5@medium gr-12@tablet no-gutter-right gutter@tablet" data-rellax data-rellax-speed="2" data-eq>
					<figure class="persona__thumbnail">
						<?php the_post_thumbnail( 'big_700x800', array( 'class' => 'cover-img hide@tablet' ) ); ?>
						<?php the_post_thumbnail( 'medium_800x800', array( 'class' => 'cover-img show@tablet' ) ); ?>
					</figure>
					<?php $datos_persona= get_field('datos_persona'); ?>
					<?php if(!empty($datos_persona)): ?>
					<div class="persona__profile hide@tablet" data-area-name="desktop-profile">
						<dl class="persona__data" data-mutable="tablet-down" data-desktop-area="desktop-profile" data-mobile-area="mobile-profile">
							<?php foreach ($datos_persona as $datos): ?>
								<dt class="persona__data__key"><?php echo $datos['key'] ?></dt>
								<dd class="persona__data__value"><?php echo $datos['value'] ?></dd>
							<?php endforeach; ?>
						</dl>
					</div>
					<?php endif; ?>
				</div>

					<div class="gr-6 gr-7@medium gr-12@tablet no-gutter-left gutter@tablet bg-white">
						<section class="persona__body" data-eq>
							<div class="single__header">
								<p class="single__meta"><?php the_field("cargo");?></p>
								<h1 class="single__title"><?php the_title();?></h1>
							</div>
							<div class="persona__profile show@tablet" data-area-name="mobile-profile"></div>
							<div class="single__content">
								<?php the_content();?>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>

		<?php if(!empty($agrupador)):?>
		<section class="horizon horizon--float">
			<div class="container">
				<div class="row heels">
					<div class="gr-10 gr-centered gr-12@medium">
						<h2 class="horizon__title title-center">Conoce más de <?php echo $grupo_name; ?></h2>
					</div>
				</div>
				<div class="grid grid-cols-5 grid-cols-4@medium grid-cols-2@tablet grid-cols-1@phablet" id="equipo-grid">
					<?php
					$args['post_type'] 			= 'en_persona';
					$args['post_status'] 		=  'publish';
					$args['tax_query'] 			= $tax_query;
					$args['posts_per_page'] = -1;
					$args['post__not_in'] = array($pid);
					$setting['random_imagenes'] 	=  array();
					$persona = new persona();
					$persona_print = '';
					$persona_print = $persona->get_grilla_personas($args, $setting);

					if(isset($persona_print) && !empty($persona_print)):
						echo $persona_print;
					endif;
					// echo $persona->get($args, $setting);
					?>
				</div>
				<?php if(!empty($button)): ?>
				<div class="flex-center hat button-group">
					<?php echo $button; ?>
				</div>
				<?php endif; ?>
			</div>
		</section>
		<?php endif; ?>
	</main>
	<?php get_footer(); ?>
