import { Component, Injectable, Input }         from '@angular/core';
import { HttpClient }                           from '@angular/common/http';
import { Observable }                           from 'rxjs';
import { map }                                  from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators }   from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  data:       any = [];
  temp:       any = [];
  searchForm: FormGroup;
  no_results: any = "";

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({
      search_input: ['', Validators.minLength(4) ]
    });
  }

  ngOnInit() {
    let term = "", hurl = "";
    // term = '?pid=4320';
    if(document.querySelector("[data-post-id]") != null) {
      term = '?pid=' + document.querySelector("[data-post-id]").getAttribute("data-post-id");
    }
    if(document.querySelector("[data-home-url]") != null) {
      hurl = document.querySelector("[data-home-url]").getAttribute("data-home-url") + '/';
    }
    console.log(hurl)
    this.queryGet(hurl + 'wp-json/get/repositorio/full' + term).then((data) => {
      this.data = data;

      let element = document.querySelector("[data-sidebar-role='sticky-sidebar']"), offsetTop = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top, arrayOffset = [];
      let elem_width = element.getBoundingClientRect().width;
      setTimeout(function(){
        document.querySelectorAll("[data-page-id]").forEach(function(page, index){
          arrayOffset.push(page.getBoundingClientRect().top - document.body.getBoundingClientRect().top);
        });
      }, 50);

      window.addEventListener("scroll", function(e) {
        if(offsetTop < -1*document.body.getBoundingClientRect().top+60){
          element.classList.add('fixed');
          element.setAttribute("style", "width: "+element.parentElement.getBoundingClientRect().width+"px;");
        } else {
          element.classList.remove('fixed');
        }
        // if(element.getBoundingClientRect().top - document.body.getBoundingClientRect().top > offsetTop) {
        //   document.querySelector("[data-sidebar-role='sticky-sidebar']").classList.add("sticky-stuck");
        //   document.querySelectorAll("[data-section-role='padded-section']").forEach(function(section){
        //     section.classList.add("sticky-stuck");
        //   });
        // } else {
        //   document.querySelector("[data-sidebar-role='sticky-sidebar']").classList.remove("sticky-stuck");
        //   document.querySelectorAll("[data-section-role='padded-section']").forEach(function(section){
        //     section.classList.remove("sticky-stuck");
        //   });
        // }

        document.querySelectorAll("[data-page-id]").forEach(function(page, index){
          if(-1*document.body.getBoundingClientRect().top > arrayOffset[index]){
            document.querySelectorAll("[data-item-id-sidebar]").forEach(function(sub_page){
              sub_page.classList.remove("active");
            });
            document.querySelector("[data-item-id-sidebar='"+ page.getAttribute("data-page-id") +"']").classList.add("active");
          }
        });
      });
    });
  }

  scrollTo(page_id, horizon_title, $event) {
    $event.preventDefault();
    let element = document.querySelector("[data-horizon-id='" + page_id + "_" + horizon_title.split(' ').join('') + "']");
    window.scrollTo({top: element.getBoundingClientRect().top + window.pageYOffset - 60, behavior: 'smooth'});
    this.toggleMenu();
  }

  toggleMenu() {
    document.querySelector("[data-sidebar-menu='collapsable-menu']").classList.toggle('collapsed');
  }

  submitSearch(form, $event) {
    $event.preventDefault();
    this.data = this.temp != '' ? this.temp : this.data;

    let resultArray = [], term = form.value.search_input;
    if(term.length > 3 && this.data) {

      this.data.forEach(function(pagina, index_pagina){
        pagina.horizontes.forEach(function(horizonte, index_horizonte){
          horizonte.documentos.forEach(function(documento, index_documento){
            if( documento.titulo.toUpperCase().includes(term.toUpperCase()) || documento.mes && documento.mes.toUpperCase().includes(term.toUpperCase()) || documento.tipo && documento.tipo.name.toUpperCase().includes(term.toUpperCase()) ) {
              if(resultArray[index_pagina] == undefined){
                resultArray[index_pagina] = {};
              }
              if(resultArray[index_pagina]['horizontes'] == undefined){
                resultArray[index_pagina]['horizontes'] = [];
              }
              if(resultArray[index_pagina]['horizontes'][index_horizonte] == undefined){
                resultArray[index_pagina]['horizontes'][index_horizonte] = {};
              }
              if(resultArray[index_pagina]['horizontes'][index_horizonte]['documentos'] == undefined){
                resultArray[index_pagina]['horizontes'][index_horizonte]['documentos'] = [];
              }
              resultArray[index_pagina]['id'] = pagina.id;
              resultArray[index_pagina]['titulo'] = pagina.titulo;
              resultArray[index_pagina]['url'] = pagina.url;
              resultArray[index_pagina]['horizontes'][index_horizonte]['titulo'] = horizonte.titulo;
              resultArray[index_pagina]['horizontes'][index_horizonte]['orden'] = horizonte.orden;
              resultArray[index_pagina]['horizontes'][index_horizonte]['documentos'].push(documento);
            }
          });
        });
      }, this);

      let filteredArray = resultArray.filter(function (el) {
        return el != null;
      });

      this.temp = this.data;
      this.data = filteredArray;

      if(this.data == '') {
        this.no_results = 'No se encontraron resultados en esta búsqueda. Intente con un término distinto.';
      }
    }
  }

  queryGet(url){
    return new Promise( (resolve, reject) => {
      this.httpClient.get(url).pipe(
        map((res: Response) => res)
      ).subscribe(
        (data)  => {
          resolve(data);
        },
        (err)   => {
          console.log(err);
          reject();
        }
      );
    });
  }
}
