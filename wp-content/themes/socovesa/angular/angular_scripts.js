import './dist/angular-socovesa-corporativo/runtime-es5.js';
import './dist/angular-socovesa-corporativo/runtime-es2015.js';

// import './dist/angular-socovesa-corporativo/polyfills-es5.js';
import './dist/angular-socovesa-corporativo/polyfills-es2015.js';

import './dist/angular-socovesa-corporativo/styles-es5.js';
import './dist/angular-socovesa-corporativo/styles-es2015.js';

import './dist/angular-socovesa-corporativo/vendor-es5.js';
import './dist/angular-socovesa-corporativo/vendor-es2015.js';

import './dist/angular-socovesa-corporativo/main-es5.js';
import './dist/angular-socovesa-corporativo/main-es2015.js';
