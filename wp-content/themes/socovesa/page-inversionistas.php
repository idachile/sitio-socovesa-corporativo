<?php
/*
* Template Name: Portada Inversionistas
* Template Post Type: en_page, page
*/
get_header();
the_post();
global $post;
?>
<main id="top">
	<?php
	$secciones = get_field('secciones');
	global $data_section;
	foreach( $secciones as $data_section ){
		get_template_part('partials/inversionista/'. str_replace('_', '-', $data_section['acf_fc_layout']));
	}
	?>
</main>
<?php echo load_template_part('partials/inversionista/kit','inversionista'); ?>
<?php get_footer(); ?>