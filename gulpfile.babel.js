'use strict';
import gulp from 'gulp';
import pkg from './package.json';
import sftp from 'gulp-sftp';

import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
const browsersync = require('browser-sync').create();
const reload = browserSync.reload;

import gulpLoadPlugins from 'gulp-load-plugins';
const $ = gulpLoadPlugins();

const ssi = require('browsersync-ssi');
const includer = require('gulp-html-ssi');
const ext_replace = require('gulp-ext-replace');
const string_replace = require('gulp-string-replace');
const imagemin = require('gulp-imagemin');
const dev_path = '/var/www/vhosts/idastage.com/httpdocs';
const site_path = '/socovesa.idastage.com/';
const theme_path = 'wp-content/themes/socovesa';

/// se llaman los plugins necesarios
const fs = require('fs');
const GulpSSH = require('gulp-ssh');
const intercept = require('gulp-intercept');
const gutil = require('gulp-util');
const shell = require('gulp-shell');
var rename = require('gulp-rename');

//                                                                              Task 1 images OK
gulp.task('images', () =>
	gulp.src(theme_path + '/assets/images/**/*')
	.pipe($.imagemin())
	.pipe(gulp.dest(theme_path + '/dist/img'))
	.pipe($.size({
		title: 'images'
	}))
);

gulp.task('styles_ui', () => {
	const AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];
	// For best performance, don't add Sass partials to `gulp.src`
	return gulp.src([
			theme_path + '/assets/styles/jquery-ui.scss',
		])
		.pipe($.sourcemaps.init())
		.pipe($.sass({
			precision: 10
		}).on('error', $.sass.logError))
		.pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
		.pipe($.if('*.css', $.cssnano({
			discardComments: {
				removeAll: true
			}
		})))
		.pipe($.size({
			title: 'styles'
		}))
		.pipe(rename('jquery-ui.css'))
		.pipe($.sourcemaps.write('./'))
		.pipe(gulp.dest(theme_path + '/dist/css'));
});
//                                                                              Task 2 styles
// Compile and automatically prefix stylesheets
gulp.task('styles', ['styles_ui'], () => {
	const AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];
	// For best performance, don't add Sass partials to `gulp.src`
	return gulp.src([
			theme_path + '/assets/styles/main.scss',
		])
		.pipe($.sourcemaps.init())
		.pipe($.sass({
			precision: 10
		}).on('error', $.sass.logError))
		.pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
		.pipe($.if('*.css', $.cssnano({
			discardComments: {
				removeAll: true
			}
		})))
		.pipe($.size({
			title: 'styles'
		}))
		.pipe($.sourcemaps.write('./'))
		.pipe(gulp.dest(theme_path + '/dist/css'));
});
gulp.task('mainstyles', () => {
	const AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];
	// For best performance, don't add Sass partials to `gulp.src`
	return gulp.src([
			theme_path + '/assets/styles/main.scss',
		])
		.pipe($.sourcemaps.init())
		.pipe($.sass({
			precision: 10
		}).on('error', $.sass.logError))
		.pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
		.pipe($.if('*.css', $.cssnano({
			discardComments: {
				removeAll: true
			}
		})))
		.pipe($.size({
			title: 'styles'
		}))
		.pipe($.sourcemaps.write('./'))
		.pipe(gulp.dest(theme_path + '/dist/css'));
});
//                                                                              Task 6 styles watch
gulp.task('styles:watch', ['styles'], () => {
	gulp.watch(theme_path + '/assets/styles/**/*', ['styles']);
});



gulp.task('libs', () =>
	gulp.src([
		theme_path + '/assets/scripts/extra/relax.vanilla.js', theme_path + '/assets/scripts/extra/canvasjs.min.js', theme_path + '/assets/scripts/extra/jquery-ui.min.js'
	])
	.pipe($.size({
		title: 'libs'
	}))
	.pipe(gulp.dest(theme_path + '/dist/js/libs'))
);


gulp.task('extra', ['libs'], () =>
	gulp.src([
		theme_path + '/assets/scripts/extra/*.js',
		'!' + theme_path + '/assets/scripts/extra/canvasjs.min.js',
		'!' + theme_path + '/assets/scripts/extra/relax.vanilla.js',
		'!' + theme_path + '/assets/scripts/extra/jquery-ui.min.js',
	])
	.pipe($.sourcemaps.init())
	.pipe($.babel())
	.pipe($.uglify({
		compress: true
	}))
	.pipe($.size({
		title: 'source'
	}))
	.pipe($.sourcemaps.write('.'))
	.pipe(gulp.dest(theme_path + '/dist/js/extra'))
);



//                                                                              Task 7 scripts
// Concatenate and minify JavaScript. Optionally transpiles ES2015 code to ES5.
// to enable ES2015 support remove the line `'only': 'gulpfile.babel.js',` in the
// `.babelrc` file.
gulp.task('scripts', ['extra'], () =>
	gulp.src([
		theme_path + '/assets/scripts/libs/*.js',
		theme_path + '/assets/scripts/src/*.js',
		theme_path + '/assets/scripts/*.js',
	])
	.pipe($.sourcemaps.init())
	.pipe($.babel())
	.pipe($.concat('main.min.js'))
	.pipe($.uglify({
		compress: true
	}))
	// Output files
	.pipe($.size({
		title: 'scripts'
	}))
	.pipe($.sourcemaps.write('.'))
	.pipe(gulp.dest(theme_path + '/dist/js'))
);

gulp.task('mainscripts', () =>
	gulp.src([
		theme_path + '/assets/scripts/libs/*.js',
		theme_path + '/assets/scripts/src/*.js',
		theme_path + '/assets/scripts/*.js',
	])
	.pipe($.sourcemaps.init())
	.pipe($.babel())
	.pipe($.concat('main.min.js'))
	.pipe($.uglify({
		compress: true
	}))
	// Output files
	.pipe($.size({
		title: 'scripts'
	}))
	.pipe($.sourcemaps.write('.'))
	.pipe(gulp.dest(theme_path + '/dist/js'))
);

//                                                                              Task 8 scripts watch
gulp.task('scripts:watch', ['scripts'], () => {
	gulp.watch(theme_path + '/assets/scripts/**/*', ['scripts']);
});

//                                                                              Task 11 Server
// Watch files for changes & reload
gulp.task('serve', ['scripts', 'styles', 'images'], () => {
	// browserSync({
	//   notify: false,
	//   logPrefix: '**WSK**',
	//   scrollElementMapping: ['main', '.mdl-layout'],
	//   // https: true,
	//   // browser: 'firefox nightly',
	//   browser: 'google chrome',
	//   server: ['app'],
	//   port: 3004
	// });

	//gulp.watch([theme_path + '/**/*.html'], reload);
	gulp.watch([theme_path + '/assets/styles/**/**/*.{scss,css}'], ['styles', reload]);
	gulp.watch([theme_path + '/assets/scripts/libs/*.js', theme_path + '/assets/scripts/src/*.js'], ['scripts', reload]);
	gulp.watch([theme_path + '/assets/images/**/*'], ['images', reload]);
});

//                                                                              Task 13 Sync
gulp.task('styles:sftp', ['styles'], () => {
	return gulp.src("./" + theme_path + '/dist/css/**')
		.pipe(sftp({
			'host': 'idastage.com',
			'user': 'idastage',
			'keyLocation': '~/.ssh/id_rsa',
			'remotePath': dev_path + site_path + theme_path + '/dist/css/'
		}));
});
gulp.task('mainstyles:sftp', ['mainstyles'], () => {
	return gulp.src("./" + theme_path + '/dist/css/main.css')
		.pipe(sftp({
			'host': 'idastage.com',
			'user': 'idastage',
			'keyLocation': '~/.ssh/id_rsa',
			'remotePath': dev_path + site_path + theme_path + '/dist/css/'
		}));
});
//                                                                              Task 14 Sync
gulp.task('scripts:sftp', ['scripts'], () => {
	return gulp.src("./" + theme_path + '/dist/js/**')
		.pipe(sftp({
			'host': 'idastage.com',
			'user': 'idastage',
			'keyLocation': '~/.ssh/id_rsa',
			'remotePath': dev_path + site_path + theme_path + '/dist/js/'
		}));
});

gulp.task('mainscripts:sftp', ['mainscripts'], () => {
	return gulp.src("./" + theme_path + '/dist/js/*')
		.pipe(sftp({
			'host': 'idastage.com',
			'user': 'idastage',
			'keyLocation': '~/.ssh/id_rsa',
			'remotePath': dev_path + site_path + theme_path + '/dist/js/'
		}));
});

//                                                                              Task 16,17,18 watch
gulp.task('styles:serve', ['styles:sftp'], () => {
	gulp.watch([theme_path + '/assets/styles/**/**/*.{scss,css}'], ['styles:sftp']);
});
gulp.task('mainstyles:serve', ['mainstyles:sftp'], () => {
	gulp.watch([theme_path + '/assets/styles/**/**/*.{scss,css}'], ['mainstyles:sftp']);
});
gulp.task('scripts:serve', ['scripts:sftp'], () => {
	gulp.watch([theme_path + '/assets/scripts/libs/**.js', theme_path + '/assets/scripts/src/**.js', theme_path + '/assets/scripts/**.js'], ['scripts:sftp']);
});
gulp.task('mainscripts:serve', ['mainscripts:sftp'], () => {
	gulp.watch([theme_path + '/assets/scripts/libs/**.js', theme_path + '/assets/scripts/src/**.js', theme_path + '/assets/scripts/**.js'], ['mainscripts:sftp']);
});


gulp.task('listen', ['scripts:sftp', 'styles:sftp'], () => {
	gulp.watch(theme_path + '/assets/styles/**/**/*.{scss,css}', ['styles:sftp']);
	gulp.watch([theme_path + '/assets/scripts/libs/**.js', theme_path + '/assets/scripts/src/**.js', theme_path + '/assets/scripts/**.js'], ['scripts:sftp']);
});



//                                                                              Task 19 wordpress task
gulp.task('download', shell.task('wget https://wordpress.org/latest.zip'))
gulp.task('unzip', ['download'], shell.task('unzip latest.zip'))
gulp.task('move', ['unzip'], shell.task('rsync -avz wordpress/ ./'))
gulp.task('delzip', ['move'], shell.task('rm -R latest.zip wordpress/ '))
gulp.task('install_wp', ['delzip'])

gulp.task('mkdir_dev', shell.task('ida_nginx sudo mkdir /var/www/develop/revistauc.dev.ida.cl/'))
gulp.task('chmod_dev', ['mkdir_dev'], shell.task('ida_nginx sudo chmod -R 777 /var/www/develop/revistauc.dev.ida.cl/'))
gulp.task('sync_dev', ['chmod_dev'], shell.task("rsync -avz --exclude 'node_module' ./ ida_nginx:/var/www/develop/revistauc.dev.ida.cl/"))
gulp.task('sync', ['sync_dev'])


gulp.task('git_init', shell.task('git init'))
gulp.task('git_addrepo', shell.task('git remote add origin git@bitbucket.org:idachile/sitio-revistauc.git'))
gulp.task('git_push', shell.task('git push -u origin master'))


gulp.task('sync_theme', shell.task("rsync -avz --exclude 'node_module' ./wp-content/themes/ ida_nginx:/var/www/develop/revistauc.dev.ida.cl/wp-content/themes/"))